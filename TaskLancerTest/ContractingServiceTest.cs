﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text.Json;
using TaskLancer.Areas.Identity.Data;
using TaskLancer.Data;
using TaskLancer.Models.Contracting.BLL;
using TaskLancer.Models.Contracting.DAL;
using TaskLancer.Models.Project.DAL;
using TaskLancerTest.Stubs;
using Xunit;

namespace TaskLancerTest
{
    [TestCaseOrderer("TaskLancerTest.PriorityOrderer", "TaskLancerTest")]
    public class ContractingServiceTest : IClassFixture<ContractingServiceTestFixture>
    {
        private TaskLancerContext _context;
        private IContractingService _contractingService;

        public static bool A0_PublishTask_ErrorCalled;
        public static bool B0_PublishTask_SuccessfulCalled;


        public ContractingServiceTest(ContractingServiceTestFixture context)
        {
            _context = context.DbContext;
            _contractingService = new ContractingService(_context, new NotificationServiceStub(), new PaymentServiceStub(), new FileTransferServiceStub(), new ContractingSchedulingServiceStub(), new RatingServiceStub());
        }

        

        [Fact, TestPriority(1)]
        public async void A_PublishTask_Error()
        {
            A0_PublishTask_ErrorCalled = true;

            Task task = await _context.Task.FirstOrDefaultAsync(t => t.TaskId == 2);
            TaskLancerUser manager = await _context.Users.FirstOrDefaultAsync(u => u.UserName == "testeManager");

            //Invalid user id
            await Assert.ThrowsAsync<ArgumentException>(() => _contractingService.PublishTask("invalid_id", task.TaskId, "description_task2", (float)19.99, new List<string>(), new DateTime(2022, 06, 01), new DateTime(2022, 05, 30)));

            //Invalid task id
            await Assert.ThrowsAsync<ArgumentException>(() => _contractingService.PublishTask(manager.Id, -1, "description_task2", (float)19.99, new List<string>(), new DateTime(2022, 06, 01), new DateTime(2022, 05, 30)));

            //Task already in a contracting process
            await Assert.ThrowsAsync<InvalidOperationException>(() => _contractingService.PublishTask(manager.Id, 1, "description_task2", (float)19.99, new List<string>(), new DateTime(2022, 06, 01), new DateTime(2022, 05, 30)));
        }

        [Fact, TestPriority(2)]
        public async void B_PublishTask_Successful()
        {
            B0_PublishTask_SuccessfulCalled = true;

            Task task = await _context.Task.FirstOrDefaultAsync(t => t.TaskId == 2);
            TaskLancerUser manager = await _context.Users.FirstOrDefaultAsync(u => u.UserName == "testeManager");

            var result = await _contractingService.PublishTask(manager.Id, task.TaskId, "description_task2", (float)19.99, new List<string>(), new DateTime(2022, 06, 01), new DateTime(2022, 05, 30));
            ContractingProcess cp = await _context.ContractingProcess.Include(c => c.Task).FirstOrDefaultAsync(c => c.Task.TaskId == task.TaskId);
            Assert.Equal(task.TaskId, result.Task.TaskId);
            Assert.Equal(cp.Task.TaskId, result.Task.TaskId);
        }

        [Fact, TestPriority(3)]
        public async void C_AddCounterProposal_Error()
        {
            ContractingProcess cp = await _context.ContractingProcess.Include(c => c.Task).FirstOrDefaultAsync(c => c.Task.TaskId == 1);
            TaskLancerUser freelancer2 = await _context.Users.FirstOrDefaultAsync(u => u.UserName == "testeFreelancer2");

            //Invalid contracting process id
            await Assert.ThrowsAsync<ArgumentException>(() => _contractingService.AddCounterProposal(-1, freelancer2.Id, "selfIntro", (float)14.99, new DateTime(2022, 05, 30)));

            //Counter proposal in a different state
            cp.State = ContractingProcessState.Pending_Payment;
            await Assert.ThrowsAsync<InvalidOperationException>(() => _contractingService.AddCounterProposal(cp.Id, freelancer2.Id, "selfIntro", (float)14.99, new DateTime(2022, 05, 30)));
            cp.State = ContractingProcessState.Published;

            //Freelancer already has a pending counter-proposal in process.
            await Assert.ThrowsAsync<InvalidOperationException>(() => _contractingService.AddCounterProposal(cp.Id, freelancer2.Id, "selfIntro", (float)14.99, new DateTime(2022, 05, 30)));
        }

        [Fact, TestPriority(4)]
        public async void D_AddCounterProposal_Successful()
        {
            ContractingProcess cp = await _context.ContractingProcess.Include(c => c.CounterProposals).Include(c => c.Task).FirstOrDefaultAsync(c => c.Task.TaskId == 2);
            TaskLancerUser freelancer = await _context.Users.FirstOrDefaultAsync(u => u.UserName == "testeFreelancer");
            TaskLancerUser freelancer2 = await _context.Users.FirstOrDefaultAsync(u => u.UserName == "testeFreelancer2");

            Assert.Equal(0, cp.CounterProposals.Count);

            await _contractingService.AddCounterProposal(cp.Id, freelancer.Id, "Self Intro 1", (float)14.99, new DateTime(2022, 05, 30));

            Assert.Equal(1, cp.CounterProposals.Count);

            await _contractingService.AddCounterProposal(cp.Id, freelancer2.Id, "Self Intro 2", (float)17.99, new DateTime(2022, 06, 01));

            Assert.Equal(2, cp.CounterProposals.Count);
        }

        [Fact, TestPriority(5)]
        public async void E_EditCounterProposal_Error()
        {
            ContractingProcess cp = await _context.ContractingProcess.Include(c => c.Task).Include(c => c.CounterProposals).FirstOrDefaultAsync(c => c.Task.TaskId == 2);
            TaskLancerUser freelancer2 = await _context.Users.FirstOrDefaultAsync(u => u.UserName == "testeFreelancer2");
            TaskLancerUser freelancer = await _context.Users.FirstOrDefaultAsync(u => u.UserName == "testeFreelancer");

            //Invalid user id
            await Assert.ThrowsAsync<ArgumentException>(() => _contractingService.EditCounterProposal(-1, 1, freelancer2.Id, "selfIntro3", (float)14.99, new DateTime(2022, 05, 30)));

            //Counter proposal invalid id
            await Assert.ThrowsAsync<InvalidOperationException>(() => _contractingService.EditCounterProposal(cp.Id, -1, freelancer2.Id, "selfIntro3", (float)14.99, new DateTime(2022, 05, 30)));

            //User id different from the one who published the counter proposal
            await Assert.ThrowsAsync<InvalidOperationException>(() => _contractingService.EditCounterProposal(cp.Id, 1, freelancer.Id, "selfIntro3", (float)14.99, new DateTime(2022, 05, 30)));
        }

        [Fact, TestPriority(6)]
        public async void F_EditCounterProposal_Successful()
        {
            ContractingProcess cp = await _context.ContractingProcess.Include(c => c.Task).Include(c => c.CounterProposals).ThenInclude(cp => cp.Freelancer).FirstOrDefaultAsync(c => c.Task.TaskId == 2);
            TaskLancerUser freelancer = await _context.Users.FirstOrDefaultAsync(u => u.UserName == "testeFreelancer");
            CounterProposal counterProposal = cp.CounterProposals.Find(c => c.Freelancer.Id == freelancer.Id);

            Assert.Equal("Self Intro 1", counterProposal.SelfIntro);
            
            var result = await _contractingService.EditCounterProposal(cp.Id, counterProposal.Id, freelancer.Id, "Self Intro 3", (float)14.99, new DateTime(2022, 05, 30));

            Assert.Equal("Self Intro 3", cp.CounterProposals.Find(c => c.Freelancer.Id == freelancer.Id).SelfIntro);
        }

        [Fact, TestPriority(8)]
        public async void H_RemoveCounterProposal_Error()
        {
            TaskLancerUser manager = await _context.Users.FirstOrDefaultAsync(u => u.UserName == "testeManager");
            TaskLancerUser freelancer2 = await _context.Users.FirstOrDefaultAsync(u => u.UserName == "testeFreelancer2");
            ContractingProcess cp = await _context.ContractingProcess.Include(c => c.Task).FirstOrDefaultAsync(c => c.Task.TaskId == 2);
            CounterProposal counterProposal = cp.CounterProposals.Find(c => c.Freelancer.Id == freelancer2.Id);

            //User id is not equal to the manager id
            //await Assert.ThrowsAsync<InvalidOperationException>(() => _contractingService.RemoveCounterProposal(freelancer2.Id, cp.Id, counterProposal.Id));

            //The process is not in the Published state
            cp.State = ContractingProcessState.Under_Evaluation;
            await Assert.ThrowsAsync<InvalidOperationException>(() => _contractingService.RemoveCounterProposal(manager.Id, cp.Id, counterProposal.Id));
            cp.State = ContractingProcessState.Published;

            //Counter proposal doesn't exist or does not belong to this process
            await Assert.ThrowsAsync<InvalidOperationException>(() => _contractingService.RemoveCounterProposal(manager.Id, cp.Id, -1));
        }

        [Fact, TestPriority(9)]
        public async void I_RemoveCounterProposal_Successful()
        {
            TaskLancerUser manager = await _context.Users.FirstOrDefaultAsync(u => u.UserName == "testeManager");
            TaskLancerUser freelancer2 = await _context.Users.FirstOrDefaultAsync(u => u.UserName == "testeFreelancer2");
            ContractingProcess cp = await _context.ContractingProcess.Include(c => c.Task).FirstOrDefaultAsync(c => c.Task.TaskId == 2);
            CounterProposal counterProposal = cp.CounterProposals.Find(c => c.Freelancer.Id == freelancer2.Id);

            _contractingService.RemoveCounterProposal(manager.Id, cp.Id, counterProposal.Id);
        }

        [Fact, TestPriority(9)]
        public async void I_AllContractingProcessesAsManager_Error()
        {
            Assert.Equal(0,_contractingService.AllContractingProcessesAsManager("invalid_id").Result.Count);
        }

        [Fact, TestPriority(10)]
        public async void J_AllContractingProcessesAsManager_Sucessful()
        {
            TaskLancerUser manager = await _context.Users.FirstOrDefaultAsync(u => u.UserName == "testeManager");

            Assert.Equal(2, _contractingService.AllContractingProcessesAsManager(manager.Id).Result.Count);
        }

        [Fact, TestPriority(11)]
        public async void K_GetAllPublishedContractingProcesses_Sucessful()
        {
            TaskLancerUser manager = await _context.Users.FirstOrDefaultAsync(u => u.UserName == "testeManager");

            Assert.Equal(2, _contractingService.GetAllPublishedContractingProcesses().Count);
        }

        [Fact, TestPriority(12)]
        public async void L_GetContractingProcess_Error()
        {
            await Assert.ThrowsAsync<ArgumentException>(() => _contractingService.GetContractingProcess(-1));
        }

        [Fact, TestPriority(13)]
        public async void M_GetContractingProcess_Sucessful()
        {
            ContractingProcess cp = await _context.ContractingProcess.FirstOrDefaultAsync();

            Assert.Equal(cp.Id, _contractingService.GetContractingProcess(cp.Id).Result.Id);
        }

        [Fact, TestPriority(14)]
        public async void N_GetContractingProcessByTaskId_Error()
        {
            await Assert.ThrowsAsync<ArgumentException>(() => _contractingService.GetContractingProcessByTaskId(-1));
        }

        [Fact, TestPriority(15)]
        public async void O_GetContractingProcessByTaskId_Sucessful()
        {
            ContractingProcess cp = await _context.ContractingProcess.Include(p => p.Task).FirstOrDefaultAsync();

            Assert.Equal(cp.Id, _contractingService.GetContractingProcessByTaskId(cp.Task.TaskId).Result.Id);
        }

        [Fact, TestPriority(16)]
        public async void P_AllContractingProcessesAsFreelancer_Error()
        {
            Assert.Equal(0, _contractingService.AllContractingProcessesAsManager("invalid_id").Result.Count);
        }

        [Fact, TestPriority(17)]
        public async void Q_AllContractingProcessesAsFreelancer_Sucessful()
        {
            TaskLancerUser freelancer = await _context.Users.FirstOrDefaultAsync(u => u.UserName == "testeFreelancer");

            Assert.Equal(0, _contractingService.AllContractingProcessesAsManager(freelancer.Id).Result.Count);
        }

        [Fact, TestPriority(18)]
        public async void R_AcceptCounterProposal_Error()
        {
            TaskLancerUser manager = await _context.Users.FirstOrDefaultAsync(u => u.UserName == "testeManager");
            TaskLancerUser freelancer = await _context.Users.FirstOrDefaultAsync(u => u.UserName == "testeFreelancer");
            ContractingProcess cp = await _context.ContractingProcess.Include(c => c.Task).FirstOrDefaultAsync(c => c.Task.TaskId == 2);
            CounterProposal counterProposal = cp.CounterProposals.Find(c => c.Freelancer.Id == freelancer.Id);

            //User id is not equal to the manager id
            await Assert.ThrowsAsync<InvalidOperationException>(() => _contractingService.AcceptCounterProposal(freelancer.Id, cp.Id, counterProposal.Id));

            //The process is not in the Published state
            cp.State = ContractingProcessState.Under_Evaluation;
            await Assert.ThrowsAsync<InvalidOperationException>(() => _contractingService.AcceptCounterProposal(manager.Id, cp.Id, counterProposal.Id));
            cp.State = ContractingProcessState.Published;

            //Counter proposal doesn't exist or does not belong to this process
            await Assert.ThrowsAsync<InvalidOperationException>(() => _contractingService.AcceptCounterProposal(manager.Id, cp.Id, -1));
        }


        [Fact, TestPriority(19)]
        public async void S_AcceptCounterProposal_Successful()
        {
            TaskLancerUser manager = await _context.Users.FirstOrDefaultAsync(u => u.UserName == "testeManager");
            TaskLancerUser freelancer = await _context.Users.FirstOrDefaultAsync(u => u.UserName == "testeFreelancer");
            ContractingProcess cp = await _context.ContractingProcess.Include(c => c.Task).FirstOrDefaultAsync(c => c.Task.TaskId == 2);
            CounterProposal counterProposal = cp.CounterProposals.Find(c => c.Freelancer.Id == freelancer.Id);

            _contractingService.AcceptCounterProposal(manager.Id, cp.Id, counterProposal.Id);
        }

        [Fact, TestPriority(20)]
        public async void T_AllContractingProcessesAsFreelancer_Error()
        {
            Assert.Equal(0, _contractingService.AllContractingProcessesAsFreelancer("invalid_id").Result.Count);
        }

        [Fact, TestPriority(21)]
        public async void U_AllContractingProcessesAsFreelancer_Sucessful()
        {
            TaskLancerUser freelancer = await _context.Users.FirstOrDefaultAsync(u => u.UserName == "testeFreelancer");

            Assert.Equal(1, _contractingService.AllContractingProcessesAsFreelancer(freelancer.Id).Result.Count);
        }

        [Fact, TestPriority(22)]
        public async void T_PaymentWasSuccessful_Error()
        {
            //Payment id invalid
            await Assert.ThrowsAsync<InvalidOperationException>(() => _contractingService.PaymentWasSuccessful(-1));

            //Contracting process in different state than Pending Payment
            ContractingProcess cp = await _context.ContractingProcess.Include(c => c.Task).FirstOrDefaultAsync(c => c.Task.TaskId == 2);
            cp.State = ContractingProcessState.Published;
            await Assert.ThrowsAsync<InvalidOperationException>(() => _contractingService.PaymentWasSuccessful((int)cp.PaymentId));
            cp.State = ContractingProcessState.Pending_Payment;

        }

        /*[Fact, TestPriority(23)]
        public async void U_AddDelivery_Successful()
        {
            ContractingProcess cp = await _context.ContractingProcess.Include(c => c.Task).FirstOrDefaultAsync(c => c.Task.TaskId == 2);
            TaskLancerUser freelancer = await _context.Users.FirstOrDefaultAsync(u => u.UserName == "testeFreelancer");

            List<string>? sampleFilesIds = JsonSerializer.Deserialize<List<string>>("sampleFileId");
            List<string>? finalFilesIds = JsonSerializer.Deserialize<List<string>>("finalFileId");

            Assert.Equal(cp.Deliveries.Count, 0);

            var result = _contractingService.AddDelivery(cp.Id, freelancer.Id, "Done",sampleFilesIds, finalFilesIds);

            Assert.Equal(cp.Deliveries.Count, 1);
        }

        [Fact, TestPriority(24)]
        public async void V_AddDelivery_Error()
        {
            ContractingProcess cp = await _context.ContractingProcess.Include(c => c.Task).FirstOrDefaultAsync(c => c.Task.TaskId == 2);
            TaskLancerUser freelancer = await _context.Users.FirstOrDefaultAsync(u => u.UserName == "testeFreelancer");
            TaskLancerUser freelancer2 = await _context.Users.FirstOrDefaultAsync(u => u.UserName == "testeFreelancer2");

            List<string>? sampleFilesIds = JsonSerializer.Deserialize<List<string>>("sampleFileId");
            List<string>? finalFilesIds = JsonSerializer.Deserialize<List<string>>("finalFileId");

            //Invalid user id
            await Assert.ThrowsAsync<ArgumentException>(() => _contractingService.AddDelivery(cp.Id, "invalid_id", "Done", sampleFilesIds, finalFilesIds));
            //Process in different state than ordered
            cp.State = ContractingProcessState.Published;
            await Assert.ThrowsAsync<InvalidOperationException>(() => _contractingService.AddDelivery(cp.Id, freelancer.Id, "Done", sampleFilesIds, finalFilesIds));
            cp.State = ContractingProcessState.Ordered;
            //Wrong freelancer
            await Assert.ThrowsAsync<InvalidOperationException>(() => _contractingService.AddDelivery(cp.Id, freelancer2.Id, "Done", sampleFilesIds, finalFilesIds));
            //Already a delivery made
            await Assert.ThrowsAsync<InvalidOperationException>(() => _contractingService.AddDelivery(cp.Id, freelancer.Id, "Done", sampleFilesIds, finalFilesIds));
        }*/

        

    }
}
