﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskLancer.Areas.Identity.Data;
using TaskLancer.Data;
using TaskLancer.Models.Contracting.DAL;
using TaskLancer.Models.Rating.BLL;
using TaskLancer.Models.Rating.DAL;
using Xunit;

namespace TaskLancerTest
{
    [TestCaseOrderer("TaskLancerTest.PriorityOrderer", "TaskLancerTest")]
    public class RatingServiceTest : IClassFixture<RatingServiceTestFixture>
    {
        private TaskLancerContext _context;
        private IRatingService _ratingService;

        public RatingServiceTest(RatingServiceTestFixture context)
        {
            _context = context.DbContext;
            _ratingService = new RatingService(_context);
        }

        [Fact, TestPriority(1)]
        public async void CreateContractingProcessReviews_Fail()
        {
            TaskLancerUser manager = await _context.Users.FirstOrDefaultAsync(u => u.UserName == "testeManager");
            TaskLancerUser freelancer = await _context.Users.FirstOrDefaultAsync(u => u.UserName == "testeFreelancer");
            ContractingProcess cp = await _context.ContractingProcess.FirstOrDefaultAsync(c => c.Id == 1);

            //Invalid contracting process ID
            await Assert.ThrowsAsync<ArgumentException>(() => _ratingService.CreateContractingProcessReviews(-1, manager.Id, freelancer.Id));

            //Invalid contracting process state
            cp.State = ContractingProcessState.Delivered;
            await Assert.ThrowsAsync<InvalidOperationException>(() => _ratingService.CreateContractingProcessReviews(cp.Id, manager.Id, freelancer.Id));
            cp.State = ContractingProcessState.Completed;

            //Invalid manager ID
            await Assert.ThrowsAsync<ArgumentException>(() => _ratingService.CreateContractingProcessReviews(cp.Id, "invalid_id", freelancer.Id));

            //Invalid freelancer ID
            await Assert.ThrowsAsync<ArgumentException>(() => _ratingService.CreateContractingProcessReviews(cp.Id, manager.Id, "invalid_id"));
        }

        [Fact, TestPriority(2)]
        public async void CreateContractingProcessReviews_Successful()
        {
            TaskLancerUser manager = await _context.Users.FirstOrDefaultAsync(u => u.UserName == "testeManager");
            TaskLancerUser freelancer = await _context.Users.FirstOrDefaultAsync(u => u.UserName == "testeFreelancer");
            ContractingProcess cp = await _context.ContractingProcess.FirstOrDefaultAsync(c => c.Id == 1);

            var result = await _ratingService.CreateContractingProcessReviews(cp.Id, manager.Id, freelancer.Id);

            Assert.Equal(2, result.Count);
        }

        [Fact, TestPriority(3)]
        public async void GetPendingReviews_Fail()
        {
            //Invalid user ID
            await Assert.ThrowsAsync<ArgumentException>(() => _ratingService.GetUserPendingReviews("invalid_id"));
        }

        [Fact, TestPriority(4)]
        public async void GetPendingReviews_Successful()
        {
            TaskLancerUser manager = await _context.Users.FirstOrDefaultAsync(u => u.UserName == "testeManager");
            ContractingProcess cp = await _context.ContractingProcess.FirstOrDefaultAsync(c => c.Id == 1);

            List<Review> reviews = await _ratingService.GetContractingProcessReviews(1);

            var result = await _ratingService.GetUserPendingReviews(manager.Id);

            Assert.Equal(1, result.Count);
        }

        [Fact, TestPriority(5)]
        public async void SubmitReview_Successful()
        {
            TaskLancerUser manager = await _context.Users.FirstOrDefaultAsync(u => u.UserName == "testeManager");
            TaskLancerUser freelancer = await _context.Users.FirstOrDefaultAsync(u => u.UserName == "testeFreelancer");
            ContractingProcess cp = await _context.ContractingProcess.FirstOrDefaultAsync(c => c.Id == 1);
            Review review = await _context.Reviews.FirstOrDefaultAsync(r => r.ContractingProcessId == cp.Id && r.AuthorId == manager.Id);

            Assert.Equal(review.State, ReviewState.Pending);
            
            await _ratingService.SubmitReview(cp.Id, manager.Id, "teste", 3.5);
            await _ratingService.SubmitReview(cp.Id, freelancer.Id, "teste", 4.0);

            Assert.Equal(review.State, ReviewState.Submitted);

        }

        [Fact, TestPriority(6)]
        public async void SubmitReview_Fail()
        {
            TaskLancerUser manager = await _context.Users.FirstOrDefaultAsync(u => u.UserName == "testeManager");
            ContractingProcess cp = await _context.ContractingProcess.FirstOrDefaultAsync(c => c.Id == 1);

            //Invalid contracting process ID
            await Assert.ThrowsAsync<ArgumentException>(() => _ratingService.SubmitReview(-1, manager.Id, "teste", 3.5));

            //Invalid manager ID
            await Assert.ThrowsAsync<ArgumentException>(() => _ratingService.SubmitReview(cp.Id, "invalid_id", "teste", 3.5));

            //Review already submitted
            await Assert.ThrowsAsync<InvalidOperationException>(() => _ratingService.SubmitReview(cp.Id, manager.Id, "teste", 3.5));
        }

        [Fact, TestPriority(7)]
        public async void GetReview_ByReviewId_Fail()
        {
            //Invalid review ID
            await Assert.ThrowsAsync<ArgumentException>(() => _ratingService.GetReview(-1));
        }

        [Fact, TestPriority(8)]
        public async void GetReview_ByReviewId_Successful()
        {
            TaskLancerUser manager = await _context.Users.FirstOrDefaultAsync(u => u.UserName == "testeManager");
            ContractingProcess cp = await _context.ContractingProcess.FirstOrDefaultAsync(c => c.Id == 1);
            Review review = await _context.Reviews.FirstOrDefaultAsync(r => r.ContractingProcessId == cp.Id && r.AuthorId == manager.Id);

            var result = await _ratingService.GetReview(review.Id);

            Assert.Equal(review, result);
        }

        [Fact, TestPriority(9)]
        public async void GetReview_ByCPIdAndUserId_Fail()
        {
            TaskLancerUser manager = await _context.Users.FirstOrDefaultAsync(u => u.UserName == "testeManager");
            ContractingProcess cp = await _context.ContractingProcess.FirstOrDefaultAsync(c => c.Id == 1);

            //Invalid contracting process ID
            await Assert.ThrowsAsync<ArgumentException>(() => _ratingService.GetReview(-1, manager.Id));

            //Invalid contracting process ID
            await Assert.ThrowsAsync<ArgumentException>(() => _ratingService.GetReview(cp.Id, "invalid_id"));
        }

        [Fact, TestPriority(10)]
        public async void GetReview_ByCPIdAndUserId_Successful()
        {
            TaskLancerUser manager = await _context.Users.FirstOrDefaultAsync(u => u.UserName == "testeManager");
            ContractingProcess cp = await _context.ContractingProcess.FirstOrDefaultAsync(c => c.Id == 1);
            Review review = await _context.Reviews.FirstOrDefaultAsync(r => r.ContractingProcessId == cp.Id && r.AuthorId == manager.Id);

            var result = await _ratingService.GetReview(cp.Id, manager.Id);

            Assert.Equal(review, result);
        }

        [Fact, TestPriority(11)]
        public async void GetReviewReceived_Fail()
        {
            TaskLancerUser manager = await _context.Users.FirstOrDefaultAsync(u => u.UserName == "testeManager");
            ContractingProcess cp = await _context.ContractingProcess.FirstOrDefaultAsync(c => c.Id == 1);

            //Invalid contracting process ID
            await Assert.ThrowsAsync<ArgumentException>(() => _ratingService.GetReviewReceived(-1, manager.Id));

            //Invalid contracting process ID
            await Assert.ThrowsAsync<ArgumentException>(() => _ratingService.GetReviewReceived(cp.Id, "invalid_id"));
        }

        [Fact, TestPriority(12)]
        public async void GetReviewReceived_Successful()
        {
            TaskLancerUser manager = await _context.Users.FirstOrDefaultAsync(u => u.UserName == "testeManager");
            ContractingProcess cp = await _context.ContractingProcess.FirstOrDefaultAsync(c => c.Id == 1);
            Review review = await _context.Reviews.FirstOrDefaultAsync(r => r.ContractingProcessId == cp.Id && r.SubjectId == manager.Id);

            var result = await _ratingService.GetReviewReceived(cp.Id, manager.Id);

            Assert.Equal(review, result);
        }

        [Fact, TestPriority(13)]
        public async void GetManagerRating_Fail()
        {
            //Invalid user ID
            await Assert.ThrowsAsync<ArgumentException>(() => _ratingService.GetManagerRating("invalid_id"));
        }

        [Fact, TestPriority(14)]
        public async void GetManagerRating_Successful()
        {
            TaskLancerUser manager = await _context.Users.FirstOrDefaultAsync(u => u.UserName == "testeManager");
            
            var result = await _ratingService.GetManagerRating(manager.Id);

            Assert.IsType<double>(result);

            TaskLancerUser freelancer = await _context.Users.FirstOrDefaultAsync(u => u.UserName == "testeFreelancer");

            var result1 = await _ratingService.GetManagerRating(freelancer.Id);

            Assert.IsType<double>(result1);
        }

        [Fact, TestPriority(13)]
        public async void GetFreelancerRating_Fail()
        {
            //Invalid user ID
            await Assert.ThrowsAsync<ArgumentException>(() => _ratingService.GetFreelancerRating("invalid_id"));
        }

        [Fact, TestPriority(14)]
        public async void GetFreelancerRating_Successful()
        {
            TaskLancerUser freelancer = await _context.Users.FirstOrDefaultAsync(u => u.UserName == "testeFreelancer");

            var result = await _ratingService.GetFreelancerRating(freelancer.Id);

            Assert.IsType<double>(result);

            TaskLancerUser manager = await _context.Users.FirstOrDefaultAsync(u => u.UserName == "testeManager");

            var result1 = await _ratingService.GetFreelancerRating(manager.Id);

            Assert.IsType<double>(result1);
        }

        [Fact, TestPriority(15)]
        public async void GetReviewsReceivedAsManager_Fail()
        {
            //Invalid user ID
            await Assert.ThrowsAsync<ArgumentException>(() => _ratingService.GetReviewsReceivedAsManager("invalid_id"));
        }

        [Fact, TestPriority(16)]
        public async void GetReviewsReceivedAsManager_Successful()
        {
            TaskLancerUser manager = await _context.Users.FirstOrDefaultAsync(u => u.UserName == "testeManager");
            
            var result = await _ratingService.GetReviewsReceivedAsManager(manager.Id);

            Assert.Equal(1, result.Count);
        }

        [Fact, TestPriority(17)]
        public async void GetReviewsReceivedAsFreelancer_Fail()
        {
            //Invalid user ID
            await Assert.ThrowsAsync<ArgumentException>(() => _ratingService.GetReviewsReceivedAsFreelancer("invalid_id"));
        }

        [Fact, TestPriority(18)]
        public async void GetReviewsReceivedAsFreelancer_Successful()
        {
            TaskLancerUser freelancer = await _context.Users.FirstOrDefaultAsync(u => u.UserName == "testeFreelancer");

            var result = await _ratingService.GetReviewsReceivedAsFreelancer(freelancer.Id);

            Assert.Equal(1, result.Count);
        }

        [Fact, TestPriority(19)]
        public async void GetReviewsSubmiteddAsManager_Fail()
        {
            //Invalid user ID
            await Assert.ThrowsAsync<ArgumentException>(() => _ratingService.GetReviewsSubmittedAsManager("invalid_id"));
        }

        [Fact, TestPriority(20)]
        public async void GetReviewsSubmiteAsManager_Successful()
        {
            TaskLancerUser manager = await _context.Users.FirstOrDefaultAsync(u => u.UserName == "testeManager");

            var result = await _ratingService.GetReviewsSubmittedAsManager(manager.Id);

            Assert.Equal(1, result.Count);
        }

        [Fact, TestPriority(21)]
        public async void GetReviewsSubmiteAsFreelancer_Fail()
        {
            //Invalid user ID
            await Assert.ThrowsAsync<ArgumentException>(() => _ratingService.GetReviewsSubmittedAsFreelancer("invalid_id"));
        }

        [Fact, TestPriority(22)]
        public async void GetReviewsSubmiteAsFreelancer_Successful()
        {
            TaskLancerUser freelancer = await _context.Users.FirstOrDefaultAsync(u => u.UserName == "testeFreelancer");

            var result = await _ratingService.GetReviewsSubmittedAsFreelancer(freelancer.Id);

            Assert.Equal(1, result.Count);
        }

    }
}
