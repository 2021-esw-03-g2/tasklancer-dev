﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskLancer.Models.FileTransfer.BLL;

namespace TaskLancerTest.Stubs
{
    internal class FileTransferServiceStub : IFileTransferService
    {
        public Task<TaskLancer.Models.FileTransfer.DAL.File> GetFile(string fileId, string userId)
        {
            return null;
        }

        public Task<Stream> GetFileStream(string fileId, string userId)
        {
            return null;
        }

        public List<TaskLancer.Models.FileTransfer.DAL.File> GetUserUnusedFiles(string userId)
        {
            return null;
        }

        public Task<bool> RemoveFile(string fileId, string userId)
        {
            return null;
        }

        public void RemoveUnusedFiles()
        {
        }

        public Task<TaskLancer.Models.FileTransfer.DAL.File> SavePrivateFile(string fileName, Stream data, string ownerId, List<string> allowedUsersIds)
        {
            return null;
        }

        public Task<TaskLancer.Models.FileTransfer.DAL.File> SavePublicFile(string fileName, Stream data, string ownerId)
        {
            return null;
        }

        public Task<bool> SetInUse(string fileId)
        {
            return null;
        }
    }
}
