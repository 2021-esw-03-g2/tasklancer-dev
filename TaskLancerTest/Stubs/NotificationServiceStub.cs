﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TaskLancer.Areas.Identity.Data;
using TaskLancer.Models.Notifications.BLL;
using TaskLancer.Models.Notifications.DAL;

namespace TaskLancerTest.Stubs
{
    public class NotificationServiceStub : INotificationService
    {
        public async Task<int> CreateNotificationSubscription(string userId)
        {
            return 1;
        }

        public async Task<int> DeleteNotification(TaskLancerUser user, int notificationId)
        {
            return 1;
        }

        public List<Notification> GetNotificationsAsync(TaskLancerUser user)
        {
            return new List<Notification>();
        }

        public List<Notification> GetSeenNotifications(TaskLancerUser user)
        {
            return new List<Notification>();
        }

        public List<Notification> GetUnseenNotifications(TaskLancerUser user)
        {
            return new List<Notification>();
        }

        public async Task<int> SaveWebPushSubscription(string userId, string endpoint, string p256dh, string auth)
        {
            return 1;
        }

        public async Task<int> SendNotification(TaskLancerUser user, string description, string content, string link)
        {
            return 1;
        }

        public async Task SendPushNotification(string userId, Notification notification)
        {
            return;
        }

        public async Task<int> SetNotificationStatus(TaskLancerUser user, int notificationId)
        {
            return 1;
        }
    }
}
