﻿using PayPalCheckoutSdk.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskLancer.Areas.Identity.Data;
using TaskLancer.Controllers;
using TaskLancer.Models.Payments.BLL;
using TaskLancer.Models.Payments.DAL;

namespace TaskLancerTest.Stubs
{
    internal class PaymentServiceStub : IPaymentService
    {
        public Task<PayPalHelper.MyPaypalSetup> AprovePayment(int id, string route)
        {
            return null;
        }

        public Task CancelPayment(int paymentId)
        {
            return null;
        }

        public Task<Order> CapturePayment(int paymentId, string orderId)
        {
            return null;
        }

        public Task<Order> CreateOrder(int paymentId, string redirectRoute)
        {
            return null;
        }

        public async Task<int> CreatePayment(int userPaymentId, Payment payment)
        {
            return 1;
        }

        public async Task<int> CreatePayment(string userId, Payment payment)
        {
            return 1;
        }

        public Task<PaymentViewModel> CreatePayout(int id)
        {
            return null;
        }

        public Task<int> CreateUserPayment(string userId)
        {
            return null;
        }

        public Task ExecutePayout(string authenticatedUserId, int paymentId)
        {
            return null;
        }

        public Task<List<Payment>> GetCanceledPayments(TaskLancerUser user)
        {
            throw new NotImplementedException();
        }

        public Task<List<Payment>> GetPaidPayments(TaskLancerUser user)
        {
            return null;
        }

        public Task<Payment> GetPayment(int id)
        {
            return null;
        }

        public Task<List<Payment>> GetPayments(TaskLancerUser user)
        {
            return null;
        }

        public Task<List<Payment>> GetPayouts(TaskLancerUser user)
        {
            return null;
        }

        public string GetPayPalEmail(TaskLancerUser user)
        {
            return null;
        }

        public Task<List<Payment>> GetRefundedPayments(TaskLancerUser user)
        {
            throw new NotImplementedException();
        }

        public Task<List<Payment>> GetUnpaidPayments(TaskLancerUser user)
        {
            return null;
        }

        public Task<PaymentViewModel> ProcedeWithTransaction(int id, string token)
        {
            return null;
        }

        public Task RefundPayment(int paymentId)
        {
            return null;
        }

        public Task<int> SetupPaypalEmail(TaskLancerUser user, string email)
        {
            return null;
        }

        public Task UnlockPaymentPayout(int paymentId)
        {
            return null;
        }

        public Task<int> UpdatePayment(Payment payment)
        {
            return null;
        }
    }
}
