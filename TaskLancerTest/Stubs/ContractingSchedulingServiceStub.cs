﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskLancer.Models.Contracting.BLL;
using TaskLancer.Models.Contracting.DAL;

namespace TaskLancerTest.Stubs
{
    public class ContractingSchedulingServiceStub : IContractingSchedulingService
    {
        public Task ContractingProcessPublished(ContractingProcess contractingProcess)
        {
            return Task.CompletedTask;
        }

        public Task CounterProposalAccepted(ContractingProcess contractingProcess)
        {
            return Task.CompletedTask;
        }

        public Task DeliveryAccepted(ContractingProcess contractingProcess)
        {
            return Task.CompletedTask;
        }

        public Task RemoveAllPendingEvents(ContractingProcess contractingProcess)
        {
            return Task.CompletedTask;
        }

        public Task TaskOrdered(ContractingProcess contractingProcess)
        {
            return Task.CompletedTask;
        }

        public Task UnderEvaluation(ContractingProcess contractingProcess)
        {
            return Task.CompletedTask;
        }
    }
}
