﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskLancer.Models.Rating.BLL;
using TaskLancer.Models.Rating.DAL;

namespace TaskLancerTest.Stubs
{
    public class RatingServiceStub : IRatingService
    {
        public async Task<List<Review>> CreateContractingProcessReviews(int contractingProcessId, string managerId, string freelancerId)
        {
            return new List<Review>();
        }

        public async Task<List<Review>> GetContractingProcessReviews(int contractingProcessId)
        {
            return new List<Review>();
        }

        public async Task<double> GetFreelancerRating(string userId)
        {
            return 2.5;
        }

        public async Task<double> GetManagerRating(string userId)
        {
            return 2.5;
        }

        public async Task<Review> GetReview(int reviewId)
        {
            return new Review();
        }

        public async Task<Review> GetReview(int contractingProcessId, string userId)
        {
            return new Review();
        }

        public async Task<Review> GetReviewReceived(int contractingProcessId, string userId)
        {
            return new Review();
        }

        public async Task<List<Review>> GetReviewsReceivedAsFreelancer(string freelancerId)
        {
            return new List<Review>();
        }

        public async Task<List<Review>> GetReviewsReceivedAsManager(string managerId)
        {
            return new List<Review>();
        }

        public async Task<List<Review>> GetReviewsSubmittedAsFreelancer(string freelancerId)
        {
            return new List<Review>();
        }

        public async Task<List<Review>> GetReviewsSubmittedAsManager(string managerId)
        {
            return new List<Review>();
        }

        public async Task<List<Review>> GetUserPendingReviews(string userId)
        {
            return new List<Review>();
        }

        public async Task<Review> SubmitReview(int contractingProcessId, string authenticatedUserId, string comment, double score)
        {
            return new Review();
        }
    }
}
