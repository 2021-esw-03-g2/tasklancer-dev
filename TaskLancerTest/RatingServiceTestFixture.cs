﻿using System;
using System.Collections.Generic;
using TaskLancer.Areas.Identity.Data;
using TaskLancer.Models.Contracting.DAL;
using TaskLancer.Models.FileTransfer.DAL;
using TaskLancer.Models.Project.DAL;

namespace TaskLancerTest
{
    public class RatingServiceTestFixture : ApplicationDbContextFixture, IDisposable
    {
        protected async override void AddObjects()
        {


            TaskLancerUser manager = new TaskLancerUser
            {
                Email = "manager@email.com",
                UserName = "testeManager",
                Biography = "Biografia Manager",
                Curriculum = "Curriculum Manager",
            };

            TaskLancerUser freelancer = new TaskLancerUser
            {
                Email = "freelancer@email.com",
                UserName = "testeFreelancer",
                Biography = "Biografia Freelancer",
                Curriculum = "Curriculum Freelancer",
            };

            DbContext.Users.AddRange(manager);
            DbContext.Users.AddRange(freelancer);

            Task task1 = new Task()
            {
                TaskId = 1,
                Title = "Task1",
                Description = "Task1"
            };

            Task task2 = new Task()
            {
                TaskId = 2,
                Title = "Task2",
                Description = "Task2"
            };

            List<Task> tasks = new List<Task>();
            tasks.Add(task1);
            tasks.Add(task2);

            Column column1 = new Column()
            {
                ColumnId = 1,
                Name = "Teste1",
                Color = "Teste1",
                Tasks = tasks,
                TasksOrder = "1;2"
            };

            Column column2 = new Column()
            {
                ColumnId = 2,
                Name = "Teste2",
                Color = "Teste2",
                Tasks = new List<Task>(),
                TasksOrder = ""
            };

            List<Column> columns = new List<Column>();
            columns.Add(column1);
            columns.Add(column2);

            Board board = new Board()
            {
                BoardId = 1,
                ProjectId = 1,
                Columns = columns,
                ColumnsOrder = "1;2",
            };

            Project project = new Project()
            {
                ProjectId = 1,
                Name = "Teste",
                Description = "Teste",
                IsPublic = false,
                Manager = manager,
                Board = board,

            };



            DbContext.Project.AddRange(project);

            DbContext.CategoryTag.AddRange(
                new CategoryTag { Name = "Web Application" },
                new CategoryTag { Name = "Media Campaign" },
                new CategoryTag { Name = "Music" },
                new CategoryTag { Name = "Publishing" },
                new CategoryTag { Name = "Art" }
            );

            Proposal proposal = new Proposal()
            {
                Manager = manager,
                Description = "description_task2",
                Amount = (float)19.99,
                DeliveryDate = new DateTime(2022, 05, 01),
                ReferenceFiles = new List<File>()
            };

            List<CounterProposal> counterProposals = new List<CounterProposal>();

            CounterProposal counterProposal = new CounterProposal()
            {
                Id = 1,
                Freelancer = freelancer,
                SelfIntro = "selfIntro",
                Amount = (float)17.99,
                DeliveryDate = new DateTime(2022, 06, 02),
            };

            Delivery delivery = new Delivery()
            {
                Id = 1,
                FinalComment = "Delivery test",
                ApprovalState = DeliveryApprovalState.Approved,
                ContractingProcessId =1,
            };

            List<Delivery> deliveries = new List<Delivery>();
            deliveries.Add(delivery);

            counterProposals.Add(counterProposal);

            ContractingProcess cp = new ContractingProcess()
            {
                Id = 1,
                Task = task1,
                State = ContractingProcessState.Completed,
                PublishingDate = DateTime.Now,
                CounterProposalsEndDate = new DateTime(2022, 04, 20),
                Proposal = proposal,
                CounterProposals = counterProposals,
                //AcceptedCounterProposal = counterProposal,
                //Deliveries = deliveries,
            };

            DbContext.ContractingProcess.AddRange(cp);
        }

        public void Dispose()
        {
            DbContext.Dispose();
        }

    }
}
