﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskLancer.Data;
using TaskLancer.Models.Project.DAL;
using TaskLancer.Services;
using Xunit;

namespace TaskLancerTest
{
    public class CategoryTagsServiceTest : IClassFixture<CategoryTagsTestFixture>
    {
        private TaskLancerContext _context;
        private ICategoryTagService _categoryTagService;

        public CategoryTagsServiceTest(CategoryTagsTestFixture context)
        {
            _context = context.DbContext;
            _categoryTagService = new CategoryTagService(_context);
        }

        [Fact]
        public async void EditCategoryTagTest()
        {
            CategoryTag categorytag = new CategoryTag { Name = "Music" };

            await _categoryTagService.ChangeCategoryTag(2, categorytag.Name, categorytag.Description);

            List<CategoryTag> categoryTagsList = _context.CategoryTag.ToList();

            CategoryTag categorytag3 = categoryTagsList.ElementAt(0);

            Assert.Equal(categorytag.Name, categorytag3.Name);;
        }
        [Fact]
        public async void DeleteCategoryTagTest()
        {
            await _categoryTagService.DeleteCategoryTag(1);
            List<CategoryTag> categoryTagsList = _context.CategoryTag.ToList();
            Assert.Equal(4, categoryTagsList.Count);
        }
        [Fact]
        public async void AddCategoryTagTest()
        {
            CategoryTag categorytag = new CategoryTag { Name = "Nova Tag"};
            //SkillTag categorytag2 = new SkillTag { Name = "3D Design", Description = "bem podre" };
            Assert.False(_categoryTagService.CheckIfExists(categorytag));
            //Assert.True(_categoryTagService.CheckIfExists(categorytag2));
            await _categoryTagService.AddCategoryTag(categorytag.Name, categorytag.Description);
            List<CategoryTag> categoryTagsList = _context.CategoryTag.ToList();
            Assert.Equal(5, categoryTagsList.Count);
        }
    }
}
