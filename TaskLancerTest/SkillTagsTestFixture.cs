﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskLancer.Models;

namespace TaskLancerTest
{
    public class SkillTagsTestFixture : ApplicationDbContextFixture
    {
        protected override void AddObjects()
        {
            DbContext.SkillTag.AddRange(
                new SkillTag { Name = "3D Design", Description = "3D model design" },
                new SkillTag { Name = "ASP-NET CORE", Description = "ASP-NET core and web development" },
                new SkillTag { Name = "React", Description = "Web development using React js" },
                new SkillTag { Name = "Concept Art", Description = "Concept Art designer" },
                new SkillTag { Name = "Music Production", Description = "Music producer" },
                new SkillTag { Name = "Voice Acting", Description = "Voice actor" },
                new SkillTag { Name = "UX Design", Description = "User-experience designer" },
                new SkillTag { Name = "UI Design", Description = "User-interface designer" }
            );
        }
    }
}
