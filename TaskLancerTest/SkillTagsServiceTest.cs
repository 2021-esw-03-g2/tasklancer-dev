﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskLancer.Data;
using TaskLancer.Models;
using TaskLancer.Services;
using Xunit;

namespace TaskLancerTest
{
    public class SkillTagsServiceTest : IClassFixture<SkillTagsTestFixture>
    {
        private TaskLancerContext _context;
        private ISkillTagService _skillTagService;

        public SkillTagsServiceTest(SkillTagsTestFixture context)
        {
            _context = context.DbContext;
            _skillTagService = new SkillTagService(_context);
        }

        [Fact]
        public async void EditSkillTagTest()
        {
            SkillTag skilltag = new SkillTag { Name = "3D Design", Description = "3D model design" };

            await _skillTagService.ChangeSkillTag(2, skilltag.Name, skilltag.Description);

            List<SkillTag> skillTagsList = _context.SkillTag.ToList();

            SkillTag skilltag3 = skillTagsList.ElementAt(0);

            Assert.Equal(skilltag.Name, skilltag3.Name);
        }
        [Fact]
        public async void DeleteSkillTagTest()
        {
            await _skillTagService.DeleteSkillTag(1);
            List<SkillTag> skillTagsList = _context.SkillTag.ToList();
            Assert.Equal(7, skillTagsList.Count);
        }
        [Fact]
        public async void AddSkillTagTest()
        {
            SkillTag skilltag = new SkillTag {Name="Nova Tag", Description="Buéda fish" };
            //SkillTag skilltag2 = new SkillTag { Name = "3D Design", Description = "bem podre" };
            Assert.False(_skillTagService.CheckIfExists(skilltag));
            //Assert.True(_skillTagService.CheckIfExists(skilltag2));
            await _skillTagService.AddSkillTag(skilltag.Name, skilltag.Description);
            List<SkillTag> skillTagsList = _context.SkillTag.ToList();
            Assert.Equal(8, skillTagsList.Count);
        }
    }
}
