﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using TaskLancer.Areas.Identity.Data;
using TaskLancer.Models.Contracting.BLL;
using TaskLancer.Models.Contracting.DAL;
using TaskLancer.Models.FileTransfer.DAL;
using TaskLancer.Models.Project.DAL;
using TaskLancerTest.Stubs;

namespace TaskLancerTest
{
    public class ContractingServiceTestFixture : ApplicationDbContextFixture, IDisposable
    {
        private IContractingService _contractingService;


        protected async override void AddObjects()
        {
            _contractingService = new ContractingService(DbContext, new NotificationServiceStub(), new PaymentServiceStub(), new FileTransferServiceStub(), new ContractingSchedulingServiceStub(), new RatingServiceStub());

            TaskLancerUser manager = new TaskLancerUser
            {
                Email = "manager@email.com",
                UserName = "testeManager",
                Biography = "Biografia Manager",
                Curriculum = "Curriculum Manager",
            };

            TaskLancerUser freelancer = new TaskLancerUser
            {
                Email = "freelancer@email.com",
                UserName = "testeFreelancer",
                Biography = "Biografia Freelancer",
                Curriculum = "Curriculum Freelancer",
            };

            TaskLancerUser freelancer2 = new TaskLancerUser
            {
                Email = "freelancer2@email.com",
                UserName = "testeFreelancer2",
                Biography = "Biografia Freelancer2",
                Curriculum = "Curriculum Freelancer2",
            };

            DbContext.Users.AddRange(manager);
            DbContext.Users.AddRange(freelancer);
            DbContext.Users.AddRange(freelancer2);

            Task task1 = new Task()
            {
                TaskId = 1,
                Title = "Task1",
                Description = "Task1"
            };

            Task task2 = new Task()
            {
                TaskId = 2,
                Title = "Task2",
                Description = "Task2"
            };

            List<Task> tasks = new List<Task>();
            tasks.Add(task1);
            tasks.Add(task2);

            Column column1 = new Column()
            {
                ColumnId = 1,
                Name = "Teste1",
                Color = "Teste1",
                Tasks = tasks,
                TasksOrder = "1;2"
            };

            Column column2 = new Column()
            {
                ColumnId = 2,
                Name = "Teste2",
                Color = "Teste2",
                Tasks = new List<Task>(),
                TasksOrder = ""
            };

            List<Column> columns = new List<Column>();
            columns.Add(column1);
            columns.Add(column2);

            Board board = new Board()
            {
                BoardId = 1,
                ProjectId = 1,
                Columns = columns,
                ColumnsOrder = "1;2",
            };

            Project project = new Project()
            {
                ProjectId = 1,
                Name = "Teste",
                Description = "Teste",
                IsPublic = false,
                Manager = manager,
                Board = board,

            };



            DbContext.Project.AddRange(project);

            DbContext.CategoryTag.AddRange(
                new CategoryTag { Name = "Web Application" },
                new CategoryTag { Name = "Media Campaign" },
                new CategoryTag { Name = "Music" },
                new CategoryTag { Name = "Publishing" },
                new CategoryTag { Name = "Art" }
            );

            Proposal proposal = new Proposal()
            {
                Manager = manager,
                Description = "description_task2",
                Amount = (float)19.99,
                DeliveryDate = new DateTime(2022, 05, 01),
                ReferenceFiles = new List<File>()
            };

            List<CounterProposal> counterProposals = new List<CounterProposal>();

            CounterProposal counterProposal = new CounterProposal()
            {
                Freelancer = freelancer2,
                SelfIntro = "selfIntro2",
                Amount = (float)17.99,
                DeliveryDate = new DateTime(2022, 06, 02),
            };

            counterProposals.Add(counterProposal);

            ContractingProcess cp = new ContractingProcess()
            {
                Task = task1,
                State = ContractingProcessState.Published,
                PublishingDate = DateTime.Now,
                CounterProposalsEndDate = new DateTime(2022, 04, 20),
                Proposal = proposal,
                CounterProposals =  counterProposals
            };

            DbContext.ContractingProcess.AddRange(cp);
        }

        public void Dispose()
        {
            DbContext.Dispose();
        }

    }
}
