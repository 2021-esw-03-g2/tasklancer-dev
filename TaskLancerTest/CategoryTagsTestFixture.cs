﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskLancer.Models.Project;
using TaskLancer.Models.Project.DAL;

namespace TaskLancerTest
{
    public class CategoryTagsTestFixture : ApplicationDbContextFixture
    {
        protected override void AddObjects()
        {
            DbContext.CategoryTag.AddRange(
                new CategoryTag { Name = "Music"},
                new CategoryTag { Name = "Games"},
                new CategoryTag { Name = "Video"},
                new CategoryTag { Name = "Web"},
                new CategoryTag { Name = "Mobile"}
            );
        }
    }
}

