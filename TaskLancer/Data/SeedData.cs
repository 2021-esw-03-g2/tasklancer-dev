﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using TaskLancer.Areas.Identity.Data;
using TaskLancer.Models;
using TaskLancer.Models.Contracting.DAL;
using System.Text.Json;
using System.Text.Json.Serialization;
using TaskLancer.Models.Payments.DAL;
using TaskLancer.Models.Notifications.DAL;
using TaskLancer.Models.Project.DAL;

namespace TaskLancer.Data
{
    public static class SeedData
    {
        public static async System.Threading.Tasks.Task Seed(UserManager<TaskLancerUser> userManager, RoleManager<IdentityRole> roleManager, TaskLancerContext context)
        {
            await SeedRolesAsync(roleManager);
            await SeedUsersAsync(userManager, context);
            await SeedTags(context);
            await SeedProjectAsync(context);
            await SeedPayments(context);
            //await SeedContracting(context);
        }

        private static async System.Threading.Tasks.Task SeedRolesAsync(RoleManager<IdentityRole> roleManager)
        {
            var userRole = new IdentityRole("moderator");
            if (!await roleManager.RoleExistsAsync(userRole.Name))
            {
                await roleManager.CreateAsync(userRole);
            }

            var adminRole = new IdentityRole("administrator");
            if (!await roleManager.RoleExistsAsync(adminRole.Name))
            {
                await roleManager.CreateAsync(adminRole);
            }
        }

        private static async System.Threading.Tasks.Task SeedUsersAsync(UserManager<TaskLancerUser> userManager, TaskLancerContext context)
        {
            if (userManager.FindByNameAsync("admin@tasklancer.com").Result == null)
            {
                var admin = new TaskLancerUser { UserName = "admin@tasklancer.com", Email = "admin@tasklancer.com", FirstName= "Admin", LastName="Test" };
                var result = await userManager.CreateAsync(admin, "Teste_1234");
                if (result.Succeeded)
                {
                    var token = await userManager.GenerateEmailConfirmationTokenAsync(admin);
                    var confirmation = await userManager.ConfirmEmailAsync(admin, token);
                    await userManager.AddToRoleAsync(admin, "administrator");


                    NotificationSubscription notificationSubscription = new()
                    {
                        IsSubscribedEmail = true,
                        IsSubscribedWebPush = false,
                        User = admin
                    };


                    await context.NotificationSubscription.AddAsync(notificationSubscription);
                   

                }
            }

            if (userManager.FindByNameAsync("mod@tasklancer.com").Result == null)
            {
                var mod = new TaskLancerUser { UserName = "mod@tasklancer.com", Email = "mod@tasklancer.com", FirstName = "Mod", LastName = "Test" };
                var result = await userManager.CreateAsync(mod, "Teste_1234");
                if (result.Succeeded)
                {
                    var token = await userManager.GenerateEmailConfirmationTokenAsync(mod);
                    var confirmation =  await userManager.ConfirmEmailAsync(mod, token);
                    await userManager.AddToRoleAsync(mod, "moderator");


                    NotificationSubscription notificationSubscription = new()
                    {
                        IsSubscribedEmail = true,
                        IsSubscribedWebPush = false,
                        User = mod
                    };

                    await context.NotificationSubscription.AddAsync(notificationSubscription);
                    
                }

                
            }
        }

        private static async System.Threading.Tasks.Task SeedProjectAsync(TaskLancerContext context)
        {
            string[] usernames = new string[]
            {
                "admin@tasklancer.com",
                "mod@tasklancer.com"
            };

            foreach (string username in usernames)
            {
                TaskLancerUser user = context.Users.Single<TaskLancerUser>(u => u.UserName == username);

                var userProjects = context.Project.Where(x => x.Manager == user).ToList();

                if (userProjects.Count > 0) continue;

                Project project = new Project()
                {
                    Name = "Test Project",
                    Description = "This project is a test project. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?",
                    IsPublic = true,
                    Manager = user
                };

                var result = await context.Project.AddAsync(project);

                if (result.State == Microsoft.EntityFrameworkCore.EntityState.Added)
                {
                    await context.SaveChangesAsync();

                    project.ProjectId = result.Entity.ProjectId;

                    List<SkillTag> tags = new List<SkillTag>();

                    Models.Project.DAL.Task task1 = new Models.Project.DAL.Task()
                    {
                        Title = "Task nº1",
                        Description = "Description nº1",
                        EndDate = DateTime.Today.AddDays(2.0),
                        SkillTags = tags,
                    };

                    Models.Project.DAL.Task task2 = new Models.Project.DAL.Task()
                    {
                        Title = "Task nº2",
                        Description = "Description nº2",
                        EndDate = DateTime.Today.AddDays(4.0),
                        SkillTags = tags,
                    };

                    Models.Project.DAL.Task task3 = new Models.Project.DAL.Task()
                    {
                        Title = "Task nº3",
                        Description = "Description nº3",
                        EndDate = DateTime.Today.AddDays(6.0),
                        SkillTags = tags,
                    };

                    Column column = new Column()
                    {
                        Name = "To Do",
                        Color = "#f44336",
                        Tasks = new List<Models.Project.DAL.Task>()
                        {
                            task1, task2, task3
                        }
                    };

                    Board board = new Board()
                    {
                        Project = project,
                        ColumnsOrder = "[]",
                        Columns = new List<Column>()
                        {
                            column
                        }
                    };

                    var boardResult = await context.Board.AddAsync(board);

                    //var columnResult = await context.Column.AddAsync(column);

                    //var taskResult = await context.Task.AddAsync(task1);

                    if (boardResult.State == Microsoft.EntityFrameworkCore.EntityState.Added)
                    {
                        await context.SaveChangesAsync();

                        project.Board = boardResult.Entity;

                        Column createdColumn = context.Entry(column).Entity;

                        List<int> columnIds = boardResult!.Entity!.Columns!.Select(c => c.ColumnId).ToList();

                        List<int> taskIds = createdColumn.Tasks.Select(t => t.TaskId).ToList();

                        createdColumn.TasksOrder = JsonSerializer.Serialize(taskIds);

                        boardResult.Entity.ColumnsOrder = JsonSerializer.Serialize(columnIds);

                        context.Column.Update(createdColumn);

                        foreach (Models.Project.DAL.Task task in createdColumn.Tasks)
                        {
                            task.Board = boardResult.Entity;
                            context.Task.Update(task);
                        }

                        var updateResult = context.Project.Update(project);


                        await context.SaveChangesAsync();
                    }


                }
            }
        }

        private static async System.Threading.Tasks.Task SeedTags(TaskLancerContext context)
        {
            List<SkillTag> tags = new List<SkillTag>() {
            
            new SkillTag { Name = "3D Design", Description = "3D model design" },
            
            new SkillTag { Name = "ASP-NET CORE", Description = "ASP-NET core and web development"},
            new SkillTag { Name = "React", Description = "Web development using React js" },
            new SkillTag { Name = "Concept Art", Description = "Concept Art designer" },
            new SkillTag { Name = "Music Production", Description = "Music producer" },
            new SkillTag { Name = "Voice Acting", Description = "Voice actor" },
            new SkillTag { Name = "UX Design", Description = "User-experience designer" },
            new SkillTag { Name = "UI Design", Description = "User-interface designer" },


        };
            foreach(SkillTag tag in tags)
            {
                if (context.SkillTag.FirstOrDefault(skilltag => skilltag.Name == tag.Name) == null)
                {
                    await context.SkillTag.AddAsync(tag);
                }
            }

            List<CategoryTag> categoryTags = new List<CategoryTag>() {

            new CategoryTag { Name = "Web Application"},
                        new CategoryTag { Name = "Media Campaign"},
            new CategoryTag { Name = "Music"},
                        new CategoryTag { Name = "Publishing"},
                                    new CategoryTag { Name = "Art"},
        };
            foreach (CategoryTag tag in categoryTags)
            {
                if (context.CategoryTag.FirstOrDefault(categoryTag => categoryTag.Name == tag.Name) == null)
                {
                    await context.CategoryTag.AddAsync(tag);
                }
            }

            context.SaveChanges();
        }

        private static async System.Threading.Tasks.Task SeedContracting(TaskLancerContext context)
        {
            string[] usernames = new string[]
            {
                "admin@tasklancer.com",
                "mod@tasklancer.com"
            };

            foreach (string username in usernames)
            {
                TaskLancerUser user = context.Users.Single<TaskLancerUser>(u => u.UserName == username);

                if(context.ContractingProcess.Include(p => p.Proposal).FirstOrDefault(p => p.Proposal.Manager == user) == null)
                {
                    Models.Project.DAL.Task? task = context.Task
                    .Include(task => task.Board)
                    .ThenInclude(b => b.Project)
                    .ThenInclude(p => p.Manager)
                    .FirstOrDefault(t => t.Board.Project.Manager == user);

                    //List<Models.Project.Task>? tasks = context.Task
                    //.Include(task => task.Board)
                    //.ThenInclude(b => b.Project)
                    //.ThenInclude(p => p.Manager)
                    //.Where(t => t.Board.Project.Manager == user).ToList();

                    if (task != null)
                    {
                        ContractingProcess contractingProcess = new ContractingProcess
                        {
                            PublishingDate = DateTime.Now,
                            State = ContractingProcessState.Published,
                            Task = task,
                            Proposal = new Proposal
                            {
                                Amount = 111.50f,
                                Description = "The proposal's description.",
                                DeliveryDate = DateTime.Now.AddDays(20),
                                Manager = user
                            }
                        };

                        context.ContractingProcess.Add(contractingProcess);
                    }
                }

                await context.SaveChangesAsync();
            }

        }

        private static async System.Threading.Tasks.Task SeedPayments(TaskLancerContext context)
        {
            TaskLancerUser admin = context.Users.Single<TaskLancerUser>(u => u.UserName == "admin@tasklancer.com");
            TaskLancerUser mod = context.Users.Single<TaskLancerUser>(u => u.UserName == "mod@tasklancer.com");


            //if (context.Payment.ToList().Count == 0)
            //{

            //    Payment payment1 = new Payment()
            //    {
            //        ServiceFee = 10,
            //        SubTotal = 100,
            //        Total = 110,
            //        Currency = "EUR",
            //        State = PaymentState.Pending_Payment,
            //        Title = "Pending Payment 1",
            //        Description = "Pending Payment 1 description",
            //        FreeLancer = mod,
            //        ProjectManager = admin,
            //    };

            //    Payment payment2 = new Payment()
            //    {
            //        ServiceFee = 20,
            //        SubTotal = 200,
            //        Total = 220,
            //        Currency = "EUR",
            //        State = PaymentState.Pending_Payment,
            //        Title = "Pending Payment 2",
            //        Description = "Pending Payment 2 description",
            //        FreeLancer = mod,
            //        ProjectManager = admin,
            //    };

            //    Payment payment3 = new Payment()
            //    {
            //        ServiceFee = 30,
            //        SubTotal = 300,
            //        Total = 330,
            //        Currency = "EUR",
            //        State = PaymentState.Pending_Payment,
            //        Title = "Pending Payment 3",
            //        Description = "Pending Payment 3 description",
            //        FreeLancer = mod,
            //        ProjectManager = admin,
            //    };

            //    List<Payment> list = new List<Payment>();
            //    list.Add(payment1);
            //    list.Add(payment2);
            //    list.Add(payment3);

            if(!context.UserPayment.Any(up => up.User == admin))
            {
                UserPayment userPayment = new UserPayment()
                {
                    User = admin,
                    Payments = new List<Payment>(),
                    paypalEmail = "sb-16jwv14356718@personal.example.com",
                };

                await context.AddAsync(userPayment);

            }

            if (!context.UserPayment.Any(up => up.User == mod))
            {
                UserPayment userPayment = new UserPayment()
                {
                    User = mod,
                    Payments = new List<Payment>(),
                    paypalEmail = "sb-16jwv14356718@personal.example.com",
                };

                await context.AddAsync(userPayment);

            }


                await context.SaveChangesAsync();
            


        }
    }
}
