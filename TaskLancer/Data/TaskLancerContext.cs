﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using TaskLancer.Areas.Identity.Data;
using TaskLancer.Models;
using TaskLancer.Models.Contracting.DAL;
using TaskLancer.Models.Rating.DAL;
using TaskLancer.Models.Payments.DAL;
using TaskLancer.Models.Notifications.DAL;
using TaskLancer.Models.Project.DAL;

namespace TaskLancer.Data;

public class TaskLancerContext : IdentityDbContext<TaskLancerUser>
{
    public TaskLancerContext(DbContextOptions<TaskLancerContext> options)
        : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        // Customize the ASP.NET Identity model and override the defaults if needed.
        // For example, you can rename the ASP.NET Identity table names and more.
        // Add your customizations after calling base.OnModelCreating(builder);

        builder.Entity<Models.FileTransfer.DAL.File>()
               .HasMany(f => f.UsersAllowedAccess)
               .WithMany(u => u.FilesAllowedAccess)
               .UsingEntity(fu => fu.ToTable("UserFileAllowedAccess"));


        builder.Entity<Models.FileTransfer.DAL.File>()
               .HasOne<TaskLancerUser>(f => f.Owner)
               .WithMany()
               .OnDelete(DeleteBehavior.NoAction);

        builder.Entity<Models.FileTransfer.DAL.File>()
               .HasIndex(f => f.FileId)
               .IsUnique(true);

        builder.Entity<ContractingProcess>()
            .HasMany(contractingProcess => contractingProcess.CounterProposals)
            .WithOne(counterProposal => counterProposal.ContractingProcess);

        builder.Entity<ContractingProcess>()
            .HasOne(contractingProcess => contractingProcess.Proposal)
            .WithOne(proposal => proposal.ContractingProcess);

        builder.Entity<ContractingProcess>()
            .HasOne(contractingProcess => contractingProcess.AcceptedCounterProposal)
            .WithOne();

        builder.Entity<Delivery>()
            .HasMany(d => d.SampleFiles)
            .WithMany(f => f.DeliveriesAsSample)
            .UsingEntity(pf => pf.ToTable("DeliverySampleFiles"));

        builder.Entity<Delivery>()
            .HasMany(d => d.FinalFiles)
            .WithMany(f => f.DeliveriesAsFinal)
            .UsingEntity(pf => pf.ToTable("DeliveryFinalFiles"));

        builder.Entity<Proposal>()
            .HasMany(p => p.ReferenceFiles)
            .WithMany(f => f.Proposals)
            .UsingEntity(pf => pf.ToTable("ProposalFiles"));

        builder.Entity<ContractingEvent>()
               .HasOne<ContractingProcess>(p => p.ContractingProcess)
               .WithMany()
               .OnDelete(DeleteBehavior.NoAction);

        //builder.Entity<Payment>()
        //    .HasOne(p => p.ProjectManager)
        //    .WithMany(up => up.Payments);

        builder.Entity<Review>()
            .HasOne<TaskLancerUser>(r => r.Author)
            .WithMany()
            .OnDelete(DeleteBehavior.NoAction);

        builder.Entity<Review>()
            .HasOne<TaskLancerUser>(r => r.Subject)
            .WithMany()
            .OnDelete(DeleteBehavior.NoAction);

        builder.Entity<Review>()
            .HasOne<TaskLancerUser>(r => r.Freelancer)
            .WithMany()
            .OnDelete(DeleteBehavior.NoAction);

        builder.Entity<Review>()
            .HasOne<TaskLancerUser>(r => r.Manager)
            .WithMany()
            .OnDelete(DeleteBehavior.NoAction);

        builder.Entity<Review>()
            .HasOne<ContractingProcess>(r => r.ContractingProcess)
            .WithMany()
            .OnDelete(DeleteBehavior.NoAction);

        builder.Entity<NotificationSubscription>()
            .HasMany<PushSubscription>(ns => ns.WebPushSubscriptions)
            .WithOne(ps => ps.NotificationSubscription)
            .OnDelete(DeleteBehavior.NoAction);

        base.OnModelCreating(builder);
    }

    public DbSet<SkillTag> SkillTag { get; set; }

    public DbSet<CategoryTag> CategoryTag { get; set; }

    public DbSet<NotificationSubscription> NotificationSubscription { get; set; }

    public DbSet<Notification> Notification { get; set; }

    public DbSet<Board> Board { get; set; } = null!;

    public DbSet<Project> Project { get; set; }

    public DbSet<Models.Project.DAL.Task> Task { get; set; }

    public DbSet<Column> Column { get; set; }

    public DbSet<Payment> Payment { get; set; }

    public DbSet<UserPayment> UserPayment { get; set; }

    public DbSet<Models.FileTransfer.DAL.File> File { get; set; }

    public DbSet<ContractingProcess> ContractingProcess { get; set; }

    public DbSet<ContractingEvent> ContractingEvents { get; set; }

    public DbSet<Review> Reviews { get; set; }
        
}
