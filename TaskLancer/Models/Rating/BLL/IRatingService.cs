﻿using TaskLancer.Models.Rating.DAL;

namespace TaskLancer.Models.Rating.BLL
{
    public interface IRatingService
    {
        Task<List<Review>> CreateContractingProcessReviews(int contractingProcessId, string managerId, string freelancerId);

        Task<Review> SubmitReview(int contractingProcessId, string authenticatedUserId, string comment, double score);

        Task<Review> GetReview(int reviewId);

        Task<Review> GetReview(int contractingProcessId, string userId);

        Task<Review> GetReviewReceived(int contractingProcessId, string userId);

        Task<double> GetManagerRating(string userId);

        Task<double> GetFreelancerRating(string userId);

        Task<List<Review>> GetContractingProcessReviews(int contractingProcessId);

        Task<List<Review>> GetReviewsReceivedAsManager(string managerId);

        Task<List<Review>> GetReviewsReceivedAsFreelancer(string freelancerId);

        Task<List<Review>> GetReviewsSubmittedAsManager(string managerId);

        Task<List<Review>> GetReviewsSubmittedAsFreelancer(string freelancerId);

        Task<List<Review>> GetUserPendingReviews(string userId);
    }
}
