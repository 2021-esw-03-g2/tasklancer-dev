﻿namespace TaskLancer.Models.Rating.BLL
{
    public class ListOfReviewsViewModel
    {
        public List<ReviewViewModel> ReviewsAsManager { get; set; }
        public List<ReviewViewModel> ReviewsAsFreelancer { get; set; }
        public string RadioButtonGroupName { get; set; }
    }
}
