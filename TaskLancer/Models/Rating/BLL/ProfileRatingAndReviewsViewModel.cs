﻿namespace TaskLancer.Models.Rating.BLL
{
    public class ProfileRatingAndReviewsViewModel
    {
        public double ManagerRating { get; set; }
        public double FreelancerRating { get; set; }
        public List<ReviewViewModel> ReviewsReceivedAsManager { get; set; }
        public List<ReviewViewModel> ReviewsReceivedAsFreelancer { get; set; }
        public List<ReviewViewModel> ReviewsSubmitedAsManager { get; set; }
        public List<ReviewViewModel> ReviewsSubmitedAsFreelancer { get; set; }
        public List<ReviewViewModel> PendingReviews { get; set; }
    }
}
