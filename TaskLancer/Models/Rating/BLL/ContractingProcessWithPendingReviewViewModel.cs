﻿namespace TaskLancer.Models.Rating.BLL
{
    public class ContractingProcessWithPendingReviewViewModel
    {
        public int ContractingProcessId { get; set; }
        public string TaskTitle { get; set; }
        public string TaskDescription { get; set; }
        public string ManagerUsername { get; set; }
    }
}
