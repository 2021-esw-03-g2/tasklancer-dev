﻿using System.ComponentModel.DataAnnotations;
using TaskLancer.Models.Rating.DAL;

namespace TaskLancer.Models.Rating.BLL
{
    public class ReviewViewModel
    {
        public int Id { get; set; }
        public DateTime CreateOn { get; set; }
        public ReviewState State { get; set; }
        public int ContractingProcessId { get; set; }
        public string? AuthorId { get; set; }
        public string? AuthorUsername { get; set; }
        public string? SubjectId { get; set; }
        public string? SubjectUsername { get; set; }
        public string? FreelancerId { get; set; }
        public string? ManagerId { get; set; }
        public string? Comment { get; set; }
        public string? Score { get; set; }
        public DateTime? SubmittedOn { get; set; }

        public bool IsAuthor { get; set; }
    }
}
