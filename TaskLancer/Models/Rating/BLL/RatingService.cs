﻿using Microsoft.EntityFrameworkCore;
using TaskLancer.Areas.Identity.Data;
using TaskLancer.Data;
using TaskLancer.Models.Contracting.DAL;
using TaskLancer.Models.Rating.DAL;

namespace TaskLancer.Models.Rating.BLL
{
    public class RatingService : IRatingService
    {
        private readonly TaskLancerContext _context;

        public RatingService(TaskLancerContext context)
        {
            _context = context;     
        }

        public async Task<List<Review>> CreateContractingProcessReviews(int contractingProcessId, string managerId, string freelancerId)
        {
            ContractingProcess contractingProcess = await _context.ContractingProcess.FirstOrDefaultAsync(c => c.Id == contractingProcessId);
            if (contractingProcess == null)
            {
                throw new ArgumentException("Contracting process ID invalid");
            }

            if(contractingProcess.State != ContractingProcessState.Completed)
            {
                throw new InvalidOperationException("This contracting process is not in the state of completed");
            }

            TaskLancerUser manager = await _context.Users.FirstOrDefaultAsync(u => u.Id == managerId);

            if (manager == null)
            {
                throw new ArgumentException("Manager ID invalid");
            }

            TaskLancerUser? freelancer = await _context.Users.FirstOrDefaultAsync(u => u.Id == freelancerId);

            if (freelancer == null)
            {
                throw new ArgumentException("Freelancer ID invalid");
            }

            Review managerReview = new Review()
            {
                Manager = manager,
                ManagerId = managerId,
                Freelancer = freelancer,
                FreelancerId = freelancerId,
                Author = manager,
                AuthorId = managerId,
                Subject = freelancer,
                SubjectId = freelancerId,
                CreateOn = DateTime.UtcNow,
                ContractingProcess = contractingProcess,
                ContractingProcessId = contractingProcess.Id,
                State = ReviewState.Pending
            };

            Review freelancerReview = new Review()
            {
                Manager = manager,
                ManagerId = managerId,
                Freelancer = freelancer,
                FreelancerId = freelancerId,
                Author = freelancer,
                AuthorId = freelancerId,
                Subject = manager,
                SubjectId = managerId,
                CreateOn = DateTime.UtcNow,
                ContractingProcess = contractingProcess,
                ContractingProcessId = contractingProcess.Id,
                State = ReviewState.Pending
            };

            List<Review> reviews = new List<Review>();
            reviews.Add(managerReview);
            reviews.Add(freelancerReview);

            _context.Add(managerReview);
            _context.Add(freelancerReview);

            await _context.SaveChangesAsync();

            return reviews;
        }

        public async Task<Review> SubmitReview(int contractingProcessId, string authenticatedUserId, string comment, double score)
        {
            ContractingProcess contractingProcess = await _context.ContractingProcess.FirstOrDefaultAsync(c => c.Id == contractingProcessId);
            if (contractingProcess == null)
            {
                throw new ArgumentException("Contracting process ID invalid");
            }

            Review review = await _context.Reviews.FirstOrDefaultAsync(r => r.ContractingProcessId == contractingProcessId && r.AuthorId == authenticatedUserId);
            if (review == null)
            {
                throw new ArgumentException("There is no review with author ID equal to authenticated user ID");
            }

            if (review.State == ReviewState.Submitted)
            {
                throw new InvalidOperationException("This review has already been submited");
            }

            review.Comment = comment;
            review.Score = score;
            review.State = ReviewState.Submitted;
            review.SubmittedOn = DateTime.UtcNow;

            _context.Update(review);

            await _context.SaveChangesAsync();

            return review;
        }

        public async Task<Review> GetReview(int reviewId)
        {
            Review review = await _context.Reviews.FirstOrDefaultAsync(r => r.Id == reviewId);
            if (review == null)
            {
                throw new ArgumentException("Review Id invalid");
            }

            return review;
        }

        public async Task<Review> GetReview(int contractingProcessId, string userId)
        {
            ContractingProcess contractingProcess = await _context.ContractingProcess.FirstOrDefaultAsync(c => c.Id == contractingProcessId);
            if(contractingProcess == null)
            {
                throw new ArgumentException("Contracting process ID invalid");
            }

            Review review = await _context.Reviews.FirstOrDefaultAsync(r => r.ContractingProcessId == contractingProcessId && r.AuthorId == userId);

            if (review == null)
            {
                throw new ArgumentException("There is no review for this user");
            }

            return review;
        }

        public async Task<Review> GetReviewReceived(int contractingProcessId, string userId)
        {
            ContractingProcess contractingProcess = await _context.ContractingProcess.FirstOrDefaultAsync(c => c.Id == contractingProcessId);
            if (contractingProcess == null)
            {
                throw new ArgumentException("Contracting process ID invalid");
            }

            Review review = await _context.Reviews.FirstOrDefaultAsync(r => r.ContractingProcessId == contractingProcessId && r.SubjectId == userId);

            if (review == null)
            {
                throw new ArgumentException("There is no review for this user");
            }

            return review;
        }

        public async Task<double> GetManagerRating(string userId) {
            TaskLancerUser user = await _context.Users.FirstOrDefaultAsync(u => u.Id == userId);

            if (user == null)
            {
                throw new ArgumentException("User ID invalid");
            }

            List<Review> reviewsSubmitedAsManager = await _context.Reviews.Where(r => r.ManagerId == userId && r.SubjectId == userId && r.State == ReviewState.Submitted).ToListAsync();

            return GetAverageRating(reviewsSubmitedAsManager);
        }

        public async Task<double> GetFreelancerRating(string userId)
        {
            TaskLancerUser user = await _context.Users.FirstOrDefaultAsync(u => u.Id == userId);

            if (user == null)
            {
                throw new ArgumentException("User ID invalid");
            }

            List<Review> reviewsReceivedAsFreelancer = await _context.Reviews.Where(r => r.FreelancerId == userId && r.SubjectId == userId && r.State == ReviewState.Submitted).ToListAsync();

            return GetAverageRating(reviewsReceivedAsFreelancer);
        }

        private double GetAverageRating(List<Review> reviews)
        {
            double totalScore = 0;

            foreach (Review review in reviews)
            {
                totalScore += (double)review.Score;
            }

            return totalScore / reviews.Count;
        }

        public async Task<List<Review>> GetContractingProcessReviews(int contractingProcessId)
        {
            ContractingProcess contractingProcess = await _context.ContractingProcess.FirstOrDefaultAsync(c => c.Id == contractingProcessId);
            if (contractingProcess == null)
            {
                throw new ArgumentException("Contracting process ID invalid");
            }

            List<Review> reviews = await _context.Reviews.Where(r => r.ContractingProcessId == contractingProcessId).ToListAsync();

            if(reviews.Count == 0)
            {
                throw new InvalidOperationException("The contracting process is not in a state where evaluations exists");
            }

            return reviews;
        }

        public async Task<List<Review>> GetReviewsReceivedAsManager(string managerId)
        {
            TaskLancerUser manager = await _context.Users.FirstOrDefaultAsync(u => u.Id == managerId);
            if(manager == null)
            {
                throw new ArgumentException("Invalid user ID");
            }

            List<Review> reviews = await _context.Reviews
                .Include(r => r.Author)
                .Include(r => r.Manager)
                .Include(r => r.Freelancer)
                .Include(r => r.Subject)
                .Where(r => r.ManagerId == managerId && r.SubjectId == managerId && r.State == ReviewState.Submitted).ToListAsync();

            return reviews;

        }

        public async Task<List<Review>> GetReviewsReceivedAsFreelancer(string freelancerId)
        {
            TaskLancerUser freelancer = await _context.Users.FirstOrDefaultAsync(u => u.Id == freelancerId);
            if (freelancer == null)
            {
                throw new ArgumentException("Invalid user ID");
            }

            List<Review> reviews = await _context.Reviews
                .Include(r => r.Author)
                .Include(r=> r.Manager)
                .Include(r => r.Freelancer)
                .Include(r => r.Subject)
                .Where(r => r.FreelancerId == freelancerId && r.SubjectId == freelancerId && r.State == ReviewState.Submitted).ToListAsync();

            return reviews;
        }

        public async Task<List<Review>> GetReviewsSubmittedAsManager(string managerId)
        {
            TaskLancerUser manager = await _context.Users.FirstOrDefaultAsync(u => u.Id == managerId);
            if (manager == null)
            {
                throw new ArgumentException("Invalid user ID");
            }

            List<Review> reviews = await _context.Reviews
                .Include(r => r.Author)
                .Include(r => r.Manager)
                .Include(r => r.Freelancer)
                .Include(r => r.Subject)
                .Where(r => r.ManagerId == managerId && r.AuthorId == managerId && r.State == ReviewState.Submitted).ToListAsync();

            return reviews;
        }

        public async Task<List<Review>> GetReviewsSubmittedAsFreelancer(string freelancerId)
        {
            TaskLancerUser freelancer = await _context.Users.FirstOrDefaultAsync(u => u.Id == freelancerId);
            if (freelancer == null)
            {
                throw new ArgumentException("Invalid user ID");
            }

            List<Review> reviews = await _context.Reviews
                .Include(r => r.Author)
                .Include(r => r.Manager)
                .Include(r => r.Freelancer)
                .Include(r => r.Subject)
                .Where(r => r.FreelancerId == freelancerId && r.AuthorId == freelancerId && r.State == ReviewState.Submitted).ToListAsync();

            return reviews;
        }

        public async Task<List<Review>> GetUserPendingReviews(string userId)
        {
            TaskLancerUser user = await _context.Users.FirstOrDefaultAsync(u => u.Id == userId);
            if (user == null)
            {
                throw new ArgumentException("Invalid user ID");
            }

            List<Review> reviews = await _context.Reviews.Include(r => r.Manager).Include(r => r.ContractingProcess).ThenInclude(c => c.Task).Where(r =>r.State == ReviewState.Pending && r.AuthorId == userId).ToListAsync();

            return reviews;
        }

        
    }
}
