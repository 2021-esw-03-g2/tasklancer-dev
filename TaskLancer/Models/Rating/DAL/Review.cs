﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TaskLancer.Areas.Identity.Data;
using TaskLancer.Models.Contracting.DAL;

namespace TaskLancer.Models.Rating.DAL
{
    public class Review
    {
        [Key]
        public int Id { get; set; }
        
        [Required]
        public DateTime CreateOn { get; set; }

        [Required]
        public ReviewState State { get; set; }

        [ForeignKey("ContractingProcess")]
        [Required]
        public int ContractingProcessId { get; set; }
        public ContractingProcess? ContractingProcess { get; set; }

        [ForeignKey("Author")]
        [Required]
        public string? AuthorId { get; set; }
        public TaskLancerUser? Author { get; set; }

        [ForeignKey("Subject")]
        [Required]
        public string? SubjectId { get; set; }
        public TaskLancerUser? Subject { get; set; }

        [ForeignKey("Freelancer")]
        [Required]
        public string? FreelancerId { get; set; }
        public TaskLancerUser? Freelancer { get; set; }

        [ForeignKey("Manager")]
        [Required]
        public string? ManagerId { get; set; }
        public TaskLancerUser? Manager { get; set; }

        public string? Comment { get; set; }

        [Range(0.0f, 5.0f)]
        public double? Score { get; set; }

        public DateTime? SubmittedOn { get; set; }

    }
}
