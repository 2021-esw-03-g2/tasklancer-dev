﻿namespace TaskLancer.Models.Rating.DAL
{
    public enum ReviewState
    {
        Pending,
        Submitted
    }
}