﻿using TaskLancer.Areas.Identity.Data;
using TaskLancer.Models.Notifications.DAL;

namespace TaskLancer.Models.Notifications.BLL
{
    public interface INotificationService
    {
        Task<int> DeleteNotification(TaskLancerUser user, int notificationId);

        Task<int> SetNotificationStatus(TaskLancerUser user, int notificationId);

        Task<int> SendNotification(TaskLancerUser user, string description, string content, string link);

        List<Notification> GetNotificationsAsync(TaskLancerUser user);

        List<Notification> GetUnseenNotifications(TaskLancerUser user);

        List<Notification> GetSeenNotifications(TaskLancerUser user);

        Task<int> CreateNotificationSubscription(string userId);

        Task<int> SaveWebPushSubscription(string userId, string endpoint, string p256dh, string auth);

        Task SendPushNotification(string userId, Notification notification);
    }
}
