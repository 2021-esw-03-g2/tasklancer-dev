﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.EntityFrameworkCore;
using TaskLancer.Areas.Identity.Data;
using TaskLancer.Data;
using TaskLancer.Models.Notifications.DAL;

namespace TaskLancer.Models.Notifications.BLL
{
    public class EmailNotificationService : IEmailNotificationService
    {
        private readonly TaskLancerContext _context;
        private readonly IEmailSender _emailSender;
        private readonly UserManager<TaskLancerUser> _userManager;

        public EmailNotificationService(TaskLancerContext context, IEmailSender EmailSender, UserManager<TaskLancerUser> userManager)
        {
            _context = context;
            _emailSender = EmailSender;
            _userManager = userManager;
        }

        /// <summary>
        /// This method checks if a certain user is subscribed to email Notifications
        /// </summary>
        /// <param name="user">User</param>
        /// <returns>True if user is subscribed, false if user is unsubscribed</returns>
        /// <exception cref="ArgumentException">In case the Notification Subscription doesn't exist</exception>
        public async Task<bool> IsSubscribedEmail(TaskLancerUser user)
        {
            NotificationSubscription? notificationSubscription = await _context.NotificationSubscription.Where(e => e.User == user).FirstOrDefaultAsync();

            if (notificationSubscription == null)
            {
                throw new ArgumentException("NotificationSubscription from user not found.");
            }

            return notificationSubscription.IsSubscribedEmail;
        }

        /// <summary>
        /// This method sends an enail notification to a user
        /// </summary>
        /// <param name="user">User</param>
        /// <param name="notification">Notificaation to be sent</param>
        /// <returns>The number of entries in the database</returns>
        public async Task<int> SendEmailNotification(TaskLancerUser user, Notification notification)
        {
            var email = await _userManager.GetEmailAsync(user);

            await _emailSender.SendEmailAsync(
                email,
                notification.Description,
                notification.Content
                );
            return await _context.SaveChangesAsync();
        }

        /// <summary>
        /// This method updates the notificaitonSubscription isEmailSubscribed value
        /// </summary>
        /// <param name="user">User</param>
        /// <param name="isEmailSubscribed">email subscription value</param>
        /// <returns>The number of entries in the database</returns>
        /// <exception cref="ArgumentException">In case the Notification Subscription doesn't exist</exception>
        public Task<int> SetEmailSubscription(TaskLancerUser user, bool isEmailSubscribed)
        {
            var notificationSubscription = _context.NotificationSubscription.Where(e => e.User == user).FirstOrDefault();

            if (notificationSubscription == null)
            {
                throw new ArgumentException("NotificationSubscription from user not found.");
            }

            notificationSubscription.IsSubscribedEmail = isEmailSubscribed;
            _context.NotificationSubscription.Update(notificationSubscription);

            return _context.SaveChangesAsync();
        }
    }
}
