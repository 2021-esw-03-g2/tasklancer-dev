﻿using TaskLancer.Areas.Identity.Data;
using TaskLancer.Data;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using WebPush;
using System.Text.Json;
using TaskLancer.Models.Notifications.DAL;

namespace TaskLancer.Models.Notifications.BLL
{
    public class NotificationService : INotificationService
    {
        private readonly TaskLancerContext _context;
        private readonly IEmailNotificationService _emailNotificationService;

        public NotificationService(TaskLancerContext ctx, IEmailNotificationService emailNotificationService)
        {
            _context = ctx;
            _emailNotificationService = emailNotificationService;
        }

        /// <summary>
        /// This method returns the user NotificationSubscription info
        /// </summary>
        /// <param name="userId">User id</param>
        /// <returns>The user NotificationSubscription info</returns>
        private async Task<NotificationSubscription> GetUserNotificationSubscription(string userId)
        {
            return await _context.NotificationSubscription.Include(n => n.Notifications).Include(n => n.User).Where(e => e.User.Id == userId).Include(n => n.WebPushSubscriptions).FirstOrDefaultAsync();
        }


        /// <summary>
        /// This method creates a NotificationsSubscription to the inserted user 
        /// </summary>
        /// <param name="userId">User id</param>
        /// <returns>Number of entries in the data base</returns>
        /// <exception cref="ArgumentException">If the user cant be found with the provided id</exception>
        public async Task<int> CreateNotificationSubscription(string userId)
        {
            TaskLancerUser user = await _context.Users.FirstOrDefaultAsync(u => u.Id == userId);

            if (user == null)
            {
                throw new ArgumentException("User could not be found.");
            }


            NotificationSubscription notificationSubscription = new NotificationSubscription
            {
                User = user,
                IsSubscribedEmail = true,
                IsSubscribedWebPush = false
            };

            _context.NotificationSubscription.Add(notificationSubscription);

            return await _context.SaveChangesAsync();
        }

        /// <summary>
        /// This method deletes a notification
        /// </summary>
        /// <param name="userId">User id</param>
        /// <returns>Number of entries in the data base</returns>
        /// <exception cref="ArgumentException">If the user cant be found with the provided id</exception>
        /// <exception cref="ArgumentException">If the notifications list is null</exception>

        public async Task<int> DeleteNotification(TaskLancerUser user, int notificationId)
        {

            NotificationSubscription notificationSubscription = await GetUserNotificationSubscription(user.Id);

            if (notificationSubscription == null)
            {
                throw new ArgumentException("NotificationSubscription with requested user not found.");
            }

            Notification notification = notificationSubscription.Notifications.Where(noti => noti.NotificationId == notificationId).First();

            if (notification == null)
            {
                throw new ArgumentException("Notifications not found.");
            }

            notificationSubscription.Notifications.Remove(notification);

            _context.NotificationSubscription.Update(notificationSubscription);
            _context.Notification.Remove(notification);

            return await _context.SaveChangesAsync();
        }

        /// <summary>
        /// This method returns a list of all the notifications from a certain user
        /// </summary>
        /// <param name="user">user</param>
        /// <returns>A list of notifications from the inserted user</returns>
        /// <exception cref="ArgumentException">If the NotificationSubscription is null</exception>
        public List<Notification> GetNotificationsAsync(TaskLancerUser user)
        {
            var userNotifications = _context.NotificationSubscription.Where(e => e.User == user).Include(x => x.Notifications).FirstOrDefault();

            if (userNotifications == null)
            {
                throw new ArgumentException("Notifications Subscriptions with requested user not found.");
            }

            var notifications = userNotifications.Notifications.ToList().OrderByDescending(noti => noti.DateTime).ToList();

            return notifications;
        }

        /// <summary>
        /// This method returns a list of all the unseen notifications from a certain user
        /// </summary>
        /// <param name="user">user</param>
        /// <returns>A list of notifications from the inserted user</returns>
        /// <exception cref="ArgumentException">If the NotificationSubscription is null</exception>
        /// <exception cref="ArgumentException">If the notifications is null</exception>
        public List<Notification> GetUnseenNotifications(TaskLancerUser user)
        {
            var userNotifications = _context.NotificationSubscription.Where(e => e.User == user).Include(x => x.Notifications).FirstOrDefault();

            if (userNotifications == null)
            {
                throw new ArgumentException("NotificationSubscription with requested user not found.");
            }

            var notifications = userNotifications.Notifications.ToList().OrderByDescending(noti => noti.DateTime).ToList();

            if (notifications == null)
            {
                throw new ArgumentException("Notifications not found.");
            }

            foreach (var notif in notifications.ToList())
            {
                if (notif.IsSeen == true)
                {
                    notifications.Remove(notif);
                }
            }

            return notifications;
        }

        /// <summary>
        /// This method sends a notification to a certain user
        /// </summary>
        /// <param name="user">user</param>
        /// <param name="description">String description</param>
        /// <param name="content">String content</param>
        /// <param name="link">String link</param>
        /// <returns>Number of entries in the data base</returns>
        /// <exception cref="ArgumentException">If the NotificationSubscription is null</exception>
        public async Task<int> SendNotification(TaskLancerUser user, string description, string content, string link)
        {
            NotificationSubscription? notificationSubscription = await GetUserNotificationSubscription(user.Id);

            if (notificationSubscription == null)
            {
                throw new ArgumentException("NotificationSubscription not found.");
            }

            Notification notification = new Notification
            {
                IsSeen = false,
                Description = description,
                Content = content,
                Link = link,
                DateTime = DateTime.Now,
            };

            notificationSubscription.Notifications.Add(notification);

            if (notificationSubscription.IsSubscribedEmail == true)
            {
                await _emailNotificationService.SendEmailNotification(user, notification);
            }

            if (notificationSubscription.IsSubscribedWebPush == true)
            {
                // metodo enviar webpush
            }

            await SendPushNotification(user.Id, notification);

            return await _context.SaveChangesAsync();
        }

        /// <summary>
        /// This method updates the notification status
        /// </summary>
        /// <param name="user">User</param>
        /// <param name="notificationId">notification Id</param>
        /// <returns>Number of entries in the data base</returns>
        /// <exception cref="ArgumentException">If the notifications list is null</exception>
        /// <exception cref="ArgumentException">If the notification is null</exception>

        public async Task<int> SetNotificationStatus(TaskLancerUser user, int notificationId)
        {
            List<Notification> notifications = GetNotificationsAsync(user);

            if (notifications == null)
            {
                throw new ArgumentException("Notifications from user not found.");
            }

            Notification notification = notifications.Find(noti => noti.NotificationId == notificationId);

            if (notification == null)
            {
                throw new ArgumentException("Notification not found.");
            }

            notification.IsSeen = true;
            _context.Notification.Update(notification);

            return await _context.SaveChangesAsync();
        }

        /// <summary>
        /// This method returns a list of all the seen notifications from a certain user
        /// </summary>
        /// <param name="user">user</param>
        /// <returns>A list of notifications from the inserted user</returns>
        /// <exception cref="ArgumentException">If the NotificationSubscription is null</exception>
        /// <exception cref="ArgumentException">If the notifications is null</exception>
        public List<Notification> GetSeenNotifications(TaskLancerUser user)
        {
            var userNotifications = _context.NotificationSubscription.Where(e => e.User == user).Include(x => x.Notifications).FirstOrDefault();

            if (userNotifications == null)
            {
                throw new ArgumentException("Notifications from user not found.");
            }

            var notifications = userNotifications.Notifications.ToList().OrderByDescending(noti => noti.DateTime).ToList();

            if (notifications == null)
            {
                throw new ArgumentException("Notifications from user not found.");
            }

            foreach (var notif in notifications.ToList())
            {
                if (notif.IsSeen == false)
                {
                    notifications.Remove(notif);
                }
            }

            return notifications;
        }

        /// <summary>
        /// Saves a web push subscription associated to a certain user
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="endpoint"></param>
        /// <param name="p256dh"></param>
        /// <param name="auth"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        public async Task<int> SaveWebPushSubscription(string userId, string endpoint, string p256dh, string auth)
        {
            var notificationsSubscription = await GetUserNotificationSubscription(userId);

            if (notificationsSubscription == null)
            {
                throw new ArgumentException("Notifications Subscriptions from user not found.");
            }


            DAL.PushSubscription pushSubscription = new DAL.PushSubscription
            {
                Endpoint = endpoint,
                P256dh = p256dh,
                Auth = auth
            };

            notificationsSubscription.WebPushSubscriptions.Add(pushSubscription);

            _context.NotificationSubscription.Update(notificationsSubscription);

            return await _context.SaveChangesAsync();
        }

        private async Task<List<DAL.PushSubscription>> GetWebPushSubscriptions(string userId)
        {
            var notificationsSubscription = await GetUserNotificationSubscription(userId);

            if (notificationsSubscription == null)
            {
                throw new ArgumentException("Notifications Subscriptions from user not found.");
            }

            return notificationsSubscription.WebPushSubscriptions.ToList();
        }

        /// <summary>
        /// Send a push notification to a certain user
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="notification"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        public async Task SendPushNotification(string userId, Notification notification)
        {
            if (userId == null)
            {
                throw new ArgumentException("Notifications Subscriptions from user not found.");
            }
            List<DAL.PushSubscription> subscriptions = await GetWebPushSubscriptions(userId);

            foreach (var subscription in subscriptions)
            {
                WebPush.PushSubscription webPushSubscription = new WebPush.PushSubscription(subscription.Endpoint, subscription.P256dh, subscription.Auth);

                if (subscription == null)
                {
                    throw new ArgumentException("Notifications Subscriptions from user not found.");
                }

                var subject = "mailto:mail@example.com";
                var publicKey = "BDCDSNTJBiu4uICZxH0lfCxBuycfxRPCMV7nOMnHMos7AKC8LAeTEItTBURAah_dqCLpgPXb_ag40o3g5xX2ZoM";
                var privateKey = "dqjMCvKRR7SPyZpn5BJsAiJ-jEqx64EoRSLrf3A9GrM";

                var vapidDetails = new VapidDetails(subject, publicKey, privateKey);

                var webPushClient = new WebPushClient();

                NotificationDTO notificationDTO = new NotificationDTO
                {
                    description = notification.Description,
                    content = notification.Content,
                    link = notification.Link
                };

                var payload = JsonSerializer.Serialize(notificationDTO);

                try
                {
                    webPushClient.SendNotification(webPushSubscription, payload, vapidDetails);
                }
                catch (Exception exception)
                {
                    // Log error
                }
            }
        }

        public class NotificationDTO
        {
            public string description { get; set; }

            public string content { get; set; }

            public string link { get; set; }
        }
    }
}
