﻿using TaskLancer.Areas.Identity.Data;
using TaskLancer.Models.Notifications.DAL;

namespace TaskLancer.Models.Notifications.BLL
{
    public interface IEmailNotificationService
    {
        Task<int> SendEmailNotification(TaskLancerUser user, Notification notification);

        Task<bool> IsSubscribedEmail(TaskLancerUser user);

        Task<int> SetEmailSubscription(TaskLancerUser user, bool isEmailSubscribed);
    }
}
