﻿using TaskLancer.Areas.Identity.Data;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Collections.Generic;

namespace TaskLancer.Models.Notifications.DAL
{
    public class NotificationSubscription
    {
        [Key]
        public int NotificationSubscriptionId { get; set; }

        [Required]
        public TaskLancerUser? User { get; set; }

        public bool IsSubscribedEmail { get; set; }

        public bool IsSubscribedWebPush { get; set; }

        public ICollection<PushSubscription>? WebPushSubscriptions { get; set; }

        public virtual ICollection<Notification>? Notifications { get; set; }
    }


}
