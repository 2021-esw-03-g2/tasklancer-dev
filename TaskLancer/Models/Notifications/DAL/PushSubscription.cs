﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TaskLancer.Models.Notifications.DAL
{
    public class PushSubscription
    {
        [Key]
        public int PushSubscriptionId { get; set; }

        [Required]
        public string? Endpoint { get; set; }

        [Required]
        public string? P256dh { get; set; }

        [Required]
        public string? Auth { get; set; }

        [ForeignKey("NotificationSubscription")]
        public int NotificationSubscriptionId { get; set; }

        public NotificationSubscription NotificationSubscription { get; set; }
    }
}
