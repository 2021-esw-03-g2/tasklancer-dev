﻿using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace TaskLancer.Models.Notifications.DAL
{
    public class Notification
    {
        [Key]
        public int NotificationId { get; set; }

        public bool IsSeen { get; set; }

        public string? Description { get; set; }

        public string? Content { get; set; }

        public string? Link { get; set; }

        public DateTime? DateTime { get; set; }
    }
}
