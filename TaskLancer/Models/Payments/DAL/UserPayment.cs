﻿using System.ComponentModel.DataAnnotations;
using TaskLancer.Areas.Identity.Data;

namespace TaskLancer.Models.Payments.DAL
{
    public class UserPayment
    {
        [Key]
        public int UserPaymentId { get; set; }

        [Required]
        public TaskLancerUser? User { get; set; }

        public string? paypalEmail { get; set; }

        public virtual ICollection<Payment>? Payments { get; set; }

    }
}
