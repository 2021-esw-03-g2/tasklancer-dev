﻿namespace TaskLancer.Models.Payments.DAL
{
    public enum PaymentState
    {
        Pending_Payment,
        Paid,
        Pending_Payout,
        Paid_Out,
        Fully_Refunded,
        Partially_Refunded,
        Canceled
    }
}
