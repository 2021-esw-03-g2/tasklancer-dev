﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using TaskLancer.Areas.Identity.Data;

namespace TaskLancer.Models.Payments.DAL
{
    public class Payment
    {
        [Key]
        public int PaymentId { get; set; }

        public float ServiceFee { get; set; }

        public float SubTotal { get; set; }

        public float Total { get; set; }

        public string? Currency { get; set; }

        public PaymentState State { get; set; }

        public DateTime? InitialDate { get; set; }

        public DateTime? LimitDate { get; set; }

        public DateTime? PaymentDate { get; set; }

        public string? Title { get; set; }

        public string? Description { get; set; }

        public TaskLancerUser? ProjectManager { get; set; }

        public TaskLancerUser? FreeLancer { get; set; }

        public string? PaypalCaptureId { get; set; }
    }

    /*
    public enum CurrencyType
    {
        EUR,
        USD,
        AUD
    }*/
}
