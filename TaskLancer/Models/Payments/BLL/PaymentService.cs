﻿using Microsoft.AspNetCore.Hosting.Server;
using Microsoft.AspNetCore.Hosting.Server.Features;
using Microsoft.EntityFrameworkCore;
using PayPalCheckoutSdk.Core;
using PayPalCheckoutSdk.Orders;
using System.Net;
using System.Text;
using System.Text.Json;
using TaskLancer.Areas.Identity.Data;
using TaskLancer.Controllers;
using TaskLancer.Data;
using TaskLancer.Models.Contracting.BLL;
using TaskLancer.Models.Contracting.DAL;
using TaskLancer.Models.Notifications.BLL;
using TaskLancer.Models.Payments.DAL;

namespace TaskLancer.Models.Payments.BLL
{
    public class PaymentService : IPaymentService
    {
        private readonly TaskLancerContext _context;
        private string _paypalEnviroment;
        private string _clientId;
        private string _secret;
        private readonly IServer _server;
        private readonly INotificationService _notificationService;

        public PaymentService(TaskLancerContext ctx, IServer server, INotificationService notificationService)
        {
            _context = ctx;
            _paypalEnviroment = "sandbox";
            _clientId = "AVi5_w2WXankIYP_w835RuK81bAvdIoNnHqXsrl2VEbcnTm6uEBITHF8fNj5s4lM4LXxXq1v7Sb250NK";
            _secret = "EAejOOOltofG4RnlmacnQYS-9zj4Hfaxdq8JD47NGitBsITdTnSiWnU3V6x3rPsk6XFtvWM9fO4FvXCC";
            _server = server;
            _notificationService = notificationService;
        }

        /// <summary>
        /// This method creates a payment
        /// </summary>
        /// <param name="userPaymentId">UserPayment id</param>
        /// <returns>Returns Id of new payment.</returns>
        /// <exception cref="ArgumentException">In case the userPayment id doesn't exist</exception>
        public async Task<int> CreatePayment(int userPaymentId, Payment payment)
        {
            UserPayment userPayment = await _context.UserPayment.Include(p => p.Payments).FirstOrDefaultAsync(p => p.UserPaymentId == userPaymentId);

            if (userPayment == null)
            {
                throw new ArgumentException("There is no UserPayment with the id received");
            }

            if (payment == null)
            {
                throw new ArgumentException("Payment cannot be null");
            }

            payment.State = PaymentState.Pending_Payment;

            if (userPayment.Payments == null)
            {
                userPayment.Payments = new List<Payment>();
            }

            var entityEntry = _context.Payment.Add(payment);

            userPayment.Payments.Add(payment);
            _context.UserPayment.Update(userPayment);
            await _context.SaveChangesAsync();

            return entityEntry.Entity.PaymentId;
        }

        public async Task<int> CreatePayment(string userId, Payment payment)
        {
            UserPayment? userPayment = await _context.UserPayment.Include(up => up.User).FirstOrDefaultAsync(up => up.User!.Id == userId);
            return await CreatePayment(userPayment!.UserPaymentId, payment);
        }

        /// <summary>
        /// This method creates a userPayment
        /// </summary>
        /// <param name="userId">User id</param>
        /// <returns>Number of entries in the data base</returns>
        /// <exception cref="ArgumentException">In case the user id doesn't exist</exception>
        public async Task<int> CreateUserPayment(string userId)
        {
            TaskLancerUser user = await _context.Users.FirstOrDefaultAsync(u => u.Id == userId);

            if (user == null)
            {
                throw new ArgumentException("User could not be found.");
            }

            UserPayment userPayment = new UserPayment
            {
                User = user,
            };

            _context.UserPayment.Add(userPayment);

            return await _context.SaveChangesAsync();
        }

        /// <summary>
        /// This method returns a list of Paid Payments
        /// </summary>
        /// <param name="User">Tasklancer User</param>
        /// <returns>List with paid payments of the user</returns>
        /// <exception cref="ArgumentException">In case the userPayment id doesn't exist</exception>
        /// <exception cref="ArgumentException">In case the Payments don't exist</exception>
        public async Task<List<Payment>> GetPaidPayments(TaskLancerUser user)
        {
            var userPayments = await _context.UserPayment.Where(e => e.User == user).Include(x => x.Payments).FirstOrDefaultAsync();

            if (userPayments == null)
            {
                throw new ArgumentException("user Payments with requested user not found.");
            }

            var payments = userPayments.Payments.Where(p => p.State == PaymentState.Paid || p.State == PaymentState.Pending_Payout).ToList();

            if (payments == null)
            {
                throw new ArgumentException("Payments not found.");
            }

            payments.OrderBy(Payment => Payment.InitialDate);

            return payments;
        }

        /// <summary>
        /// This method return a payment
        /// </summary>
        /// <param name="id">payment id</param>
        /// <returns>a paymentr</returns>
        /// <exception cref="ArgumentException">In case the Payment doesn't exist</exception>
        public async Task<Payment> GetPayment(int id)
        {
            Payment? payment = await _context.Payment.Include(p => p.FreeLancer!).Include(p => p.ProjectManager!).FirstOrDefaultAsync(p => p.PaymentId == id);

            if (payment == null)
            {
                throw new ArgumentException("Payment not found.");
            }

            return payment;
        }

        /// <summary>
        /// This method returns a list of Payments
        /// </summary>
        /// <param name="User">Tasklancer User</param>
        /// <returns>List with payments of the user</returns>
        /// <exception cref="ArgumentException">In case the Payments don't exist</exception>
        public async Task<List<Payment>> GetPayments(TaskLancerUser user)
        {
            var userPayments = await _context.UserPayment.Where(e => e.User == user).Include(x => x.Payments).FirstOrDefaultAsync();

            if (userPayments == null)
            {
                throw new ArgumentException("Use Payments with requested user not found.");
            }

            var payments = userPayments.Payments.ToList();

            payments.OrderBy(Payment => Payment.InitialDate);

            return payments;
        }

        /// <summary>
        /// This method returns a list of Payouts
        /// </summary>
        /// <param name="User">Tasklancer User</param>
        /// <returns>List with payouts of the user</returns>
        /// <exception cref="ArgumentException">In case the userPayment id doesn't exist</exception>
        /// <exception cref="ArgumentException">In case the Payments don't exist</exception>
        public async Task<List<Payment>> GetPayouts(TaskLancerUser user)
        {

            //var userPayouts = _context.Payment.Where<Payment>(e => e.FreeLancer == user).ToList().Where(p => p.State == "COMPLETED");
            var userPayouts = await _context.Payment.Where(p => p.FreeLancer == user && p.State == PaymentState.Pending_Payout).OrderBy(p => p.InitialDate).ToListAsync();

            if (userPayouts == null)
            {
                throw new ArgumentException("user Payouts with requested user not found.");
            }

            return userPayouts;
        }

        /// <summary>
        /// This method returns a list of Unpaid Payments
        /// </summary>
        /// <param name="User">Tasklancer User</param>
        /// <returns>List with paid payments of the user</returns>
        /// <exception cref="ArgumentException">In case the userPayment id doesn't exist</exception>
        /// <exception cref="ArgumentException">In case the Payments don't exist</exception>
        public async Task<List<Payment>> GetUnpaidPayments(TaskLancerUser user)
        {
            var userPayments = await _context.UserPayment.Where(e => e.User == user).Include(x => x.Payments).FirstOrDefaultAsync();

            if (userPayments == null)
            {
                throw new ArgumentException("user Payments with requested user not found.");
            }

            var payments = userPayments.Payments.Where(p => p.State == PaymentState.Pending_Payment).OrderBy(Payment => Payment.InitialDate).ToList();

            if (payments == null)
            {
                throw new ArgumentException("Payments not found.");
            }

            return payments;
        }

        /// <summary>
        /// Updates a payment
        /// </summary>
        /// <param name="payment"Payment</param>
        /// <returns>Number of entries in the data base</returns>
        /// <exception cref="ArgumentException">In case the Payment is null</exception>
        public Task<int> UpdatePayment(Payment payment)
        {
            if (payment == null)
            {
                throw new ArgumentException("Payment cannot be null");
            }

            _context.Update(payment);
            return _context.SaveChangesAsync();
        }

        public async Task<PayPalHelper.MyPaypalSetup> AprovePayment(int id, string route)
        {
            try
            {
                PayPalHelper.MyPaypalSetup payPalSetup = new PayPalHelper.MyPaypalSetup { Environment = _paypalEnviroment, ClientId = _clientId, Secret = _secret };

                Payment payment = await GetPayment(id);

                List<string> paymentResultList = new List<string>();

                //redirect URL. when approved or cancelled on PayPal, PayPal uses this URL to redirect to your app/website.
                //payPalSetup.RedirectUrl = _server.Features.Get<IServerAddressesFeature>().Addresses.First() + "/UserPayments";
                payPalSetup.RedirectUrl = _server.Features.Get<IServerAddressesFeature>().Addresses.First() + route + payment.PaymentId;
                PayPalHttp.HttpResponse response = await PayPalHelper.CreateOrder(payPalSetup, payment);

                var statusCode = response.StatusCode;
                Order result = response.Result<Order>();

                Console.WriteLine("Status: {0}", result.Status);
                Console.WriteLine("Order Id: {0}", result.Id);
                Console.WriteLine("Intent: {0}", result.CheckoutPaymentIntent);
                Console.WriteLine("Links:");

                foreach (LinkDescription link in result.Links)
                {
                    Console.WriteLine("\t{0}: {1}\tCall Type: {2}", link.Rel, link.Href, link.Method);
                    if (link.Rel.Trim().ToLower() == "approve")
                    {
                        payPalSetup.ApproveUrl = link.Href;
                    }
                }
                return payPalSetup;
            }
            catch (Exception ex)
            {
                throw new ArgumentException("There was an error in processing your payment");
            }
        }

        public async Task<PaymentViewModel> CreatePayout(int id)
        {
            PaymentViewModel paymentViewModel = new PaymentViewModel();
            paymentViewModel.ShowStatusMessage = true;
            Payment payment = await _context.Payment.Include(p => p.FreeLancer).Include(p => p.ProjectManager).FirstOrDefaultAsync(p => p.PaymentId == id);

            var receiverPaypalEmail = GetPayPalEmail(payment.FreeLancer);

            WebClient? client = new WebClient();
            client.Headers.Add("content-type", "application/json");
            client.Headers.Add("authorization", "Bearer " + await GetAccessToken());
            client.Headers.Add("accept", "application/json");


            try
            {
                var response = client.UploadString("https://api.sandbox.paypal.com/v1/payments/payouts", "POST", GenerateJSONBody(payment.Title, payment.SubTotal, receiverPaypalEmail));
                Console.WriteLine(response);

                payment.State = PaymentState.Paid_Out;
                await UpdatePayment(payment);

                paymentViewModel.StatusMessageTitle = "Payout Successful!";
                paymentViewModel.StatusMessageDescription = "Your payout related to the task '" + payment.Title + "' has been successful";
                paymentViewModel.Successful = true;

                string url = await urlForNotification(payment.PaymentId);

                await _notificationService.SendNotification(payment.FreeLancer, paymentViewModel.StatusMessageTitle, paymentViewModel.StatusMessageDescription, url);
                await _notificationService.SendNotification(payment.ProjectManager, "Payment accepted by freelancer", "The payment related to the task " + payment.Title + " has been accepted by the freelancer", url);

                return paymentViewModel;
            }
            catch (WebException e)
            {
                var errorResponse = e.Response as HttpWebResponse;
                Console.WriteLine(e.Response.Headers);
                string responseText;
                using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                {
                    responseText = reader.ReadToEnd();
                    Console.WriteLine(responseText);
                }
                paymentViewModel.StatusMessageTitle = "Payout Unsuccessful!";
                paymentViewModel.StatusMessageDescription = "There was a problem with your payout related to the task  '" + payment.Title + "'";
                paymentViewModel.Successful = false;

                return paymentViewModel;
            }


        }

        private async Task<string> GetAccessToken()
        {
            string encodedCredentials = Base64Encode(_clientId, _secret);

            var client = new WebClient();
            client.Headers.Add("authorization", "basic " + encodedCredentials);
            client.Headers.Add("content-type", "application/x-www-form-urlencoded");
            client.Headers.Add("accept-language", "en_US");
            client.Headers.Add("accept", "application/json");

            var body = "grant_type=client_credentials";

            try
            {
                var response = client.UploadString("https://api.sandbox.paypal.com/v1/oauth2/token", "POST", body);
                Console.WriteLine(GetAccessTokenFromResponse(response));

                return GetAccessTokenFromResponse(response);
            }
            catch (WebException e)
            {
                var errorResponse = e.Response as HttpWebResponse;
                Console.WriteLine(e.Response.Headers);
                string responseText;
                using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                {
                    responseText = reader.ReadToEnd();
                    Console.WriteLine(responseText);
                }
                return responseText;
            }
        }

        private string GetAccessTokenFromResponse(string response)
        {
            string[] strs = response.Split(",");
            string[] strs1 = strs[1].Split(":");

            return strs1[1].Replace("\"", "");
        }

        private static string Base64Encode(string username, string pw)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(username + ":" + pw);
            return Convert.ToBase64String(plainTextBytes);
        }

        private string GenerateJSONBody(string title, float value, string email)
        {
            var body = new
            {
                sender_batch_header = new
                {
                    //sender_batch_id = "Payouts_2018_100008",
                    email_subject = "You have a payout!",
                    email_message = "You have received a payout related to " + title + "! Thanks for using our service!"
                },
                items = new[] {
                    new
                    {
                        recipient_type = "EMAIL",
                        amount = new
                        {
                            value = value.ToString().Replace(",","."),
                            currency = "EUR"
                        },
                        note = "Thanks for your patronage!",
                        //sender_item_id = "201403140002",
                        receiver = email,

                    }
                }
            };

            string json = JsonSerializer.Serialize(body);

            return json;
        }
        /// <summary>
        /// Get paypal email associated with user.
        /// </summary>
        /// <param name="user">User</param>
        /// <returns>Email address</returns>
        public string GetPayPalEmail(TaskLancerUser user)
        {
            var userPayments = _context.UserPayment.Where(e => e.User == user).FirstOrDefault();

            return userPayments.paypalEmail;
        }


        public async Task<int> SetupPaypalEmail(TaskLancerUser user, string email)
        {
            var userPayments = await _context.UserPayment.Where(e => e.User == user).FirstOrDefaultAsync();
            userPayments.paypalEmail = email;
            _context.UserPayment.Update(userPayments);
            return await _context.SaveChangesAsync();
        }
        /// <summary>
        /// Set payment to cancelled state.
        /// </summary>
        /// <param name="paymentId">Payment ID</param>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException">Payment ID is invalid.</exception>
        public async Task CancelPayment(int paymentId)
        {
            Payment? payment = await _context.Payment.Include(p => p.FreeLancer).Include(p => p.ProjectManager).FirstOrDefaultAsync(p => p.PaymentId == paymentId);

            if (payment == null)
            {
                throw new InvalidOperationException("No payment with given ID.");
            }

            payment.State = PaymentState.Canceled;

            await UpdatePayment(payment);
        }

        /// <summary>
        /// Unlock payout to freelancer for given payment
        /// </summary>
        /// <param name="paymentId">Payment ID</param>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException">Payment invalid or payment state invalid.</exception>
        public async Task UnlockPaymentPayout(int paymentId)
        {
            Payment? payment = await _context.Payment.Include(p => p.FreeLancer).Include(p => p.ProjectManager).FirstOrDefaultAsync(p => p.PaymentId == paymentId);

            if (payment == null)
            {
                throw new InvalidOperationException("No payment with given ID.");
            }
            if (payment.State != PaymentState.Paid)
            {
                throw new InvalidOperationException("Payment must be in Paid state before unlocking freelancer payout.");
            }

            payment.State = PaymentState.Pending_Payout;
            await UpdatePayment(payment);

            string url = await urlForNotification(payment.PaymentId);


            await _notificationService.SendNotification(payment.FreeLancer, $"Payout unlocked.", $"You now have a pending payout. Check Payments to receive your earnings.", url);

        }

        /// <summary>
        /// Fully refund a paypal order.
        /// </summary>
        /// <param name="paymentId">Payment Id</param>
        /// <returns></returns>
        public async Task RefundPayment(int paymentId)
        {
            Payment? payment = await _context.Payment.Include(p => p.FreeLancer).Include(p => p.ProjectManager).FirstOrDefaultAsync(p => p.PaymentId == paymentId);

            WebClient? client = new WebClient();
            client.Headers.Add("content-type", "application/json");
            client.Headers.Add("authorization", "Bearer " + await GetAccessToken());
            client.Headers.Add("accept", "application/json");

            var body = new
            {
                amount = new
                {
                    value = payment.SubTotal,
                    currency_code = "EUR"
                },
                note_to_payer = "You have been fully refunded for a payment. Visit TaskLancer to learn more."
            };

            string json = JsonSerializer.Serialize(body);

            try
            {

                var response = client.UploadString($"https://api.sandbox.paypal.com/v2/payments/captures/{payment!.PaypalCaptureId}/refund", "POST", json);
                Console.WriteLine(response);

                payment.State = PaymentState.Fully_Refunded;
                await UpdatePayment(payment);

                string url = await urlForNotification(payment.PaymentId);

                await _notificationService.SendNotification(payment.ProjectManager!, "Payment refunded.", "The payment related to the task " + payment.Title + " has been fully refunded.", url);

            }
            catch (WebException e)
            {
                var errorResponse = e.Response as HttpWebResponse;
                Console.WriteLine(e.Response.Headers);
                string responseText;
                using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                {
                    responseText = reader.ReadToEnd();
                    Console.WriteLine(responseText);
                }

            }
        }

        /// <summary>
        /// Creates a new Paypal order.
        /// </summary>
        /// <param name="paymentId">Payment Id</param>
        /// <param name="redirectRoute">Custom URL for redirecting after payment</param>
        /// <returns>Paypal order instance</returns>
        public async Task<Order> CreateOrder(int paymentId, string redirectRoute)
        {
            PayPalHelper.MyPaypalSetup payPalSetup = new PayPalHelper.MyPaypalSetup { Environment = _paypalEnviroment, ClientId = _clientId, Secret = _secret };

            Payment payment = await GetPayment(paymentId);

            payPalSetup.RedirectUrl = _server.Features.Get<IServerAddressesFeature>().Addresses.First() + redirectRoute;
            PayPalHttp.HttpResponse response = await PayPalHelper.CreateOrder(payPalSetup, payment);

            return response.Result<Order>();
        }

        /// <summary>
        /// Capture Paypal payment and notify users and modules.
        /// </summary>
        /// <param name="paymentId">Payment Id</param>
        /// <param name="orderId">Paypal order ID</param>
        /// <returns>Paypal Order instance</returns>
        public async Task<Order> CapturePayment(int paymentId, string orderId)
        {
            Payment payment = await _context.Payment.Include(p => p.ProjectManager).Include(p => p.FreeLancer).FirstOrDefaultAsync(p => p.PaymentId == paymentId);

            PayPalHelper.MyPaypalSetup payPalSetup = new PayPalHelper.MyPaypalSetup { Environment = _paypalEnviroment, ClientId = _clientId, Secret = _secret };

            payPalSetup.PayerApprovedOrderId = orderId;

            //this is where actual transaction is carried out
            PayPalHttp.HttpResponse response = await PayPalHelper.CaptureOrder(payPalSetup);

            Order result = response.Result<Order>();

            if (result.Status == "COMPLETED")
            {
                string captureId = result.PurchaseUnits.First().Payments.Captures.FirstOrDefault(c => c.FinalCapture == true)!.Id;
                payment.PaypalCaptureId = captureId;
                payment.State = PaymentState.Paid;
                await UpdatePayment(payment);

                string statusMessageTitle = "Payment Successful!";
                string statusMessageDescription = $"Your payment related to the task  {payment.Title} has been successful";
                string url = await urlForNotification(payment.PaymentId);

                await _notificationService.SendNotification(payment.ProjectManager!, statusMessageTitle, statusMessageDescription, url);
            }

            return result;
        }

        /// <summary>
        /// Transfer pending payout funds to freelancer's associated paypal account.
        /// </summary>
        /// <param name="authenticatedUserId">Authenticated user Id should match freelancer's</param>
        /// <param name="paymentId">Id of payment with pending payout</param>
        /// <returns></returns>
        /// <exception cref="ArgumentException">Payment Id not valid</exception>
        /// <exception cref="InvalidOperationException">Authenticated user is not freelancer or operation failed.</exception>
        public async Task ExecutePayout(string authenticatedUserId, int paymentId)
        {
            Payment payment = await _context.Payment.Include(p => p.ProjectManager).Include(p => p.FreeLancer).FirstOrDefaultAsync(p => p.PaymentId == paymentId);

            if(payment == null)
            {
                throw new ArgumentException("Payment Id not valid.");
            }

            if(payment.FreeLancer.Id != authenticatedUserId)
            {
                throw new InvalidOperationException("Authenticated user is not freelancer");
            }

            var receiverPaypalEmail = GetPayPalEmail(payment.FreeLancer);

            WebClient? client = new WebClient();
            client.Headers.Add("content-type", "application/json");
            client.Headers.Add("authorization", "Bearer " + await GetAccessToken());
            client.Headers.Add("accept", "application/json");


            try
            {
                var response = client.UploadString("https://api.sandbox.paypal.com/v1/payments/payouts", "POST", GenerateJSONBody(payment.Title, payment.SubTotal, receiverPaypalEmail));
                Console.WriteLine(response);

                payment.State = PaymentState.Paid_Out;
                await UpdatePayment(payment);

                string url = await urlForNotification(payment.PaymentId);


                await _notificationService.SendNotification(payment.FreeLancer, "Payout Successful!", "Your payout related to the task '" + payment.Title + "' has been successful", url);
                await _notificationService.SendNotification(payment.ProjectManager, "Paymente accepted by freelancer", "The paymente related to the task " + payment.Title + " has been accepted by the freelancer", url);

            }
            catch (WebException e)
            {
                var errorResponse = e.Response as HttpWebResponse;
                Console.WriteLine(e.Response.Headers);
                string responseText;
                using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                {
                    responseText = reader.ReadToEnd();
                    Console.WriteLine(responseText);
                }

                throw new InvalidOperationException("Error while executing payout to freelancer.");
            }


        }

        private async Task<string> urlForNotification(int paymentId)
        {
            ContractingProcess? contractingProcess = await _context.ContractingProcess
                .Include(p => p.Payment)
                .Where(p => p.Payment!.PaymentId == paymentId).FirstOrDefaultAsync();

            if (contractingProcess == null)
            {
                throw new ArgumentException("Task with given ID not associated with a contracting process.");
            }

            return $"/Contracting/{contractingProcess!.Id}";
        }

        public async Task<List<Payment>> GetRefundedPayments(TaskLancerUser user)
        {
            var userPayments = await _context.UserPayment.Where(e => e.User == user).Include(x => x.Payments).FirstOrDefaultAsync();

            if (userPayments == null)
            {
                throw new ArgumentException("user Payments with requested user not found.");
            }

            var payments = userPayments.Payments.Where(p => p.State == PaymentState.Fully_Refunded).ToList();

            if (payments == null)
            {
                throw new ArgumentException("Payments not found.");
            }

            payments.OrderBy(Payment => Payment.InitialDate);

            return payments;
        }

        public async Task<List<Payment>> GetCanceledPayments(TaskLancerUser user)
        {
            var userPayments = await _context.UserPayment.Where(e => e.User == user).Include(x => x.Payments).FirstOrDefaultAsync();

            if (userPayments == null)
            {
                throw new ArgumentException("user Payments with requested user not found.");
            }

            var payments = userPayments.Payments.Where(p => p.State == PaymentState.Canceled).ToList();

            if (payments == null)
            {
                throw new ArgumentException("Payments not found.");
            }

            payments.OrderBy(Payment => Payment.InitialDate);

            return payments;
        }
    }


    public class PayPalHelper
    {
        public static PayPalHttpClient Client(MyPaypalSetup paypalEnvironment)
        {
            PayPalEnvironment environment = null;

            if (paypalEnvironment.Environment == "live")
            {
                // Creating a live environment
                environment = new LiveEnvironment(paypalEnvironment.ClientId, paypalEnvironment.Secret);
            }
            else
            {
                // Creating a sandbox environment
                environment = new SandboxEnvironment(paypalEnvironment.ClientId, paypalEnvironment.Secret);
            }

            // Creating a client for the environment
            PayPalHttpClient client = new PayPalHttpClient(environment);
            return client;
        }

        public async static Task<PayPalHttp.HttpResponse> CreateOrder(MyPaypalSetup paypalSetup, Payment payment)
        {
            PayPalHttp.HttpResponse response = null;

            try
            {
                // Construct a request object and set desired parameters
                // Here, OrdersCreateRequest() creates a POST request to /v2/checkout/orders
                var order = new OrderRequest()
                {
                    CheckoutPaymentIntent = "CAPTURE",
                    PurchaseUnits = new List<PurchaseUnitRequest>()
                        {
                            new PurchaseUnitRequest()
                            {
                                ReferenceId = "Tasklancer_outsource_"+payment.PaymentId,
                                Description = "Outsourcing a task with tasklancer",
                                Items = new List<Item>()
                                {
                                    new Item()
                                    {
                                        Quantity = "1",
                                        Name = payment.Title,
                                        Description = payment.Description,
                                        Tax = new Money(){ CurrencyCode = payment.Currency, Value = payment.ServiceFee.ToString().Replace(",",".") },
                                        UnitAmount = new Money(){ CurrencyCode = payment.Currency, Value = payment.SubTotal.ToString().Replace(",",".")},
                                    },
                                },

                                AmountWithBreakdown = new AmountWithBreakdown()
                                {
                                    CurrencyCode = payment.Currency,
                                    Value = payment.Total.ToString().Replace(",","."),

                                    AmountBreakdown = new AmountBreakdown()
                                    {
                                        TaxTotal = new Money()
                                        {
                                            CurrencyCode = payment.Currency,
                                            Value = payment.ServiceFee.ToString().Replace(",",".")
                                        },
                                        ItemTotal = new Money()
                                        {
                                            CurrencyCode = payment.Currency,
                                            Value = payment.SubTotal.ToString().Replace(",",".")
                                        }
                                    }
                                },
                                Payee = new Payee()
                                {

                                }
                            }
                        },
                    ApplicationContext = new ApplicationContext()
                    {
                        //ReturnUrl = paypalSetup.RedirectUrl + "/SuccessfulPayment/"+payment.PaymentId,
                        //CancelUrl = paypalSetup.RedirectUrl + "/CanceledPayment/"+payment.PaymentId
                        ReturnUrl = paypalSetup.RedirectUrl,
                        CancelUrl = paypalSetup.RedirectUrl + "&Cancel=true"
                    }
                };


                //IMPORTANT
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;


                // Call API with your client and get a response for your call
                var request = new OrdersCreateRequest();
                request.Prefer("return=representation");
                request.RequestBody(order);
                PayPalHttpClient paypalHttpClient = Client(paypalSetup);
                response = await paypalHttpClient.Execute(request);

            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: {0}", ex.Message);
                throw;
            }
            return response;
        }

        public async static Task<PayPalHttp.HttpResponse> CaptureOrder(MyPaypalSetup paypalSetup)
        {
            // Construct a request object and set desired parameters
            // Replace ORDER-ID with the approved order id from create order
            var request = new OrdersCaptureRequest(paypalSetup.PayerApprovedOrderId);
            request.RequestBody(new OrderActionRequest());
            PayPalHttpClient paypalHttpClient = Client(paypalSetup);
            PayPalHttp.HttpResponse response = await paypalHttpClient.Execute(request);
            return response;
        }



        public class MyPaypalSetup
        {
            /// <summary>
            /// Provide value sandbox for testing,  provide value live for real money
            /// </summary>
            public string Environment { get; set; }
            /// <summary>
            /// Client id as provided by Paypal on dashboard. Ensure you use correct value based on your environment selection
            /// Use sandbox accounts for sandbox testing
            /// </summary>
            public string ClientId { get; set; }
            /// <summary>
            /// Secret as provided by Paypal on dashboard. Ensure you use correct value based on your environment selection
            /// Use sandbox accounts for sandbox testing
            /// </summary>
            public string Secret { get; set; }

            /// <summary>
            /// This is the URL that you will pass to paypal which paypal will use to redirect payer back to your website.
            /// So essentially it is the same controller URL that you must pass
            /// </summary>
            public string RedirectUrl { get; set; }

            /// <summary>
            /// Once order is created on Paypal, it redirects control to your app with a URL that shows order details. Your website must take the payer to this page
            /// so the payer approved the payment. Store this URL in this property
            /// </summary>
            public string ApproveUrl { get; set; }

            /// <summary>
            /// When paypal redirects control to your website it provides a Approved Order ID which we then pass it back to paypal to execute the order.
            /// Store this approved order ID in this property
            /// </summary>
            public string PayerApprovedOrderId { get; set; }
        }


    }
}
