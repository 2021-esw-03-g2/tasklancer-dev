﻿using PayPalCheckoutSdk.Orders;
using TaskLancer.Areas.Identity.Data;
using TaskLancer.Controllers;
using TaskLancer.Models.Payments.DAL;

namespace TaskLancer.Models.Payments.BLL
{
    public interface IPaymentService
    {
        Task<List<Payment>> GetPayments(TaskLancerUser user);

        Task<List<Payment>> GetUnpaidPayments(TaskLancerUser user);

        Task<List<Payment>> GetPaidPayments(TaskLancerUser user);

        Task<List<Payment>> GetRefundedPayments(TaskLancerUser user);

        Task<List<Payment>> GetCanceledPayments(TaskLancerUser user);

        Task<int> UpdatePayment(Payment payment);

        Task<int> CreateUserPayment(string userId);

        Task<int> CreatePayment(int userPaymentId, Payment payment);

        Task<int> CreatePayment(string userId, Payment payment);

        Task CancelPayment(int paymentId);

        Task UnlockPaymentPayout(int paymentId);

        Task RefundPayment(int paymentId);

        Task<Payment> GetPayment(int id);

        Task<List<Payment>> GetPayouts(TaskLancerUser user);

        Task<PayPalHelper.MyPaypalSetup> AprovePayment(int id, string route);

        //Task<int> MakePayment(int paymentId);

        Task<PaymentViewModel> CreatePayout(int id);

        string GetPayPalEmail(TaskLancerUser user);

        Task<int> SetupPaypalEmail(TaskLancerUser user, string email);

        Task<Order> CreateOrder(int paymentId, string redirectRoute);
        Task<Order> CapturePayment(int paymentId, string orderId);

        Task ExecutePayout(string authenticatedUserId, int paymentId);
    }
}
