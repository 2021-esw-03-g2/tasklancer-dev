﻿using System.ComponentModel.DataAnnotations;
using TaskLancer.Areas.Identity.Data;

namespace TaskLancer.Models
{
    public class SkillTag
    {
        [Key]
        public int SkillTagId { get; set; }

        public string Name { get; set; }

        public string? Description { get; set; }

        public virtual ICollection<TaskLancerUser>? TaskLancerUsers { get; set; }

        public virtual ICollection<Project.DAL.Task>? Tasks { get; set; }
    }
}
