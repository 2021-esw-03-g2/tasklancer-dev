﻿using TaskLancer.Areas.Identity.Data;
using TaskLancer.Models;

namespace TaskLancer.Services
{
    public interface IProfileService
    {
        //string GetBiographyAsync(TaskLancerUser user);

        Task<int> SetBiographyAsync(TaskLancerUser user, string newBiography);

        Task<int> SetCurriculumAsync(TaskLancerUser user, string newCurriculum);

        Task<int> SetSkillTagsAsync(TaskLancerUser user, ICollection<SkillTag> skillTags);

        List<SkillTag> GetSkillTags(TaskLancerUser user);

        Task<TaskLancerUser> GetUserWithSkillTagsAsync(string username);
    }
}
