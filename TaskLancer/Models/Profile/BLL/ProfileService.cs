﻿using Microsoft.EntityFrameworkCore;
using TaskLancer.Areas.Identity.Data;
using TaskLancer.Data;
using TaskLancer.Models;

namespace TaskLancer.Services
{
    /// <summary>
    /// Service for Progfile Management
    /// </summary>
    public class ProfileService : IProfileService
    {
        private readonly TaskLancerContext _context;

        public ProfileService(TaskLancerContext ctx)
        {
            _context = ctx;
        }
        
        /// <summary>
        /// Gets a List with all the SkillTags associated with a user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public List<SkillTag> GetSkillTags(TaskLancerUser user)
        {
            var userWithSkillTags = _context.Users.Where<TaskLancerUser>(e => e.Id == user.Id).Include(x => x.SkillTags).FirstOrDefault();
            return userWithSkillTags.SkillTags.ToList();

        }

        /// <summary>
        /// Returns a user
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        public async Task<TaskLancerUser> GetUserWithSkillTagsAsync(string username)
        {
            var user = await _context.Users.Where<TaskLancerUser>(e => e.UserName == username).Include(x => x.SkillTags).FirstOrDefaultAsync();
            if(user == null) { throw new ArgumentException("Could not find the user"); }
            return user;
        }

        /// <summary>
        /// Sets the biography of a certain user
        /// </summary>
        /// <param name="user"></param>
        /// <param name="newBiography"></param>
        /// <returns></returns>
        public Task<int> SetBiographyAsync(TaskLancerUser user, string newBiography)
        {   
            
            user.Biography = newBiography;
            _context.Users.Update(user);
            return _context.SaveChangesAsync();
        }

        /// <summary>
        /// Sets the Curriculum of a certain user
        /// </summary>
        /// <param name="user"></param>
        /// <param name="newCurriculum"></param>
        /// <returns></returns>
        public Task<int> SetCurriculumAsync(TaskLancerUser user, string newCurriculum)
        {
            user.Curriculum = newCurriculum;
            _context.Users.Update(user);
            return _context.SaveChangesAsync();
        }

        /// <summary>
        /// Sets the skill tags of a certain user
        /// </summary>
        /// <param name="user"></param>
        /// <param name="skillTags"></param>
        /// <returns></returns>
        public Task<int> SetSkillTagsAsync(TaskLancerUser user, ICollection<SkillTag> skillTags)
        {
            
            user = _context.Users.Include(u => u.SkillTags).Single(u => u == user);

            user.SkillTags = skillTags;

            _context.Users.Update(user);
            Console.WriteLine(_context.ChangeTracker.DebugView.LongView);

            return _context.SaveChangesAsync();
        }
    }
}
