﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TaskLancer.Areas.Identity.Data;
using TaskLancer.Models.Contracting.DAL;

namespace TaskLancer.Models.FileTransfer.DAL
{
    public class File
    {
        [Required]
        [Key]
        public string FileId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public TaskLancerUser Owner { get; set; }

        [ForeignKey("Owner")]
        public string OwnerId { get; set; }

        [Required]
        public FileAccessPolicy AccessPolicy { get; set; }

        public bool InUse { get; set; }

        public virtual ICollection<TaskLancerUser> UsersAllowedAccess { get; set; }

        public virtual ICollection<Proposal> Proposals { get; set; }

        public virtual ICollection<Delivery> DeliveriesAsSample { get; set; }

        public virtual ICollection<Delivery> DeliveriesAsFinal { get; set; }


    }

    public enum FileAccessPolicy
    {
        Public,
        Private,
    }
}
