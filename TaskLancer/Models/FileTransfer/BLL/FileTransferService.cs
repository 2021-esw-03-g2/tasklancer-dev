﻿using Azure;
using Azure.Storage.Blobs;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using TaskLancer.Areas.Identity.Data;
using TaskLancer.Data;
using TaskLancer.Models.FileTransfer.DAL;

namespace TaskLancer.Models.FileTransfer.BLL
{
    public class FileTransferService : IFileTransferService
    {
        private BlobContainerClient _blobContainerClient;
        private readonly TaskLancerContext _context;

        public FileTransferService(TaskLancerContext context)
        {
            _context = context;
            string connectionString = "DefaultEndpointsProtocol=https;AccountName=tasklancerfiles;AccountKey=nBO9XFDQ8o3Z9hDFBNBOTQ+fY+1GYB5rv2JtpBMhQBIxHdm0bmW4l85eXtk1IFdYT1DZl3M17hn1rNDr/6m+hw==;EndpointSuffix=core.windows.net";
            _blobContainerClient = new BlobContainerClient(connectionString, "userfiles");
        }

        public async Task<Stream> GetFileStream(string fileId, string userId)
        {
            DAL.File? file = await _context.File.Include(file => file.UsersAllowedAccess).FirstOrDefaultAsync(f => f.FileId == fileId);


            if (file.AccessPolicy == FileAccessPolicy.Private && file!.OwnerId != userId)
            {
                TaskLancerUser? user = file.UsersAllowedAccess.FirstOrDefault(u => u.Id == userId);
                if (user == null)
                {
                    throw new ArgumentException();
                }
            }

            Stream data = await DownloadFromBlobStorage(fileId);

            return data;
        }

        public async Task<DAL.File> GetFile(string fileId, string userId)
        {
            DAL.File? file = await _context.File
                .Include(f => f.UsersAllowedAccess)
                .FirstOrDefaultAsync(f => f.FileId == fileId);

            if(file == null)
            {
                throw new ArgumentException("File ID not valid.");
            }

            if (file.AccessPolicy == FileAccessPolicy.Private && file!.OwnerId != userId)
            {
                TaskLancerUser? user = file.UsersAllowedAccess.FirstOrDefault(u => u.Id == userId);
                if (user == null)
                {
                    throw new ArgumentException();
                }
            }


            return file!;
        }

        public List<DAL.File> GetUserUnusedFiles(string userId)
        {
            List<DAL.File> files = _context.File.Where(f => f.InUse == false && f.OwnerId == userId).ToList();
            return files;
        }


        public async Task<bool> RemoveFile(string fileId, string userId)
        {
            DAL.File file = _context.File.FirstOrDefault(f => f.FileId == fileId);
            if (file == null)
            {
                return false;
            }

            if (file.OwnerId != userId)
            {
                throw new ArgumentException();
            }

            _context.File.Remove(file);

            await _context.SaveChangesAsync();

            await RemoveFromBlobStorage(fileId);

            return true;
        }


        public void RemoveUnusedFiles()
        {
            foreach (DAL.File file in _context.File.Where(f => f.InUse == false).ToList())
            {
                _context.File.Remove(file);
            }

            _context.SaveChanges();

        }

        public async Task<DAL.File> SavePrivateFile(string fileName, Stream data, string ownerId, List<string> allowedUsersIds)
        {
            string blobName = await UploadToBlobStorage(data);

            TaskLancerUser? user = _context.Users.FirstOrDefault(u => u.Id == ownerId);

            List<TaskLancerUser> allowedUsers = new List<TaskLancerUser>();

            foreach (var userId in allowedUsersIds)
            {
                TaskLancerUser? allowedUser = _context.Users.FirstOrDefault(u => u.Id == userId);
                allowedUsers.Add(allowedUser!);
            }

            DAL.File file = new DAL.File()
            {
                FileId = blobName,
                Name = fileName,
                Owner = user!,
                InUse = false,
                AccessPolicy = FileAccessPolicy.Private,
                UsersAllowedAccess = allowedUsers
            };

            EntityEntry<DAL.File> entry = await _context.File.AddAsync(file);

            await _context.SaveChangesAsync();

            return entry.Entity;
        }

        public async Task<DAL.File> SavePublicFile(string fileName, Stream data, string ownerId)
        {
            string blobName = await UploadToBlobStorage(data);

            TaskLancerUser? user = _context.Users.FirstOrDefault(u => u.Id == ownerId);

            DAL.File file = new DAL.File()
            {
                FileId = blobName,
                Name = fileName,
                Owner = user!,
                InUse = false,
                AccessPolicy = FileAccessPolicy.Public
            };

            EntityEntry<DAL.File> entry = await _context.File.AddAsync(file);

            await _context.SaveChangesAsync();

            return entry.Entity;
        }

        public async Task<bool> SetInUse(string fileId)
        {
            DAL.File? file = _context.File.FirstOrDefault(f => f.FileId == fileId);

            if (file == null)
                return false;

            file.InUse = true;

            _context.File.Update(file);

            await _context.SaveChangesAsync();

            return true;
        }

        private async Task<string> UploadToBlobStorage(Stream data)
        {
            string blobName = Path.GetRandomFileName();

            try
            {
                await _blobContainerClient.UploadBlobAsync(blobName, data);
            }
            catch (RequestFailedException e)
            {
                throw e;
            }

            return blobName;
        }

        private async Task<Stream> DownloadFromBlobStorage(string blobName)
        {
            Stream data = new MemoryStream();
            BlobClient blobClient = _blobContainerClient.GetBlobClient(blobName);


            try
            {
                await blobClient.DownloadToAsync(data);
            }
            catch (RequestFailedException e)
            {
                throw e;
            }

            return data;
        }

        private async Task<bool> RemoveFromBlobStorage(string blobName)
        {
            BlobClient blobClient = _blobContainerClient.GetBlobClient(blobName);
            try
            {
                await blobClient.DeleteAsync();
            }
            catch (RequestFailedException e)
            {
                return false;
            }

            return true;
        }
    }
}
