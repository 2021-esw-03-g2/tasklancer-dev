﻿
using TaskLancer.Models.FileTransfer.DAL;

namespace TaskLancer.Models.FileTransfer.BLL
{
    public interface IFileTransferService
    {
        Task<DAL.File> GetFile(string fileId, string userId);

        Task<Stream> GetFileStream(string fileId, string userId);
        List<DAL.File> GetUserUnusedFiles(string userId);

        Task<bool> RemoveFile(string fileId, string userId);
        void RemoveUnusedFiles();

        Task<DAL.File> SavePrivateFile(string fileName, Stream data, string ownerId, List<string> allowedUsersIds);
        Task<DAL.File> SavePublicFile(string fileName, Stream data, string ownerId);

        Task<bool> SetInUse(string fileId);

    }
}
