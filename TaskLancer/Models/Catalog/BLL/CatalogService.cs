﻿using TaskLancer.Models.Contracting.BLL;
using TaskLancer.Models.Contracting.DAL;

namespace TaskLancer.Models.Catalog.BLL
{
    public class CatalogService : ICatalogService
    {
        private readonly IContractingService _contractingService;

        public CatalogService(IContractingService contractingService)
        {
            _contractingService = contractingService;
        }

        /// <summary>
        /// Returns a list with contracts filtered by the parameters inserted
        /// </summary>
        /// <param name="selectedvalues"></param>
        /// <param name="filterDateValue"></param>
        /// <returns></returns>
        public List<ContractingProcess> GetFilteredContracts(List<int> selectedvalues, string filterDateValue)
        {
            List<ContractingProcess> contractsList = _contractingService.GetAllPublishedContractingProcesses();

            List<ContractingProcess> filteredList = new List<ContractingProcess>();

            foreach (var contract in contractsList)
            {
                if (contract.Task.SkillTags != null)
                {
                    foreach (var tag in contract.Task.SkillTags)
                    {
                        foreach (var item in selectedvalues)
                        {
                            if (tag.SkillTagId == item)
                            {
                                if (!filteredList.Contains(contract))
                                {
                                    filteredList.Add(contract);
                                }
                            }
                        }
                    }
                }
            }

            if (selectedvalues.Count == 0 || selectedvalues == null)
            {
                filteredList = contractsList;
            }

            if (filterDateValue.Equals("1"))
            {
                filteredList = filteredList.OrderBy(x => x.Proposal.DeliveryDate).ToList();
            }
            else if (filterDateValue.Equals("2"))
            {
                filteredList = filteredList.OrderByDescending(x => x.Proposal.DeliveryDate).ToList();
            }

            return filteredList;
        }
    }
}
