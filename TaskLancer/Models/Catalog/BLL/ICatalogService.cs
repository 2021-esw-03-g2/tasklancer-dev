﻿using TaskLancer.Models.Contracting.DAL;

namespace TaskLancer.Models.Catalog.BLL
{
    public interface ICatalogService
    {
        List<ContractingProcess> GetFilteredContracts(List<int> selectedvalues, string filterDateValue);
    }
}
