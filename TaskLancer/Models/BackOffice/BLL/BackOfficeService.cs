﻿using Microsoft.AspNetCore.Identity;
using TaskLancer.Areas.Identity.Data;
using TaskLancer.Data;

namespace TaskLancer.Models.BackOffice.BLL
{
    public class BackOfficeService : IBackOfficeService
    {
        private readonly TaskLancerContext _context;

        public BackOfficeService(TaskLancerContext ctx)
        {
            _context = ctx;
        }
        /// <summary>
        /// This method return the name of the role of the user with the id received
        /// </summary>
        /// <param name="id">User Id</param>
        /// <param name="userManager">UserManager service</param>
        /// <returns>Name of the role</returns>
        public string getUserRole(string id, UserManager<TaskLancerUser> userManager)
        {
            IdentityUserRole<string> userRole = _context.UserRoles.Where(u => u.UserId == id).FirstOrDefault();

            if (userRole != null)
            {
                return _context.Roles.Where(u => u.Id == userRole.RoleId).FirstOrDefault().Name;
            }
            return "No role";

        }
        /// <summary>
        /// This method change the role of the user with de id received
        /// </summary>
        /// <param name="id">User Id</param>
        /// <param name="userRole">User Role</param>
        /// <param name="userManager">UserManager service</param>
        /// <returns>Number of entries in the data base</returns>
        public async Task<int> ChangeUserRole(string id, string userRole, UserManager<TaskLancerUser> userManager)
        {
            TaskLancerUser user = await userManager.FindByIdAsync(id);
            var roles = await userManager.GetRolesAsync(user);
            if (roles.FirstOrDefault() != null)
            {
                await userManager.RemoveFromRoleAsync(user, roles.FirstOrDefault());
            }
            if (userRole != "null")
            {
                await userManager.AddToRoleAsync(user, userRole);
            }
            return await _context.SaveChangesAsync();
        }
    }
}
