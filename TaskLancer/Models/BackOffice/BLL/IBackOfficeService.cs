﻿using Microsoft.AspNetCore.Identity;
using TaskLancer.Areas.Identity.Data;

namespace TaskLancer.Models.BackOffice.BLL
{
    public interface IBackOfficeService
    {
        Task<int> ChangeUserRole(string id, string userRole, UserManager<TaskLancerUser> userManager);
        public string getUserRole(string id, UserManager<TaskLancerUser> userManager);
    }
}
