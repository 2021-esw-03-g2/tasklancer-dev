﻿using System.ComponentModel.DataAnnotations;

namespace TaskLancer.Models.Project.DAL
{
    public class CategoryTag
    {
        [Key]
        public int CategoryTagId { get; set; }

        public string Name { get; set; }

        public string? Description { get; set; }

        public virtual ICollection<Project>? Projects { get; set; }
    }
}
