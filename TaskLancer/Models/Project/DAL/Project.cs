﻿using System.ComponentModel.DataAnnotations;
using TaskLancer.Areas.Identity.Data;

namespace TaskLancer.Models.Project.DAL
{
    public class Project
    {
        [Key]
        public int ProjectId { get; set; }
        [Required]
        public string? Name { get; set; }
        public string? Description { get; set; }
        public bool IsPublic { get; set; }
        public Board? Board { get; set; }
        public TaskLancerUser? Manager { get; set; }
        //public ProjectState State {get;set;}

        public ICollection<CategoryTag>? CategoryTags { get; set; }

    }
}
