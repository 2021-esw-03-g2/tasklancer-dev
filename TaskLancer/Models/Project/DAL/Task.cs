﻿using System.ComponentModel.DataAnnotations;

namespace TaskLancer.Models.Project.DAL
{
    public class Task
    {
        [Key]
        public int TaskId { get; set; }
        [Required]
        [Display(Name = "Title")]
        public string Title { get; set; }
        [Display(Name = "Description")]
        public string Description { get; set; }
        [Display(Name = "Start Date")]
        public DateTime? StartDate { get; set; }
        [Display(Name = "End Date")]
        public DateTime? EndDate { get; set; }
        public ICollection<SkillTag>? SkillTags { get; set; }
        public Column? Column { get; set; }
        public Board? Board { get; set; }
    }
}
