﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TaskLancer.Models.Project.DAL
{
    public class Board
    {
        [Key]
        public int BoardId { get; set; }
        public ICollection<Column>? Columns { get; set; }
        public string ColumnsOrder { get; set; }
        [ForeignKey("Project")]
        public int ProjectId { get; set; }
        public Project? Project { get; set; }

    }
}
