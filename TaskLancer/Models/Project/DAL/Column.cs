﻿using System.ComponentModel.DataAnnotations;

namespace TaskLancer.Models.Project.DAL
{
    public class Column
    {
        [Key]
        public int ColumnId { get; set; }
        [Required]
        public string? Name { get; set; }
        [Required]
        public string? Color { get; set; }
        public List<Task>? Tasks { get; set; }
        public string? TasksOrder { get; set; }
        public Board? Board { get; set; }
    }
}
