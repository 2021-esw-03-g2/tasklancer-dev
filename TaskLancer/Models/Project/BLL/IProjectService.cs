﻿using TaskLancer.Models.Project.DAL;


namespace TaskLancer.Models.Project.BLL
{
    public interface IProjectService
    {
        Task<int> CreateProjectAsync(string name, string description, bool isPublic, ICollection<CategoryTag> categoryTags, string authenticatedUserId);
        Task<int> CreateUserProject(string userId);
        Task<DAL.Project> GetProjectDetailsAsync(int projectId, string authenticatedUserId);

        Task<List<DAL.Project>> GetAllUserProjects(string userId);

        Task<int> SetProjectDetailsAsync(int projectId, string name, string description, bool isPublic, ICollection<CategoryTag> categoryTags, string authenticatedUserId);

        Task<DAL.Project> GetProject(int boardId);

        Task<Board> GetBoard(int boardId);

        Task<int> RemoveBoard(int boardId);

        Task<Column> CreateColumn(int boardId, Column column);

        Task<Column> EditColumn(int boardId, Column column);

        Task<int> RemoveColumn(int boardId, int columnId);

        Task<int> MoveColumn(int boardId, int columnId, int newPosition);

        Task<DAL.Task> CreateTask(int boardId, int columnId, DAL.Task task);

        Task<DAL.Task> GetTask(int taskId);

        Task<int> EditTask(int boardId, DAL.Task task);

        Task<int> MoveTask(int boardId, int taskId, int fromColumnId, int toColumnId, int newPosition);

        Task<int> RemoveTask(int boardId, int columnId, int taskId);

        string generateJwt(string userId, int boardId);

        bool validateJWT(string token, int boardId);

        Task<int> GetProjectBoardId(int projectId);

    }
}
