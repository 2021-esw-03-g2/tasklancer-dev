﻿using JWT.Algorithms;
using JWT.Builder;
using JWT.Exceptions;
using Microsoft.EntityFrameworkCore;
using System.Text.Json;
using TaskLancer.Areas.Identity.Data;
using TaskLancer.Data;
using TaskLancer.Models;
using TaskLancer.Models.Project.DAL;

namespace TaskLancer.Models.Project.BLL
{
    public class ProjectService : IProjectService
    {
        private readonly TaskLancerContext _context;

        public ProjectService(TaskLancerContext ctx)
        {
            _context = ctx;
        }



        /// <summary>
        /// This method creates a default project for the user with the id received
        /// </summary>
        /// <param name="userId">User id</param>
        /// <returns>Number of entries in the data base</returns>
        /// <exception cref="ArgumentException">In case the user id doesn't exist</exception>
        public async Task<int> CreateUserProject(string userId)
        {
            TaskLancerUser user = await _context.Users.FirstOrDefaultAsync(u => u.Id == userId);

            if (user == null)
            {
                throw new ArgumentException("User could not be found.");
            }

            DAL.Project project = new DAL.Project
            {
                Name = "My Project",
                Description = "My first TaskLancer project.",
                Manager = user,
                IsPublic = true,
                Board = new Board() { ColumnsOrder = "[]" }
            };

            _context.Project.Add(project);

            return await _context.SaveChangesAsync();
        }

        /// <summary>
        /// This method creates a default project for the user with the id received
        /// </summary>
        /// <param name="userId">User id</param>
        /// <returns>Number of entries in the data base</returns>
        /// <exception cref="ArgumentException">In case the user id doesn't exist</exception>
        public async Task<int> CreateProjectAsync(string name, string description, bool isPublic, ICollection<CategoryTag> categoryTags, string authenticatedUserId)
        {
            TaskLancerUser user = await _context.Users.FirstOrDefaultAsync(u => u.Id == authenticatedUserId);

            if (user == null)
            {
                throw new ArgumentException("User could not be found.");
            }

            DAL.Project project = new DAL.Project
            {
                Name = name,
                Description = description,
                Manager = user,
                IsPublic = isPublic,
                CategoryTags = categoryTags,
                Board = new Board() { ColumnsOrder = "[]" }
            };

            var entityEntry = _context.Project.Add(project);

            await _context.SaveChangesAsync();

            return entityEntry.Entity.ProjectId;
        }


        /// <summary>
        /// This method returns the project with the id received, from the user that has the same id as the one received
        /// </summary>
        /// <param name="projectId">Project id</param>
        /// <param name="authenticatedUserId">User authenticated id</param>
        /// <returns>The project that has the same id as the one received</returns>
        /// <exception cref="ArgumentException">In case the id of the project doesn't exist or in case the project is 
        /// private and the authenticated user is not the manager
        ///</exception>
        public async Task<DAL.Project> GetProjectDetailsAsync(int projectId, string authenticatedUserId)
        {
            DAL.Project? project = await _context.Project.Include(p => p.Manager).Include(p => p.CategoryTags).FirstOrDefaultAsync(p => p.ProjectId == projectId);

            if (project == null)
            {
                throw new ArgumentException("Project with requested Id not found.");
            }

            if (project.IsPublic == false && !(project.Manager.Id == authenticatedUserId))
            {
                throw new ArgumentException("Requested project is private and not managed by authenticated user.");
            }

            return project;

            //return await _context.Project.FindAsync(projectId);
        }

        public async Task<List<DAL.Project>> GetAllUserProjects(string userId)
        {
            List<DAL.Project> projects = await _context.Project.Include(p => p.CategoryTags).Include(p => p.Manager).Where(p => p.Manager!.Id == userId).ToListAsync();

            return projects;
        }

        /// <summary>
        /// This method updates the project details with the params received 
        /// </summary>
        /// <param name="projectId"></param>
        /// <param name="name">New name of the project</param>
        /// <param name="description">New description of the project</param>
        /// <param name="isPublic">Boolean specifing the privacy of the project</param>
        /// <param name="categoryTags">New list of category tags</param>
        /// <param name="authenticatedUserId">User authenticated id</param>
        /// <returns>The number of entries in the data base</returns>
        /// <exception cref="ArgumentException">In case the id of the project received in param doesn't exist or 
        /// in case the id of the user received in param as well is not the manager of the project</exception>
        public async Task<int> SetProjectDetailsAsync(int projectId, string name, string description, bool isPublic, ICollection<CategoryTag> categoryTags, string authenticatedUserId)
        {
            DAL.Project? project = await _context.Project.Include(p => p.Manager).Include(p => p.CategoryTags).FirstOrDefaultAsync(p => p.ProjectId == projectId);

            if (project == null)
            {
                throw new ArgumentException("Project with requested Id not found.");
            }

            if (project.Manager.Id != authenticatedUserId)
            {
                throw new ArgumentException("Project can only be edited by it's manager.");
            }

            project.Name = name;
            project.Description = description;
            project.IsPublic = isPublic;
            project.CategoryTags = categoryTags;

            _context.Project.Update(project);

            return await _context.SaveChangesAsync();
        }

        public async Task<DAL.Project> GetProject(int boardId)
        {
            DAL.Project? project = await _context.Project.Include(p => p.Board).Where(p => p.Board.BoardId == boardId).FirstOrDefaultAsync();

            if (project == null)
            {
                throw new ArgumentException("Project with board with requested Id not found.");
            }

            return project;
        }

        public async Task<int> GetProjectBoardId(int projectId)
        {
            DAL.Project? project = await _context.Project.Include(p => p.Board).FirstOrDefaultAsync(p => p.ProjectId == projectId);

            if (project != null && project.Board != null)
            {
                return project.Board.BoardId;
            }

            return 0;
        }
        public async Task<Board> GetBoard(int boardId)
        {
            Board? board = await _context.Board.Include(t => t.Columns).ThenInclude(c => c.Tasks).ThenInclude(t => t.SkillTags).FirstOrDefaultAsync(b => b.BoardId == boardId);

            if (board == null)
            {
                throw new ArgumentException("Board with requested Id not found.");
            }

            return board;
        }

        public async Task<int> RemoveBoard(int boardId)
        {
            Board? board = await _context.Board.FirstOrDefaultAsync(b => b.BoardId == boardId);

            if (board == null)
            {
                throw new ArgumentException("Board with requested Id not found.");
            }

            _context.Board.Remove(board);
            return await _context.SaveChangesAsync();
        }


        public async Task<Column> CreateColumn(int boardId, Column column)
        {
            Board? board = await _context.Board.FirstOrDefaultAsync(b => b.BoardId == boardId);

            if (board == null)
            {
                throw new ArgumentException("Board with requested Id not found.");
            }

            if (column == null)
            {
                throw new ArgumentException("Column is null");
            }

            column.Board = board;
            column.Tasks = new List<DAL.Task>();
            column.TasksOrder = "[]";
            var result = _context.Column.AddAsync(column);

            if (result.Result.State == EntityState.Added)
            {
                await _context.SaveChangesAsync();
                column.ColumnId = result.Result.Entity.ColumnId;

                board = AddColumnToProject(board, column.ColumnId);

                _context.Update(board);
                await _context.SaveChangesAsync();

                return column;
            }

            return null;
        }

        public async Task<Column> EditColumn(int boardId, Column column)
        {
            Board? board = await _context.Board.FirstOrDefaultAsync(b => b.BoardId == boardId);

            if (board == null)
            {
                throw new ArgumentException("Board with requested Id not found.");
            }

            if (column == null)
            {
                throw new ArgumentException("Column null");
            }

            _context.Update(column);
            await _context.SaveChangesAsync();

            return column;
        }

        public async Task<int> RemoveColumn(int boardId, int columnId)
        {
            Board? board = await _context.Board.FirstOrDefaultAsync(b => b.BoardId == boardId);

            if (board == null)
            {
                throw new ArgumentException("Board with requested Id not found.");
            }

            Column? column = await _context.Column.Include(b => b.Tasks).FirstOrDefaultAsync(c => c.ColumnId == columnId);

            if (column == null)
                throw new ArgumentException("Column with requested Id not found.");

            board = DeleteColumnFromProject(board, column);

            _context.Board.Update(board);

            foreach (DAL.Task task in column.Tasks)
            {
                _context.Task.Remove(task);
            }

            _context.Column.Remove(column);

            return await _context.SaveChangesAsync();
        }

        public async Task<int> MoveColumn(int boardId, int columnId, int newPosition)
        {
            Board? board = await _context.Board.FirstOrDefaultAsync(b => b.BoardId == boardId);

            if (board == null)
            {
                throw new ArgumentException("Board with requested Id not found.");
            }

            Column? column = await _context.Column.FirstOrDefaultAsync(c => c.ColumnId == columnId);

            if (column == null)
                throw new ArgumentException("Column with requested Id not found.");

            board = MoveColumnInBoard(board, columnId, newPosition);

            _context.Board.Update(board);

            return await _context.SaveChangesAsync();
        }

        public async Task<DAL.Task> CreateTask(int boardId, int columnId, DAL.Task task)
        {
            Board? board = await _context.Board.FirstOrDefaultAsync(b => b.BoardId == boardId);

            if (board == null)
            {
                throw new ArgumentException("Board with requested Id not found.");
            }

            Column? column = await _context.Column.FirstOrDefaultAsync(c => c.ColumnId == columnId);

            if (column == null)
                throw new ArgumentException("Column with requested Id not found.");

            if (task == null)
            {
                throw new ArgumentException("Task null");
            }

            task.Column = column;
            task.Board = board;
            var result = _context.Task.AddAsync(task);

            if (result.Result.State == EntityState.Added)
            {
                await _context.SaveChangesAsync();
                task.TaskId = result.Result.Entity.TaskId;

                column = AddTaskToColumn(column, task);
                _context.Update(column);

                await _context.SaveChangesAsync();

                return task;
            }
            return null;

        }

        public async Task<int> EditTask(int boardId, DAL.Task task)
        {
            Board? board = await _context.Board.FirstOrDefaultAsync(b => b.BoardId == boardId);

            if (board == null)
            {
                throw new ArgumentException("Board with requested Id not found.");
            }



            DAL.Task? dbTask = await _context.Task.Include(t => t.SkillTags).FirstOrDefaultAsync(t => t.TaskId == task.TaskId);

            if (dbTask == null)
            {
                throw new ArgumentException("Task with requested Id not found.");

            }

            dbTask.Title = task.Title;
            dbTask.Description = task.Description;
            dbTask.StartDate = task.StartDate;
            dbTask.EndDate = task.EndDate;
            dbTask.SkillTags = task.SkillTags;

            //if(dbTask != null && dbTask.SkillTags != null)
            //{
            //    foreach(SkillTag skillTag in dbTask.SkillTags)
            //    {
            //        if (!task.SkillTags.Contains(skillTag))
            //        {
            //            skillTag.Tasks.Remove(dbTask);
            //            _context.SkillTag.Update(skillTag);
            //        }
            //    }
            //}

            _context.Task.Update(dbTask);

            return await _context.SaveChangesAsync();
        }

        public async Task<int> RemoveTask(int boardId, int columnId, int taskId)
        {
            Board? board = await _context.Board.FirstOrDefaultAsync(b => b.BoardId == boardId);

            if (board == null)
            {
                throw new ArgumentException("Board with requested Id not found.");
            }

            Column? column = await _context.Column.FirstOrDefaultAsync(c => c.ColumnId == columnId);

            if (column == null)
                throw new ArgumentException("Column with requested Id not found.");

            DAL.Task? task = await _context.Task.FirstOrDefaultAsync(t => t.TaskId == taskId);

            if (task == null)
                throw new ArgumentException("Task with requested Id not found.");


            column = RemoveTask(column, task);
            _context.Remove(task);
            _context.Update(column);

            return await _context.SaveChangesAsync();
        }

        public async Task<int> MoveTask(int boardId, int taskId, int fromColumnId, int toColumnId, int newPosition)
        {
            Board? board = await _context.Board.FirstOrDefaultAsync(b => b.BoardId == boardId);

            if (board == null)
            {
                throw new ArgumentException("Board with requested Id not found.");
            }

            if (fromColumnId == toColumnId)
            {
                Column? column = await _context.Column.FirstOrDefaultAsync(c => c.ColumnId == toColumnId);

                if (column == null)
                    throw new ArgumentException("Column with requested Id not found.");

                column = MoveTaskInColumn(column, taskId, newPosition);
                _context.Update(column);
            }
            else
            {
                DAL.Task? task = await _context.Task.FirstOrDefaultAsync(t => t.TaskId == taskId);

                if (task == null)
                    throw new ArgumentException("Task with requested Id not found.");

                Column? columnToRemove = await _context.Column.FirstOrDefaultAsync(c => c.ColumnId == fromColumnId);
                if (columnToRemove == null)
                    throw new ArgumentException("Column with requested Id not found.");

                Column? columnToAdd = await _context.Column.FirstOrDefaultAsync(c => c.ColumnId == toColumnId);
                if (columnToAdd == null)
                    throw new ArgumentException("Column with requested Id not found.");

                MoveTask(columnToRemove, columnToAdd, task, newPosition);
                _context.Update(columnToRemove);
                _context.Update(columnToAdd);
            }

            return await _context.SaveChangesAsync();
        }

        private Board AddColumnToProject(Board board, int columnId)
        {
            List<int> columnIds = ConvertIdsOrderInList(board.ColumnsOrder);
            columnIds.Add(columnId);
            board.ColumnsOrder = ConvertListOfIdsInString(columnIds);

            return board;
        }

        private Board DeleteColumnFromProject(Board board, Column column)
        {
            board.Columns.Remove(column);

            List<int> columnIds = ConvertIdsOrderInList(board.ColumnsOrder);
            columnIds.Remove(column.ColumnId);
            board.ColumnsOrder = ConvertListOfIdsInString(columnIds);

            return board;
        }

        private Board MoveColumnInBoard(Board board, int columnId, int newIndex)
        {
            List<int> columnIds = ConvertIdsOrderInList(board.ColumnsOrder);

            columnIds.Remove(columnId);
            columnIds.Insert(newIndex, columnId);

            board.ColumnsOrder = ConvertListOfIdsInString(columnIds);

            return board;
        }

        private Column AddTaskToColumn(Column column, DAL.Task task)
        {
            List<int> taskIds = ConvertIdsOrderInList(column.TasksOrder);
            taskIds.Add(task.TaskId);
            column.TasksOrder = ConvertListOfIdsInString(taskIds);

            return column;
        }


        private Column RemoveTask(Column column, DAL.Task task)
        {
            //column.Tasks.Remove(task);

            List<int> taskIds = ConvertIdsOrderInList(column.TasksOrder);
            taskIds.Remove(task.TaskId);
            column.TasksOrder = ConvertListOfIdsInString(taskIds);

            return column;
        }

        private void MoveTask(Column columnToRemove, Column columnToAdd, DAL.Task task, int newPosition)
        {
            columnToRemove.Tasks.Remove(task);
            List<int> taskIdsOfColumnToRemove = ConvertIdsOrderInList(columnToRemove.TasksOrder);
            taskIdsOfColumnToRemove.Remove(task.TaskId);
            columnToRemove.TasksOrder = ConvertListOfIdsInString(taskIdsOfColumnToRemove);

            if (columnToAdd.Tasks == null)
            {
                columnToAdd.Tasks = new List<DAL.Task>();
            }

            columnToAdd.Tasks.Add(task);
            List<int> taskIdsOfColumnToAdd = ConvertIdsOrderInList(columnToAdd.TasksOrder);
            taskIdsOfColumnToAdd.Insert(newPosition, task.TaskId);
            columnToAdd.TasksOrder = ConvertListOfIdsInString(taskIdsOfColumnToAdd);
        }

        private Column MoveTaskInColumn(Column column, int taskId, int newPosition)
        {
            List<int> taskIds = ConvertIdsOrderInList(column.TasksOrder);

            taskIds.Remove(taskId);
            taskIds.Insert(newPosition, taskId);

            column.TasksOrder = ConvertListOfIdsInString(taskIds);

            return column;
        }

        private List<int> ConvertIdsOrderInList(string idsOrder)
        {
            List<int>? idList = JsonSerializer.Deserialize<List<int>>(idsOrder);

            if (idList == null)
            {
                return new List<int>();
            }

            return idList;
        }

        private string ConvertListOfIdsInString(List<int> ids)
        {
            return JsonSerializer.Serialize(ids);
        }


        public string generateJwt(string userId, int boardId)
        {
            const string jwtSecret = "AKJASHDKJASHD";

            var token = JwtBuilder.Create()
                                    .WithAlgorithm(new HMACSHA256Algorithm())
                                    .WithSecret(jwtSecret)
                                    .AddClaim("exp", DateTimeOffset.UtcNow.AddHours(1).ToUnixTimeSeconds())
                                    .AddClaim("userId", userId)
                                    .AddClaim("boardId", boardId)
                                    .Encode();

            return token;
        }


        public bool validateJWT(string token, int boardId)
        {
            const string jwtSecret = "AKJASHDKJASHD";

            try
            {
                var jwt = JwtBuilder.Create()
                                       .WithAlgorithm(new HMACSHA256Algorithm())
                                       .WithSecret(jwtSecret)
                                       .MustVerifySignature()
                                       .Decode<IDictionary<string, object>>(token);

                if (jwt == null)
                    return false;

                if (Convert.ToInt32(jwt["boardId"]) != boardId)
                    return false;

                Board board = _context.Board.Include(b => b.Project).ThenInclude(p => p.Manager).FirstOrDefault(b => b.BoardId == boardId);

                return jwt["userId"].Equals(board.Project.Manager.Id);
            }
            catch (Exception ex)
            {
                return false;
            }



        }

        public async Task<DAL.Task> GetTask(int taskId)
        {
            DAL.Task? task = await _context.Task.FirstOrDefaultAsync(t => t.TaskId == taskId);

            return task!;
        }
    }
}
