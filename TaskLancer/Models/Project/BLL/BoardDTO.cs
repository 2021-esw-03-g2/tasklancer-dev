﻿namespace TaskLancer.Models.Project.BLL
{
    public class BoardDTO
    {
        public int? BoardId { get; set; }
        public ICollection<ColumnDTO>? Columns { get; set; }
    }
}
