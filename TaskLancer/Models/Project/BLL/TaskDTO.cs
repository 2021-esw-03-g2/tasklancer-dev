﻿namespace TaskLancer.Models.Project.BLL
{
    public class TaskDTO
    {
        public int? TaskId { get; set; }
        public string? Title { get; set; }
        public string? Description { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public ICollection<SkillTagDTO>? SkillTags { get; set; }
        public bool? HasContractingProcess { get; set; }
    }
}
