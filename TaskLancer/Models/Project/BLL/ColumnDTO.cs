﻿namespace TaskLancer.Models.Project.BLL
{
    public class ColumnDTO
    {
        public int? ColumnId { get; set; }
        public string? Name { get; set; }
        public string? Color { get; set; }
        public List<TaskDTO>? Tasks { get; set; }
    }
}
