﻿namespace TaskLancer.Models
{
    public class SkillTagDTO
    {
        public int SkillTagId { get; set; }

        public string? Name { get; set; }

        public string? Description { get; set; }

    }
}
