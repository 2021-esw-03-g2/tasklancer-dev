﻿using TaskLancer.Views.FileTransfer;

namespace TaskLancer.Models.Contracting.BLL
{
    public class ProposalViewModel
    {
        public int Id { get; set; }
        public int TaskId { get; set; }
        public string Description { get; set; }
        public string Amount { get; set; }

        public DateTime DeliveryDate { get; set; }
        public DateTime CounterProposalsEndDate { get; set; }


        public string? ReferenceFilesIdsJson { get; set; }

        public UploadPartialViewModel? UploadPartialViewModel { get; set; }
    }
}
