﻿using TaskLancer.Models.Contracting.DAL;
using TaskLancer.Services;

namespace TaskLancer.Models.Contracting.BLL
{
    public interface IContractingSchedulingService
    {
        Task ContractingProcessPublished(ContractingProcess contractingProcess);

        Task CounterProposalAccepted(ContractingProcess contractingProcess);

        Task TaskOrdered (ContractingProcess contractingProcess);

        Task UnderEvaluation(ContractingProcess contractingProcess);

        Task DeliveryAccepted (ContractingProcess contractingProcess);
        Task RemoveAllPendingEvents(ContractingProcess contractingProcess);
    }
}
