﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System.Linq;
using TaskLancer.Areas.Identity.Data;
using TaskLancer.Data;
using TaskLancer.Models.Contracting.DAL;
using TaskLancer.Models.FileTransfer.BLL;
using TaskLancer.Models.Notifications.BLL;
using TaskLancer.Models.Payments.BLL;
using TaskLancer.Models.Payments.DAL;
using TaskLancer.Models.Project.DAL;
using TaskLancer.Models.Rating.BLL;

namespace TaskLancer.Models.Contracting.BLL
{
    /// <summary>
    /// Service for logic related to contracting processes
    /// </summary>
    public class ContractingService : IContractingService
    {
        private readonly TaskLancerContext _context;
        private readonly INotificationService _notificationService;
        private readonly IPaymentService _paymentService;
        private readonly IFileTransferService _fileTransferService;
        private readonly IContractingSchedulingService _contractingSchedulingService;
        private readonly IRatingService _ratingService;

        public ContractingService(TaskLancerContext context,
            INotificationService notificationService,
            IPaymentService paymentService,
            IFileTransferService fileTransferService,
            IContractingSchedulingService contractingSchedulingService,
            IRatingService ratingService)
        {
            _context = context;
            _notificationService = notificationService;
            _paymentService = paymentService;
            _fileTransferService = fileTransferService;
            _contractingSchedulingService = contractingSchedulingService;
            _ratingService = ratingService;
        }

        /// <summary>
        /// This method handles a contracting process's manager counter proposal acceptance action.
        /// </summary>
        /// <param name="userId">Manager user id</param>
        /// <param name="contractingProcessId">The contracting process being changed.</param>
        /// <param name="counterProposalId">The counter proposal to accept</param>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException">In case the user id doesn't exist or isn't manager. Also when the contracting process or the counter proposal don't exist.</exception>
        public async System.Threading.Tasks.Task AcceptCounterProposal(string userId, int contractingProcessId, int counterProposalId)
        {
            // Get contract, throws argument exception if not valid id
            //! TODO handle exceptions
            ContractingProcess contractingProcess = await GetContractingProcess(contractingProcessId);

            // Check if User trying to access is manager
            if(contractingProcess.Proposal.Manager.Id != userId)
                throw new InvalidOperationException("User making request is not manager.");
            

            //Check if process is in published state
            if(contractingProcess.State != ContractingProcessState.Published)
                throw new InvalidOperationException("Contracting process must be in Published state.");

            //Check if counter-proposal belongs to process
            CounterProposal? counterProposal = contractingProcess.CounterProposals.FirstOrDefault(counterProposal => counterProposal.Id == counterProposalId);

            if (counterProposal == null)
                throw new InvalidOperationException("Counter proposal does not belong to process.");

            //Set counterproposal as accepted and refuse all others
            contractingProcess.AcceptedCounterProposal = counterProposal;

            await RejectAllCounterProposalsButAccepted(contractingProcess);

            //Changing state to pending payment
            int paymentId = await SetupPendingPayment(contractingProcess);

            contractingProcess.PaymentId = paymentId;
            contractingProcess.State = ContractingProcessState.Pending_Payment;

            await _notificationService.SendNotification(counterProposal.Freelancer,
                "Your counter-proposal was accepted.",
                $"Your counter-proposal in {contractingProcess.Task.Title} was accepted. Awaiting for manager payment.", $"/Contracting/{contractingProcess.Id}");

            _context.ContractingProcess.Update(contractingProcess);

            await _context.SaveChangesAsync();

            await _contractingSchedulingService.CounterProposalAccepted(contractingProcess);
        }

        private async System.Threading.Tasks.Task RejectAllCounterProposalsButAccepted(ContractingProcess contractingProcess)
        {
            foreach(CounterProposal counterProposal in contractingProcess.CounterProposals)
            {
                if(counterProposal != contractingProcess.AcceptedCounterProposal)
                {
                    await RejectCounterProposal(contractingProcess, counterProposal, false);
                }
            }
        }

        private async System.Threading.Tasks.Task RejectCounterProposal(ContractingProcess contractingProcess, CounterProposal counterProposal, bool canReSubmit)
        {
            string reSubmitMessage = canReSubmit
                ? "You can still try submiting a new counter-proposal."
                : "You can no longer submit a counter-proposal in this process because another freelancer was chosen.";

            await _notificationService.SendNotification(counterProposal.Freelancer,
                "Your counter-proposal was rejected.",
                $"Your counter-proposal in {contractingProcess.Task.Title} was rejected. {reSubmitMessage}", $"/Contracting/{contractingProcess.Id}");

            //contractingProcess.CounterProposals.Remove(counterProposal);

        }

        private async Task<int> SetupPendingPayment(ContractingProcess contractingProcess)
        {
            float subtotal = contractingProcess.AcceptedCounterProposal!.Amount;
            float serviceFee = (float) Math.Round(Math.Clamp(subtotal * 0.05f, 0.25f, subtotal), 2);
            float total = subtotal + serviceFee;

            Payment payment = new Payment()
            {
                ServiceFee = serviceFee,
                SubTotal = subtotal,
                Total = total,
                Currency = "EUR",
                State = PaymentState.Pending_Payment,
                LimitDate = contractingProcess.CounterProposalsEndDate.AddSeconds(48),
                Title = $"\"{contractingProcess.Task.Title}\" Payment",
                Description = $"Payment for outsourcing task \"{contractingProcess.Task.Title}\" to freelancer {contractingProcess.AcceptedCounterProposal!.Freelancer.UserName}.",
                FreeLancer = contractingProcess.AcceptedCounterProposal.Freelancer,
                ProjectManager = contractingProcess.Proposal.Manager,
            };

            return await _paymentService.CreatePayment(contractingProcess.Proposal.Manager.Id, payment);
        }

        /// <summary>
        /// This method adds a freelancer's counter proposal to an existing contracting process.
        /// </summary>
        /// <param name="contractingProcessId">The contracting process being changed.</param>
        /// <param name="userId">Manager user id</param>
        /// <param name="selfIntro">Self introductory letter from freelancer.</param>
        /// <param name="amount">Monetary amount in euros.</param>
        /// <param name="deliveryDate">Counter proposed limit date for delivery.</param>
        /// <returns>Added counter-proposal.</returns>
        /// <exception cref="InvalidOperationException">In case the user id doesn't exist, the process is not accepting submissions, or freelancer already has pending counter-proposal.</exception>
        public async Task<CounterProposal> AddCounterProposal(int contractingProcessId, string userId, string selfIntro, float amount, DateTime deliveryDate)
        {
            ContractingProcess contractingProcess = await GetContractingProcess(contractingProcessId);

            
            TaskLancerUser? freelancer = await _context.Users.FirstOrDefaultAsync(u => u.Id == userId);

            if(freelancer == null)
            {
                throw new ArgumentException("No user with requested ID.");
            }

            if (contractingProcess.State != ContractingProcessState.Published)
            {
                throw new InvalidOperationException("Requested process in not accepting counter-proposals.");
            }

            if (IsTherePendingCounterProposalByUserInProcess(contractingProcess, freelancer))
            {
                throw new InvalidOperationException("Freelancer already has a pending counter-proposal in process.");
            }

            CounterProposal counterProposal = new CounterProposal()
            {
                ContractingProcess = contractingProcess,
                Freelancer = freelancer,
                Amount = amount,
                DeliveryDate = deliveryDate,
                SelfIntro = selfIntro
            };

            contractingProcess.CounterProposals.Add(counterProposal);

            _context.ContractingProcess.Update(contractingProcess);

            await _context.SaveChangesAsync();

            await _notificationService.SendNotification(contractingProcess.Proposal.Manager,
                $"You recieved a counter-proposal in \"{contractingProcess.Task.Description}\"'s contracting process.",
                $"{counterProposal.Freelancer.FirstName} {counterProposal.Freelancer.LastName} submitted a counter-proposal in your ongoing contracting process for task \"{contractingProcess.Task.Description}\". Check the contracting process for terms.",
                $"/Contracting/{contractingProcess.Id}");

            return counterProposal;
        }

        /// <summary>
        /// This method edits an existing freelancer's counter proposal to an existing contracting process.
        /// </summary>
        /// <param name="contractingProcessId">The contracting process being changed.</param>
        /// <param name="counterProposalId">The counter-proposal being edited.</param>
        /// <param name="userId">Manager user id</param>
        /// <param name="selfIntro">Self introductory letter from freelancer.</param>
        /// <param name="amount">Monetary amount in euros.</param>
        /// <param name="deliveryDate">Counter proposed limit date for delivery.</param>
        /// <returns>Edited counter-proposal.</returns>
        /// <exception cref="InvalidOperationException">In case the user id doesn't exist, is not author of counter-proposal or there is no pending counter-proposal.</exception>
        public async Task<CounterProposal> EditCounterProposal(int contractingProcessId, int counterProposalId, string userId, string selfIntro, float amount, DateTime deliveryDate)
        {
            ContractingProcess contractingProcess = await GetContractingProcess(contractingProcessId);

            TaskLancerUser? freelancer = await _context.Users.FirstOrDefaultAsync(u => u.Id == userId);
            if (freelancer == null)
                throw new ArgumentException("No user with requested ID.");

            CounterProposal? counterProposal = contractingProcess.CounterProposals.FirstOrDefault(p => p.Id == counterProposalId);

            if (counterProposal == null)
                throw new InvalidOperationException("No pending counter proposal with given ID in contracting process.");

            if (counterProposal.Freelancer != freelancer)
                throw new InvalidOperationException("User with given ID is not publisher of counter-proposal");

            counterProposal.Amount = amount;
            counterProposal.DeliveryDate = deliveryDate;
            counterProposal.SelfIntro = selfIntro;

            var entityEntry = _context.Update(counterProposal);

            await _context.SaveChangesAsync();

            return entityEntry.Entity;
        }

        private bool IsTherePendingCounterProposalByUserInProcess(ContractingProcess contractingProcess, TaskLancerUser freelancer)
        {
            foreach(CounterProposal counterProposal in contractingProcess.CounterProposals)
            {
                if (counterProposal.Freelancer == freelancer)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// This method adds a freelancer's delivery to an existing contracting process accepting deliveries.
        /// </summary>
        /// <param name="contractingProcessId">The contracting process being changed.</param>
        /// <param name="userId">Freelancer user id</param>
        /// <param name="finalComment">Comment for this delivery by freelancer.</param>
        /// <param name="sampleFilesIds">List of files identifiers for sample files.</param>
        /// <param name="finalFilesIds">List of files identifiers for final files.</param>
        /// <returns>Added delivery.</returns>
        /// <exception cref="InvalidOperationException">In case the user id doesn't exist, is not freelancer in process, process is not accepting delivery.</exception>
        public async Task<Delivery> AddDelivery(int contractingProcessId, string userId, string finalComment, List<string> sampleFilesIds, List<string> finalFilesIds)
        {

            ContractingProcess contractingProcess = await GetContractingProcess(contractingProcessId);


            TaskLancerUser? freelancer = await _context.Users.FirstOrDefaultAsync(u => u.Id == userId);

            if (freelancer == null)
            {
                throw new ArgumentException("No user with requested ID.");
            }

            if (contractingProcess.State != ContractingProcessState.Ordered)
            {
                throw new InvalidOperationException("Requested process is not accepting deliveries.");
            }

            if (contractingProcess.AcceptedCounterProposal!.Freelancer != freelancer)
            {
                throw new InvalidOperationException("User is not the process chosen freelancer.");
            }

            if (contractingProcess.Deliveries.Any(d => d.ApprovalState != DeliveryApprovalState.Declined))
                throw new InvalidOperationException("Can't add delivery while there is a pending delivery.");



            List<FileTransfer.DAL.File> sampleFiles = FileIdListToFileList(sampleFilesIds);
            List<FileTransfer.DAL.File> finalFiles = FileIdListToFileList(finalFilesIds);


            
            Delivery delivery = new Delivery()
            {
                ApprovalState = DeliveryApprovalState.Pending,
                ContractingProcess = contractingProcess,
                FinalFiles = finalFiles,
                SampleFiles = sampleFiles,
                FinalComment = finalComment
            };

            var entityEntry = _context.Add(delivery);

            contractingProcess.Deliveries.Add(delivery);

            _context.ContractingProcess.Update(contractingProcess);

            await _context.SaveChangesAsync();

            foreach(FileTransfer.DAL.File file in finalFiles)
            {
                await _fileTransferService.SetInUse(file.FileId);
            }

            foreach (FileTransfer.DAL.File file in sampleFiles)
            {
                await _fileTransferService.SetInUse(file.FileId);
            }

            await _notificationService.SendNotification(contractingProcess.Proposal.Manager,
                $"You recieved a delivery in \"{contractingProcess.Task.Description}\"'s contracting process.",
                $"{contractingProcess.AcceptedCounterProposal.Freelancer.FirstName} {contractingProcess.AcceptedCounterProposal.Freelancer.LastName} submitted a delivery in your ongoing contracting process for task \"{contractingProcess.Task.Description}\". Check the contracting process page to view sample and make a choice.",
                $"/Contracting/{contractingProcess.Id}");

            return entityEntry.Entity;
        }


        /// <summary>
        /// Get all contracting processes where user is freelancer.
        /// </summary>
        /// <param name="userId">Freelancer user id</param>
        /// <returns>List of contracting processes</returns>
        public async Task<List<ContractingProcess>> AllContractingProcessesAsFreelancer(string userId)
        {
            List<ContractingProcess> contractingProcesses = await _context.ContractingProcess
                .Include(p => p.Task)
                .ThenInclude(t => t.SkillTags)
                .Include(p => p.Proposal)
                .ThenInclude(p => p.Manager)
                .Include(p => p.Proposal)
                .ThenInclude(p => p.ReferenceFiles)
                .Include(p => p.CounterProposals)
                .ThenInclude(c => c.Freelancer)
                .Include(p => p.AcceptedCounterProposal)
                .ThenInclude(a => a.Freelancer)
                .Include(p => p.Deliveries)
                .ThenInclude(d => d.FinalFiles)
                .Include(p => p.Deliveries)
                .ThenInclude(d => d.SampleFiles)
                .Where(p => (p.AcceptedCounterProposal != null ? p.AcceptedCounterProposal.Freelancer.Id == userId : true) &&
                    p.CounterProposals.Any(cp => cp.Freelancer.Id == userId)).ToListAsync();

            return contractingProcesses;
        }

        /// <summary>
        /// Get all contracting processes where user is manager.
        /// </summary>
        /// <param name="userId">Manager user id</param>
        /// <returns>List of contracting processes</returns>
        public async Task<List<ContractingProcess>> AllContractingProcessesAsManager(string userId)
        {
            List<ContractingProcess> contractingProcesses = await _context.ContractingProcess
                .Include(p => p.Task)
                .ThenInclude(t => t.SkillTags)
                .Include(p => p.Proposal)
                .ThenInclude(p => p.Manager)
                .Include(p => p.Proposal)
                .ThenInclude(p => p.ReferenceFiles)
                .Include(p => p.CounterProposals)
                .ThenInclude(c => c.Freelancer)
                .Include(p => p.AcceptedCounterProposal)
                .ThenInclude(a => a.Freelancer)
                .Include(p => p.Deliveries)
                .ThenInclude(d => d.FinalFiles)
                .Include(p => p.Deliveries)
                .ThenInclude(d => d.SampleFiles)
                .Where(p => p.Proposal!.Manager!.Id == userId).ToListAsync();

            return contractingProcesses;
        }

        /// <summary>
        /// Get all published contracting processes.
        /// </summary>
        /// <returns>List of contracting processes</returns>
        public List<ContractingProcess> GetAllPublishedContractingProcesses()
        {
            List<ContractingProcess> publishedContractingProcesses = _context.ContractingProcess
                .Include(p => p.Task)
                .ThenInclude(t => t.SkillTags)
                .Include(p => p.Proposal)
                .ThenInclude(p => p.Manager)
                .Where(p => p.State == ContractingProcessState.Published).ToList();
            return publishedContractingProcesses;
        }

        /// <summary>
        /// Get contracting processes by ID.
        /// </summary>
        /// <param name="contractingProcessId">Contracting Process ID</param>
        /// <returns>Contracting process instance</returns>
        public async Task<ContractingProcess> GetContractingProcess(int contractingProcessId)
        {
            ContractingProcess? contractingProcess = await _context.ContractingProcess
                .Include(p => p.Task)
                .ThenInclude(t => t.SkillTags)
                .Include(p => p.Proposal)
                .ThenInclude(p => p.Manager)
                .Include(p => p.Proposal)
                .ThenInclude(p => p.ReferenceFiles)
                .Include(p => p.CounterProposals)
                .ThenInclude(c => c.Freelancer)
                .Include(p => p.AcceptedCounterProposal)
                .ThenInclude(a => a.Freelancer)
                .Include(p => p.Deliveries)
                .ThenInclude(d => d.FinalFiles)
                .Include(p => p.Deliveries)
                .ThenInclude(d => d.SampleFiles)
                .Include(p => p.Payment)
                .Where(p => p.Id == contractingProcessId).FirstOrDefaultAsync();

            if(contractingProcess == null)
            {
                throw new ArgumentException("Contracting Process Id not valid.");
            }

            return contractingProcess!;
        }

        /// <summary>
        /// Get contracting processes by associated task ID.
        /// </summary>
        /// <param name="contractingProcessId">Task ID</param>
        /// <returns>Contracting process instance</returns>
        public async Task<ContractingProcess> GetContractingProcessByTaskId(int taskId)
        {
            ContractingProcess? contractingProcess = await _context.ContractingProcess
                .Include(p => p.Task)
                .ThenInclude(t => t.SkillTags)
                .Include(p => p.Proposal)
                .ThenInclude(p => p.Manager)
                .Include(p => p.Proposal)
                .ThenInclude(p => p.ReferenceFiles)
                .Include(p => p.CounterProposals)
                .ThenInclude(c => c.Freelancer)
                .Include(p => p.AcceptedCounterProposal)
                .ThenInclude(a => a.Freelancer)
                .Include(p => p.Deliveries)
                .ThenInclude(d => d.FinalFiles)
                .Include(p => p.Deliveries)
                .ThenInclude(d => d.SampleFiles)
                .Where(p => p.Task.TaskId == taskId).FirstOrDefaultAsync();

            if (contractingProcess == null)
            {
                throw new ArgumentException("Task with given ID not associated with a contracting process.");
            }

            return contractingProcess!;
        }

        /// <summary>
        /// Get contracting processes by associated payment ID.
        /// </summary>
        /// <param name="contractingProcessId">Payment ID</param>
        /// <returns>Contracting process instance</returns>
        public async Task<ContractingProcess> GetContractingProcessByPaymentId(int paymentId)
        {
            ContractingProcess? contractingProcess = await _context.ContractingProcess
                .Include(p => p.Task)
                .ThenInclude(t => t.SkillTags)
                .Include(p => p.Proposal)
                .ThenInclude(p => p.Manager)
                .Include(p => p.Proposal)
                .ThenInclude(p => p.ReferenceFiles)
                .Include(p => p.CounterProposals)
                .ThenInclude(c => c.Freelancer)
                .Include(p => p.AcceptedCounterProposal)
                .ThenInclude(a => a.Freelancer)
                .Include(p => p.Deliveries)
                .ThenInclude(d => d.FinalFiles)
                .Include(p => p.Deliveries)
                .ThenInclude(d => d.SampleFiles)
                .Include(p => p.Payment)
                .Where(p => p.Payment!.PaymentId == paymentId).FirstOrDefaultAsync();

            if (contractingProcess == null)
            {
                throw new ArgumentException("Task with given ID not associated with a contracting process.");
            }

            return contractingProcess!;
        }

        /// <summary>
        /// Create a new contracting process for a project's task.
        /// </summary>
        /// <param name="userId">Project manager Id.</param>
        /// <param name="taskId">Project task Id.</param>
        /// <param name="description">Description for proposal.</param>
        /// <param name="proposalAmount">Amount, in euros, of proposal.</param>
        /// <param name="referenceFilesIds">List of reference files identifiers.</param>
        /// <param name="deliveryDate">Proposed limit date for delivery.</param>
        /// <param name="counterProposalsEndDate">End date for accepting counter-proposals.</param>
        /// <returns>Contracting process instance</returns>
        /// <exception cref="ArgumentException">User Id not valid or task Id not valid.</exception>
        /// <exception cref="InvalidOperationException">Already exists a contracting process for this task.</exception>
        public async Task<ContractingProcess> PublishTask(string userId, int taskId, string description, float proposalAmount, List<string> referenceFilesIds, DateTime deliveryDate, DateTime counterProposalsEndDate)
        {
            TaskLancerUser? manager = _context.Users.FirstOrDefault(u => u.Id == userId);

            if(manager == null)
            {
                throw new ArgumentException("User Id is not valid.");
            }

            Project.DAL.Task? task = _context.Task.Where(t=> t.TaskId == taskId).FirstOrDefault();

            if(task == null)
            {
                throw new ArgumentException("Task Id is not valid.");
            }

            // Check if there is a process for the same task not canceled
            ContractingProcess? alreadyInContext = await _context.ContractingProcess.FirstOrDefaultAsync(e => e.Task == task && e.State != ContractingProcessState.Canceled);
            // If there is, operation invalid
            if (alreadyInContext != null)
            {
                throw new InvalidOperationException("Already exists a contracting process for this task not cancelled.");
            }

            List<FileTransfer.DAL.File> referenceFiles = FileIdListToFileList(referenceFilesIds);

            Proposal proposal = new Proposal
            {
                Manager = manager,
                Description = description,
                Amount = proposalAmount,
                DeliveryDate = deliveryDate,
                ReferenceFiles = referenceFiles,
            };

            ContractingProcess contractingProcess = new ContractingProcess
            {
                Task = task,
                PublishingDate = DateTime.Now,
                CounterProposalsEndDate = counterProposalsEndDate,
                State = ContractingProcessState.Published,
                Proposal = proposal,
                CounterProposals = new List<CounterProposal>(),
                Deliveries = new List<Delivery>()
            };

           EntityEntry<ContractingProcess> entry = await _context.ContractingProcess.AddAsync(contractingProcess);

            await _context.SaveChangesAsync();

            foreach(FileTransfer.DAL.File file in referenceFiles)
            {
                await _fileTransferService.SetInUse(file.FileId);

            }

            await _contractingSchedulingService.ContractingProcessPublished(entry.Entity);


            return entry.Entity;
        }

        /// <summary>
        /// Remove/decline a counter proposal in contracting process.
        /// </summary>
        /// <param name="userId">Contracting process' manager ID or delivery author ID.</param>
        /// <param name="contractingProcessId">Contracting process ID.</param>
        /// <param name="counterProposalId">Counter Proposal ID</param>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException">Counter proposal does not belong to process, user making request is not manager or freelancer or contracting process isn't in Published state.</exception>
        public async System.Threading.Tasks.Task RemoveCounterProposal(string userId, int contractingProcessId, int counterProposalId)
        {
            // Get contract, throws argument exception if not valid id
            ContractingProcess contractingProcess = await GetContractingProcess(contractingProcessId);

            //Check if counter-proposal belongs to process
            CounterProposal? counterProposal = contractingProcess.CounterProposals.FirstOrDefault(counterProposal => counterProposal.Id == counterProposalId);
            if (counterProposal == null)
                throw new InvalidOperationException("Counter proposal does not belong to process.");

            // Check if User trying to access is manager
            if (userId != contractingProcess.Proposal.Manager.Id &&
                userId != counterProposal.Freelancer.Id)
                throw new InvalidOperationException("User making request is not manager or freelancer.");


            //Check if process is in published state
            if (contractingProcess.State != ContractingProcessState.Published)
                throw new InvalidOperationException("Contracting process must be in Published state.");

            contractingProcess.CounterProposals.Remove(counterProposal);

            await _context.SaveChangesAsync();

            if(userId == contractingProcess.Proposal.Manager.Id)
            {
                RejectCounterProposal(contractingProcess, counterProposal, true);
            }


        }

        /// <summary>
        /// Sets the approval state of a delivery.
        /// </summary>
        /// <param name="userId">Manager ID.</param>
        /// <param name="contractingProcessId">Contracting Process Id.</param>
        /// <param name="deliveryId">Delivery Id.</param>
        /// <param name="newState">Approval state to apply to delivery.</param>
        /// <returns></returns>
        /// <exception cref="ArgumentException">User Id is invalid.</exception>
        /// <exception cref="InvalidOperationException">Requested process is not accepting deliveries,
        /// User is not the process' manager, there is no pending delivery on the requested process,
        /// delivery ID given does not match process pending delivery ID or no payment for contracting process.</exception>
        public async System.Threading.Tasks.Task SetDeliveryApprovalState(string userId, int contractingProcessId, int deliveryId, DeliveryApprovalState newState)
        {
            ContractingProcess contractingProcess = await GetContractingProcess(contractingProcessId);

            TaskLancerUser? manager = await _context.Users.FirstOrDefaultAsync(u => u.Id == userId);

            if (manager == null)
            {
                throw new ArgumentException("No user with requested ID.");
            }

            if (contractingProcess.State != ContractingProcessState.Ordered)
            {
                throw new InvalidOperationException("Requested process is not accepting deliveries.");
            }

            if (contractingProcess.Proposal!.Manager != manager)
            {
                throw new InvalidOperationException("User is not the process' manager.");
            }

            Delivery? delivery = contractingProcess.Deliveries.FirstOrDefault(d => d.ApprovalState == DeliveryApprovalState.Pending);

            if (delivery == null)
                throw new InvalidOperationException("There is no pending delivery on the requested process.");

            if (delivery.Id != deliveryId)
                throw new InvalidOperationException("Delivery ID given does not match process pending delivery ID.");

            delivery.ApprovalState = newState;

            var entityEntry = _context.Update(delivery);
            await _context.SaveChangesAsync();

            // If delivery was approved then move process to completed
            if(newState == DeliveryApprovalState.Approved)
            {
                Payment? payment = await _context.FindAsync<Payment>(contractingProcess.PaymentId);

                if (payment == null)
                    throw new InvalidOperationException("No payment for contracting process.");

                await _paymentService.UnlockPaymentPayout(payment.PaymentId);

                contractingProcess.State = ContractingProcessState.Completed;

                _context.ContractingProcess.Update(contractingProcess!);

                await _ratingService.CreateContractingProcessReviews(contractingProcessId, manager.Id, payment.FreeLancer.Id);
                
                await _context.SaveChangesAsync();

                await _notificationService.SendNotification(contractingProcess.AcceptedCounterProposal!.Freelancer!,
                "Your delivery was approved.",
                $"Your last delivery in {contractingProcess.Task.Title} was approved.", $"/Contracting/{contractingProcess.Id}");

                await _contractingSchedulingService.DeliveryAccepted(contractingProcess);

            }

            if(newState == DeliveryApprovalState.Declined)
            {
                await _notificationService.SendNotification(contractingProcess.AcceptedCounterProposal!.Freelancer!,
                "Your delivery was declined.",
                $"Your last delivery in {contractingProcess.Task.Title} was declined.", $"/Contracting/{contractingProcess.Id}");
            }
            
        }


        /// <summary>
        /// Method to trigger delivery date logic, used by event manager.
        /// </summary>
        /// <param name="contractingProcessId">Triggered contracting process</param>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException">Contracting process not in ordered state.</exception>
        public async System.Threading.Tasks.Task ReachedDeliveryDate(int contractingProcessId)
        {
            ContractingProcess contractingProcess = await GetContractingProcess(contractingProcessId);

            if (contractingProcess.State != ContractingProcessState.Ordered)
            {
                throw new InvalidOperationException("Contracting Process must be in ordered state before reaching delivery date.");
            }

            if (IsTherePendingDelivery(contractingProcess))
            {
                contractingProcess.State = ContractingProcessState.Under_Evaluation;

                await _context.SaveChangesAsync();

                await _contractingSchedulingService.UnderEvaluation(contractingProcess);

                await _notificationService.SendNotification(contractingProcess.Proposal!.Manager!,
                    $"You have 48 hours to decide.",
                    $"Delivery date reached in \"{contractingProcess.Task.Title}\"'s contracting process and there is still a pending delivery." +
                    $"You have 48 hours to make a decision before the process is canceled.",
                    $"/Contracting/{contractingProcess.Id}");
            } else
            {
                await CancelProcess(contractingProcessId, $"You did not approve any delivery and there are no pending deliveries.");
            }

        }

        private bool IsTherePendingDelivery(ContractingProcess contractingProcess)
        {
            foreach(Delivery delivery in contractingProcess.Deliveries)
            {
                if (delivery.ApprovalState == DeliveryApprovalState.Pending)
                    return true;
            }

            return false;
        }

        private List<FileTransfer.DAL.File> FileIdListToFileList(List<string> filesIds)
        {
            List<FileTransfer.DAL.File> files = _context.File.Where(f => filesIds.Contains(f.FileId)).ToList();
            return files;
        }

        /// <summary>
        /// Method to trigger successful payment logic, used by Payment Service
        /// </summary>
        /// <param name="paymentId">Successful paymnt ID</param>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException">Payment ID doesn't match or contracting process is not pending payment.</exception>
        public async System.Threading.Tasks.Task PaymentWasSuccessful(int paymentId)
        {
            ContractingProcess? contractingProcess = await _context.ContractingProcess
                .Include(cp => cp.Task)
                .Include(cp => cp.AcceptedCounterProposal)
                .Include(cp => cp.Payment)
                .FirstOrDefaultAsync(cp => cp.Payment.PaymentId == paymentId);

            if (contractingProcess == null)
                throw new InvalidOperationException("No contracting process payment matches given ID.");

            if (contractingProcess.State != ContractingProcessState.Pending_Payment)
                throw new InvalidOperationException("Contracting process is not pending payment.");

            contractingProcess.State = ContractingProcessState.Ordered;

            _context.ContractingProcess.Update(contractingProcess);

            await _context.SaveChangesAsync();

            await _contractingSchedulingService.TaskOrdered(contractingProcess);

            await _notificationService.SendNotification(contractingProcess.AcceptedCounterProposal!.Freelancer!,
                "Manager completed payment for your accepted counter-proposal.",
                $"The payment for the task '{contractingProcess.Task.Title}' has been secured.", $"/Contracting/{contractingProcess.Id}");
        }

        /// <summary>
        /// Cancels a contracting process, triggering adequate payment resolution and notifying users.
        /// </summary>
        /// <param name="contractingProcessId">Contracting Process Id to cancel.</param>
        /// <param name="notificationMessage">Custom notification message.</param>
        /// <returns></returns>
        public async System.Threading.Tasks.Task CancelProcess(int contractingProcessId, string? notificationMessage)
        {
            ContractingProcess contractingProcess = await GetContractingProcess(contractingProcessId);

            if (contractingProcess.State == ContractingProcessState.Ordered ||
                contractingProcess.State == ContractingProcessState.Delivered ||
                contractingProcess.State == ContractingProcessState.Under_Evaluation ||
                contractingProcess.State == ContractingProcessState.In_Dispute)
            {
                await _paymentService.RefundPayment(contractingProcess.Payment!.PaymentId!);
            }

            if (contractingProcess.State == ContractingProcessState.Pending_Payment)
            {
                await _paymentService.CancelPayment(contractingProcess.Payment!.PaymentId!);
            }

            contractingProcess.State = ContractingProcessState.Canceled;

            _context.ContractingProcess.Update(contractingProcess);

            await _context.SaveChangesAsync();

            await _contractingSchedulingService.RemoveAllPendingEvents(contractingProcess);


            if (contractingProcess.AcceptedCounterProposal != null)
            {
                await _notificationService.SendNotification(contractingProcess.AcceptedCounterProposal!.Freelancer!,
                $"\"{contractingProcess.Task.Title}\"'s contracting process was canceled.",
                $"Your ongoing contracting process for the task \"{contractingProcess.Task.Title}\" has been canceled.", $"/Contracting/{contractingProcess.Id}");
            }
            else
            {
                foreach (CounterProposal counterProposal in contractingProcess.CounterProposals)
                {
                    await _notificationService.SendNotification(counterProposal.Freelancer!,
                $"\"{contractingProcess.Task.Title}\"'s contracting process was canceled.",
                $"The contracting process for the task \"{contractingProcess.Task.Title}\" has been canceled.", $"/Contracting/{contractingProcess.Id}");
                }
            }

            await _notificationService.SendNotification(contractingProcess.Proposal!.Manager!,
                $"\"{contractingProcess.Task.Title}\"'s contracting process was canceled.",
                $"Your contracting process for the task \"{contractingProcess.Task.Title}\" has been canceled. {(notificationMessage != null ? notificationMessage : "")}", $"/Contracting/{contractingProcess.Id}");
        }

        /// <summary>
        /// Get Id of task associated with given contracting process.
        /// </summary>
        /// <param name="contractingProcesId">Contracting process ID</param>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException">No contracting process payment matches given ID</exception>
        public async Task<int> GetTaskId(int contractingProcesId)
        {
            ContractingProcess? contractingProcess = await _context.ContractingProcess.Include(cp => cp.Task).FirstOrDefaultAsync(cp => cp.Id == contractingProcesId);

            if (contractingProcess == null)
                throw new InvalidOperationException("No contracting process payment matches given ID.");

            return contractingProcess.Task.TaskId;
        }
    }
}
