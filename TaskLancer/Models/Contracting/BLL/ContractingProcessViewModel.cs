﻿using TaskLancer.Models.Contracting.DAL;
using TaskLancer.Models.Rating.BLL;

namespace TaskLancer.Models.Contracting.BLL
{
    public class ContractingProcessViewModel
    {
        public int ContractingProcessId { get; set; }

        //Task details
        public int TaskId { get; set; }
        public string TaskTitle { get; set; }
        public string TaskDescription { get; set; }
        public DateTime TaskStartDate { get; set; }
        public DateTime TaskEndDate { get; set; }
        public List<string> TaskSkillTags { get; set; }

        //Proposal details
        public int ProposalId { get; set; }
        public string ProposalDescription { get; set; }
        public float ProposalAmount { get; set; }
        public DateTime ProposalDeliveryDate { get; set; }
        public List<FileTransfer.DAL.File> ReferenceFiles { get; set; }

        //Manager Details
        public string ManagerId { get; set; }
        public string ManagerFirstName { get; set; }
        public string ManagerLastName { get; set; }
        public string ManagerUsername { get; set; }

        //Contracting Process details
        public ContractingPaymentViewModel? PaymentViewModel { get; set; }

        public List<CounterProposalViewModel>? CouterProposalsViewModel { get; set; }

        public CounterProposalViewModel? AcceptedCounterProposal { get; set; }

        public ContractingProcessState State { get; set; }
        public DateTime PublishingDate { get; set; }
        public DateTime CounterProposalsEndDate { get; set; }

        public DeliveryViewModel? DeliveryViewModel { get; set; }

        public ReviewViewModel? ReviewSubmitedViewModel { get; set; }
        public ReviewViewModel? ReviewReceivedViewModel { get; set; }
    }
}
