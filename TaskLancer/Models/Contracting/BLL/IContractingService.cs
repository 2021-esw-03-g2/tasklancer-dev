﻿using TaskLancer.Models.Contracting.DAL;

namespace TaskLancer.Models.Contracting.BLL
{
    public interface IContractingService
    {
        Task<ContractingProcess> PublishTask(string userId,
            int taskId, string description, float proposalAmount,
            List<string> referenceFilesIds, DateTime deliveryDate,
            DateTime counterProposalsEndDate);

        Task<CounterProposal> AddCounterProposal(int contractingProcessId,
            string userId, string selfIntro, float amount, DateTime deliveryDate);

        Task<CounterProposal> EditCounterProposal(int contractingProcessId, int counterProposalId, string userId, string selfIntro, float amount, DateTime deliveryDate);

        Task<ContractingProcess> GetContractingProcess(int contractingProcessId);

        Task<ContractingProcess> GetContractingProcessByTaskId(int taskId);

        Task<ContractingProcess> GetContractingProcessByPaymentId(int paymentId);

        Task RemoveCounterProposal(string userId, int contractingProcessId, int counterProposalId);

        Task AcceptCounterProposal(string userId, int contractingProcessId, int counterProposalId);

        Task<Delivery> AddDelivery(int contractingProcessId, string userId,
            string finalComment, List<string> sampleFilesIds, List<string> finalFilesIds);

        Task ReachedDeliveryDate(int contractingProcessId);

        Task PaymentWasSuccessful(int paymentId);

        Task<List<ContractingProcess>> AllContractingProcessesAsFreelancer(string userId);
        Task<List<ContractingProcess>> AllContractingProcessesAsManager(string userId);

        Task SetDeliveryApprovalState(string userId, int contractingProcessId, int deliveryId, DeliveryApprovalState newState);

        List<ContractingProcess> GetAllPublishedContractingProcesses();
        Task CancelProcess(int contractingProcessId, string? notificationMessage);

        Task<int> GetTaskId(int contractingProcesId);
    }
}
