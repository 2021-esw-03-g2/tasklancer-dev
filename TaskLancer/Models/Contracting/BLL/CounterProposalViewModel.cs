﻿using System.ComponentModel.DataAnnotations;

namespace TaskLancer.Models.Contracting.BLL
{
    public class CounterProposalViewModel
    {
        public int ContractingProcessId { get; set; }
        public int CounterProposalId { get; set; }
        public string? FreelancerId { get; set; }
        public string? FreelancerFirstName { get; set; }
        public string? FreelancerSecondName { get; set; }
        public string FreelancerUsername{ get; set; }
        public string? SelfIntro { get; set; }
        public string Amount { get; set; }
        public DateTime DeliveryDate { get; set; }
        public DateTime LimitDate { get; set; }
    }
}
