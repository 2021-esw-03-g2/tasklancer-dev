﻿using Microsoft.EntityFrameworkCore;
using TaskLancer.Data;
using TaskLancer.Models.Contracting.DAL;
using TaskLancer.Services;

namespace TaskLancer.Models.Contracting.BLL
{
    public class ContractingSchedulingService : IContractingSchedulingService
    {
        private readonly TaskLancerContext _context;


        public ContractingSchedulingService(TaskLancerContext context)
        {
            _context = context;
        }

        public async Task ContractingProcessPublished(ContractingProcess contractingProcess)
        {
             AddPendingEvent(contractingProcess.Id, ContractingEventType.NoCounterProposalAccepted, contractingProcess.CounterProposalsEndDate);

            await _context.SaveChangesAsync();

        }

        public async Task CounterProposalAccepted(ContractingProcess contractingProcess)
        {

            await RemovePendingEvent(contractingProcess.Id, ContractingEventType.NoCounterProposalAccepted);

            AddPendingEvent(contractingProcess.Id, ContractingEventType.NoSuccessfulPayment, contractingProcess.CounterProposalsEndDate.AddHours(48));

            await _context.SaveChangesAsync();
        }

        public async Task TaskOrdered(ContractingProcess contractingProcess)
        {
            await RemovePendingEvent(contractingProcess.Id, ContractingEventType.NoSuccessfulPayment);

            AddPendingEvent(contractingProcess.Id, ContractingEventType.ReachedDeliveryDate, contractingProcess.AcceptedCounterProposal!.DeliveryDate.AddHours(48));

            await _context.SaveChangesAsync();
        }

        public async Task DeliveryAccepted(ContractingProcess contractingProcess)
        {

            //await RemovePendingEvent(contractingProcess.Id, ContractingEventType.NoDeliveryAccepted);

            await RemoveAllPendingEvents(contractingProcess);

            //await _context.SaveChangesAsync();
        }

        private async Task RemovePendingEvent(int contractingProcessId, ContractingEventType contractingEventType)
        {
            ContractingEvent? contractingEvent = await _context.ContractingEvents
                .FirstOrDefaultAsync(e => e.ContractingProcessId == contractingProcessId && e.EventType == contractingEventType);

            if (contractingEvent == null)
                throw new InvalidOperationException($"Can't find event of type '{contractingEventType}' to remove.");

            _context.ContractingEvents.Remove(contractingEvent);
        }

        private void AddPendingEvent(int contractingProcessId, ContractingEventType eventType, DateTime triggerDate)
        {
            ContractingEvent contractingEvent = new ContractingEvent()
            {
                ContractingProcessId = contractingProcessId,
                EventType = eventType,
                TriggerDate = triggerDate
            };

            _context.ContractingEvents.Add(contractingEvent);
        }

        public async Task UnderEvaluation(ContractingProcess contractingProcess)
        {
            AddPendingEvent(contractingProcess.Id, ContractingEventType.NoDeliveryAccepted, contractingProcess.AcceptedCounterProposal!.DeliveryDate.AddHours(48));

            await _context.SaveChangesAsync();
        }

        

        public async Task RemoveAllPendingEvents(ContractingProcess contractingProcess)
        {
            List<ContractingEvent> contractingEvents = await _context.ContractingEvents
                .Where(e => e.ContractingProcessId == contractingProcess.Id).ToListAsync();

            foreach(ContractingEvent contractingEvent in contractingEvents)
            {
                _context.ContractingEvents.Remove(contractingEvent);
            }

            await _context.SaveChangesAsync();
        }
    }
}
