﻿using TaskLancer.Models.Payments.DAL;

namespace TaskLancer.Models.Contracting.BLL
{
    public class ContractingPaymentViewModel
    {
        public int PaymentId { get; set; }
        public int ContractingProcessId { get; set; }
        public float ServiceFee { get; set; }
        public float SubTotal { get; set; }
        public float Total { get; set; }
        public string? Currency { get; set; }
        public PaymentState? State { get; set; }
        public DateTime? LimitDate { get; set; }
        public string? Title { get; set; }
    }
}
