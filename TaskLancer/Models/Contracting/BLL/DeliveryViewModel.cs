﻿using TaskLancer.Models.Contracting.DAL;
using TaskLancer.Views.FileTransfer;

namespace TaskLancer.Models.Contracting.BLL
{
    public class DeliveryViewModel
    {
        public int DeliveryId { get; set; }
        public int ContractingProcessId { get; set; }
        public string FinalComment { get; set; }
        public DeliveryApprovalState ApprovalState { get; set; }
        public DateTime LimitDate { get; set; }
        public string? FinalFileIdJson { get; set; }
        public string? SampleFileIdJson { get; set; }

        public List<FileTransfer.DAL.File>? SampleFiles { get; set; }
        public List<FileTransfer.DAL.File>? FinalFiles { get; set; }

        public UploadPartialViewModel? UploadFinalFilePartialViewModel { get; set; }
        public UploadPartialViewModel? UploadSampleFilePartialViewModel { get; set; }
    }
}
