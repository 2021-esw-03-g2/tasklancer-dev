﻿using Microsoft.EntityFrameworkCore;
using TaskLancer.Data;
using TaskLancer.Models.Contracting.DAL;
using TaskLancer.Services;

namespace TaskLancer.Models.Contracting.BLL
{
    public class ContractingScopedSchedulerService : IScopedSchedulerService
    {
        private readonly TaskLancerContext _context;
        private readonly IContractingService _contractingService;

        public ContractingScopedSchedulerService(TaskLancerContext context, IContractingService contractingService)
        {
            _context = context;
            _contractingService = contractingService;
        }
        public async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            List<ContractingEvent> contractingEvents = await _context.ContractingEvents.Where(e => e.TriggerDate <= DateTime.Now).ToListAsync();

            foreach (ContractingEvent contractingEvent in contractingEvents)
            {
                await HandleEvent(contractingEvent);
                _context.ContractingEvents.Remove(contractingEvent);
            }

            try
            {
                await _context.SaveChangesAsync();
            } catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private async Task HandleEvent(ContractingEvent contractingEvent)
        {
            switch (contractingEvent.EventType)
            {
                case ContractingEventType.NoCounterProposalAccepted:
                    await _contractingService.CancelProcess(contractingEvent.ContractingProcessId, $"No counter-proposal was accepted before {contractingEvent.TriggerDate}.");
                    break;
                case ContractingEventType.NoSuccessfulPayment:
                    await _contractingService.CancelProcess(contractingEvent.ContractingProcessId, $"No successful payment was completed before {contractingEvent.TriggerDate}.");
                    break;
                case ContractingEventType.ReachedDeliveryDate:
                    await _contractingService.ReachedDeliveryDate(contractingEvent.ContractingProcessId);
                    break;
                case ContractingEventType.NoDeliveryAccepted:
                    await _contractingService.CancelProcess(contractingEvent.ContractingProcessId, $"No final delivery was accepted before {contractingEvent.TriggerDate}.");
                    break;
            }
        }
    }
}
