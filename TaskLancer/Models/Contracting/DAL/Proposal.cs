﻿using System.ComponentModel.DataAnnotations.Schema;
using TaskLancer.Areas.Identity.Data;

namespace TaskLancer.Models.Contracting.DAL
{
    public class Proposal
    {
        public int Id { get; set; }
        public TaskLancerUser Manager  { get; set; }

        [ForeignKey("ContractingProcess")]
        public int ContractingProcessId { get; set; }
        public ContractingProcess ContractingProcess { get; set; }

        public string Description { get; set; }
        public float Amount { get; set; }
        public DateTime DeliveryDate { get; set; }

        public List<FileTransfer.DAL.File> ReferenceFiles { get; set; }

    }
}
