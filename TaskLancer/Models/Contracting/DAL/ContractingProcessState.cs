﻿namespace TaskLancer.Models.Contracting.DAL
{
    public enum ContractingProcessState
    {
        Published,
        Pending_Payment,
        Ordered,
        Delivered,
        Under_Evaluation,
        Completed,
        In_Dispute,
        Canceled
    }
}
