﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TaskLancer.Models.Contracting.DAL
{
    public class ContractingEvent
    {
        public int Id { get; set; }

        [ForeignKey("ContractingProcess")]
        public int ContractingProcessId { get; set; }
        [Required]
        public ContractingProcess ContractingProcess { get; set; }
        [Required]
        public DateTime TriggerDate { get; set; }

        [Required]
        public ContractingEventType EventType { get; set; }
    }
}
