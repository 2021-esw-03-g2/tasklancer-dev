﻿using System.ComponentModel.DataAnnotations.Schema;

namespace TaskLancer.Models.Contracting.DAL
{
    public class Delivery
    {
        public int Id { get; set; }

        [ForeignKey("ContractingProcess")]
        public int ContractingProcessId { get; set; }
        public ContractingProcess ContractingProcess { get; set; }
        public string FinalComment { get; set; }

        public List<FileTransfer.DAL.File> SampleFiles { get; set; }

        public List<FileTransfer.DAL.File> FinalFiles { get; set; }

        public DeliveryApprovalState ApprovalState { get; set; }

    }
}
