﻿namespace TaskLancer.Models.Contracting.DAL
{
    public enum DeliveryApprovalState
    {
        Pending,
        Approved,
        Declined
    }
}
