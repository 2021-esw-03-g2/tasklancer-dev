﻿namespace TaskLancer.Models.Contracting.DAL
{
    public enum ContractingEventType
    {
        NoCounterProposalAccepted,
        NoSuccessfulPayment,
        ReachedDeliveryDate,
        NoDeliveryAccepted
    }
}
