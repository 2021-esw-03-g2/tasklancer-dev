﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TaskLancer.Areas.Identity.Data;

namespace TaskLancer.Models.Contracting.DAL
{
    public class CounterProposal
    {
        public int Id { get; set; }
        public TaskLancerUser Freelancer { get; set; }

        [ForeignKey("ContractingProcess")]
        public int ContractingProcessId { get; set; }
        public ContractingProcess ContractingProcess { get; set; }

        public string SelfIntro { get; set; }
        public float Amount { get; set; }
        public DateTime DeliveryDate { get; set; }

    }
}
