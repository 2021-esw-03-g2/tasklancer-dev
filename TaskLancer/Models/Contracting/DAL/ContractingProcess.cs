﻿using System.ComponentModel.DataAnnotations.Schema;
using TaskLancer.Areas.Identity.Data;
using TaskLancer.Models.Payments.DAL;

namespace TaskLancer.Models.Contracting.DAL
{
    public class ContractingProcess
    {
        public int Id { get; set; }
        public Project.DAL.Task Task { get; set; }

        public Proposal Proposal { get; set; }

        public List<CounterProposal> CounterProposals { get; set; }

        [ForeignKey("AcceptedCounterProposal")]
        public int? AcceptedCounterProposalId { get; set; }
        public CounterProposal? AcceptedCounterProposal { get; set; }
        
        //public TaskLancerUser Manager { get; set; }
        //public TaskLancerUser Freelancer { get; set; }

        [ForeignKey("Payment")]
        public int? PaymentId { get; set; }
        public Payment? Payment { get; set; }
        public ContractingProcessState State { get; set; }
        public DateTime PublishingDate { get; set; }
        public DateTime CounterProposalsEndDate { get; set; }

        public List<Delivery> Deliveries { get; set; }
    }
}
