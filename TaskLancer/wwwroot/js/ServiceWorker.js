﻿self.addEventListener('fetch', function (event) { });

self.addEventListener('push', function (e) {

    var payload = e.data.json();
    console.dir(payload);

    var options = {
        body: payload.content,
        vibrate: [100, 50, 100],
        data: {
            dateOfArrival: Date.now(),
            url: payload.link,
        },
        actions: [
            {
                action: "explore", title: "Check!", 
            },
            {
                action: "close", title: "Ignore",
            },
        ]
    };
    e.waitUntil(
        self.registration.showNotification(payload.description, options)
    );
});

self.addEventListener('notificationclick', function (e) {
    var notification = e.notification;
    var action = e.action;
    var url = e.notification.data.url;

    if (action === 'close') {
        notification.close();
    } else {
        // Some actions
        clients.openWindow(location.origin + url);
        notification.close();
    }
});