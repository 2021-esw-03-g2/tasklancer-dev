using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.EntityFrameworkCore;
using TaskLancer.Areas.Identity.Data;
using TaskLancer.Data;
using TaskLancer.Models;
using TaskLancer.Models.BackOffice.BLL;
using TaskLancer.Models.Catalog.BLL;
using TaskLancer.Models.Contracting.BLL;
using TaskLancer.Models.FileTransfer.BLL;
using TaskLancer.Models.Notifications.BLL;
using TaskLancer.Models.Payments.BLL;
using TaskLancer.Models.Project.BLL;
using TaskLancer.Models.Rating.BLL;
using TaskLancer.Services;
using TaskLancer.Services.Scheduler;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
var connectionString = builder.Configuration.GetConnectionString("DefaultConnection");
builder.Services.AddDbContext<TaskLancerContext>(options =>
    options.UseSqlServer(connectionString));
builder.Services.AddDatabaseDeveloperPageExceptionFilter();

builder.Services.AddDefaultIdentity<TaskLancerUser>(options => options.SignIn.RequireConfirmedAccount = true)
    .AddRoles<IdentityRole>()
    .AddEntityFrameworkStores<TaskLancerContext>();
builder.Services.AddControllersWithViews();
builder.Services.AddScoped<IProfileService, ProfileService>();
builder.Services.AddScoped<ISkillTagService, SkillTagService>();
builder.Services.AddScoped<IProjectService, ProjectService>();
builder.Services.AddTransient<IEmailSender, EmailSender>();
builder.Services.AddScoped<IEmailNotificationService, EmailNotificationService>();
builder.Services.AddScoped<INotificationService, NotificationService>();
builder.Services.AddScoped<IPaymentService, PaymentService>();
builder.Services.AddScoped<IFileTransferService, FileTransferService>();
builder.Services.AddScoped<IContractingService, ContractingService>();
builder.Services.AddHostedService<SchedulerService>();
builder.Services.AddScoped<IScopedSchedulerService, ContractingScopedSchedulerService>();
builder.Services.AddScoped<IContractingSchedulingService, ContractingSchedulingService>();
builder.Services.AddScoped<IBackOfficeService, BackOfficeService>();
builder.Services.AddScoped<ICategoryTagService, CategoryTagService>();
builder.Services.AddScoped<ICatalogService, CatalogService>();
builder.Services.AddScoped<IRatingService, RatingService>();
builder.Services.AddScoped<IStatisticsService, StatisticsService>();


builder.Services.AddCors();

var app = builder.Build();

using (var scope = app.Services.CreateScope())
{
    var userManager = scope.ServiceProvider.GetService<UserManager<TaskLancerUser>>();
    var roleManager = scope.ServiceProvider.GetService<RoleManager<IdentityRole>>();
    var context = scope.ServiceProvider.GetService<TaskLancerContext>();


    await SeedData.Seed(userManager, roleManager, context);
}

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseMigrationsEndPoint();
}
else
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthentication();
app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=DashBoard}/{action=MyProjects}/{id?}");
app.MapRazorPages();

app.UseCors(option => option.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());

app.Run();

