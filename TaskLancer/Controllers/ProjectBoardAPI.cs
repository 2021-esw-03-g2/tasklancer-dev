﻿#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Primitives;
using TaskLancer.Areas.Identity.Data;
using TaskLancer.Data;
using TaskLancer.Models;
using TaskLancer.Models.Contracting.BLL;
using TaskLancer.Models.Contracting.DAL;
using TaskLancer.Models.Project.BLL;
using TaskLancer.Models.Project.DAL;
using TaskLancer.Services;

namespace TaskLancer.Controllers
{
    [Route("api/ProjectBoard")]
    [ApiController]
    public class ProjectBoardAPI : ControllerBase
    {
        private readonly TaskLancerContext _context;
        private readonly UserManager<TaskLancerUser> _userManager;
        private readonly IProjectService _projectService;
        private readonly ISkillTagService _skillTagService;
        private readonly IContractingService _contractingService;

        public ProjectBoardAPI(TaskLancerContext context, UserManager<TaskLancerUser> userManager, IProjectService projectService, ISkillTagService skillTagService, IContractingService contractingService)
        {
            _context = context;
            _userManager = userManager;
            _projectService = projectService;
            _skillTagService = skillTagService;
            _contractingService = contractingService;
        }

        //// GET: api/ProjectBoardAPI
        //[HttpGet]
        //public async Task<ActionResult<IEnumerable<BoardDTO>>> GetBoard()
        //{
        //    return await _context.Board.Select(x => BoardToDTO(x)).ToListAsync();
        //}

        // GET: api/ProjectBoardAPI/5
        [HttpGet("{id}")]
        public async Task<ActionResult<BoardDTO>> GetBoard(int id)
        {
            try
            {
                string token = GetJWTFromHeader();
                
                Project project = await _projectService.GetProject(id);

                if (!project.IsPublic)
                {
                    if (token != null)
                    {
                        if (!_projectService.validateJWT(token, id))
                            return BadRequest();
                    }
                    else
                    {
                        return BadRequest();
                    }
                    
                }

                Board board = await _projectService.GetBoard(id);

                return await BoardToDTO(board);
            }
            catch (Exception ex)
            {
                return NotFound();
            }
            
        }

        // PUT: api/ProjectBoardAPI/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /*[HttpPut("{id}")]
        public async Task<IActionResult> PutBoard(int id, [FromBody] BoardDTO resource)
        {
            if (id != resource.BoardId)
            {
                return BadRequest();
            }

            if (resource.Token != null)
            {
                if (!_projectService.validateJWT(resource.Token, id))
                    return BadRequest();
            }
            else
            {
                return BadRequest();
            }

            var board = await _context.Board.Include(b => b.Project).FirstOrDefaultAsync(b => b.BoardId == id);

            board.ColumnsOrder = resource.ColumnsOrder;

            _context.Entry(board).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BoardExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }*/

        // POST: api/ProjectBoardAPI
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        //[HttpPost]
        //public async Task<ActionResult<Board>> PostBoard(Board board)
        //{
        //    try
        //    {
        //        //string token = GetJWTFromHeader();

        //        //if (token != null)
        //        //{
        //        //    if (!_projectService.validateJWT(token, id))
        //        //        return BadRequest();
        //        //}
        //        //else
        //        //{
        //        //    return BadRequest();
        //        //}

        //        _context.Board.Add(board);
        //        await _context.SaveChangesAsync();

        //        return CreatedAtAction(nameof(GetBoard), new { id = board.BoardId }, board);
        //    }
        //    catch(Exception ex)
        //    {
        //        return NotFound();
        //    }
            
        //}

        // DELETE: api/ProjectBoardAPI/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBoard(int id)
        {
            try
            {
                string token = GetJWTFromHeader();

                if(token != null)
                {
                    if (!_projectService.validateJWT(token, id))
                    return BadRequest();
                }
                else
                {
                    return BadRequest();
                }

                await _projectService.RemoveBoard(id);

                return NoContent();
            }
            catch(Exception ex)
            {
                return NotFound();
            }
            
        }

        //private bool BoardExists(int id)
        //{
        //    return _context.Board.Any(e => e.BoardId == id);
        //}


        //Columns operations

        [HttpGet("{boardId}/column/{columnId}")]
        public async Task<ActionResult<ColumnDTO>> GetColumn(int boardId, int columnId)
        {
            string token = GetJWTFromHeader();

            if (token != null)
            {
                if (!_projectService.validateJWT(token, boardId))
                    return BadRequest();
            }
            else
            {
                return BadRequest();
            }

            Board? board = await _context.Board.FirstOrDefaultAsync(b => b.BoardId == boardId);
            if (board == null)
            {
                return BadRequest();
            }

            Column? column = await _context.Column.Include(c => c.Tasks).FirstOrDefaultAsync(c => c.ColumnId == columnId);
            if (column == null)
            {
                return BadRequest();
            }

            return await ColumnToDTO(column);
        }

        [HttpPost("{boardId}/column")]
        public async Task<ActionResult<ColumnDTO>> CreateColumn(int boardId, [FromBody] ColumnDTO resource)
        {
            string token = GetJWTFromHeader();

            if (token != null)
            {
                if (!_projectService.validateJWT(token, boardId))
                    return BadRequest();
            }
            else
            {
                return BadRequest();
            }

            Column column = new Column()
            {
                Name = resource.Name,
                Color = resource.Color
            };

            column = await _projectService.CreateColumn(boardId, column);
            if (column == null)
            {
                return BadRequest();
            }

            return await ColumnToDTO(column);
        }

        [HttpPut("{boardId}/column/")]
        public async Task<ActionResult<ColumnDTO>> EditColumn(int boardId, [FromBody] ColumnDTO resource)
        {
            string token = GetJWTFromHeader();

            if (token != null)
            {
                if (!_projectService.validateJWT(token, boardId))
                    return BadRequest();
            }
            else
            {
                return BadRequest();
            }

            Column column = await _context.Column.FirstOrDefaultAsync(c=> c.ColumnId == resource.ColumnId);

            column.Name = resource.Name;
            column.Color = resource.Color;

            return await ColumnToDTO(_projectService.EditColumn(boardId, column).Result);
        }

        [HttpPost("{boardId}/column/{columnId}/newPosition/{newPosition}")]
        public async Task<IActionResult> MoveColumn(int boardId, int columnId, int newPosition)
        {
            string token = GetJWTFromHeader();

            if (token != null)
            {
                if (!_projectService.validateJWT(token, boardId))
                    return BadRequest();
            }
            else
            {
                return BadRequest();
            }

            await _projectService.MoveColumn(boardId, columnId, newPosition);

            return Ok();
        }

        [HttpDelete("{boardId}/column/{columnId}")]
        public async Task<IActionResult> RemoveColumn(int boardId, int columnId)
        {
            string token = GetJWTFromHeader();

            if (token != null)
            {
                if (!_projectService.validateJWT(token, boardId))
                    return BadRequest();
            }
            else
            {
                return BadRequest();
            }

            await _projectService.RemoveColumn(boardId, columnId);
            return Ok();
        }

        //Tasks operations

        [HttpGet("{boardId}/task/{taskId}")]
        public async Task<ActionResult<TaskDTO>> GetTask(int boardId, int taskId)
        {
            string token = GetJWTFromHeader();

            if (token != null)
            {
                if (!_projectService.validateJWT(token, boardId))
                    return BadRequest();
            }
            else
            {
                return BadRequest();
            }

            Board? board = await _context.Board.FirstOrDefaultAsync(b => b.BoardId == boardId);
            if (board == null)
            {
                return BadRequest();
            }

            Models.Project.DAL.Task? task = await _context.Task.FirstOrDefaultAsync(t => t.TaskId == taskId);
            if(task == null)
            {
                return BadRequest();
            }

            return await TaskToDTO(task);
        }
        
        [HttpPost("{boardId}/column/{columnId}/task")]
        public async Task<ActionResult<TaskDTO>> CreateTask(int boardId, int columnId, [FromBody] TaskDTO resource)
        {
            string token = GetJWTFromHeader();

            if (token != null)
            {
                if (!_projectService.validateJWT(token, boardId))
                    return BadRequest();
            }
            else
            {
                return BadRequest();
            }

            List<SkillTag> skillTags = new List<SkillTag>();

            if (resource.SkillTags != null)
            {
                foreach (SkillTagDTO skill in resource.SkillTags)
                {
                    skillTags.Add(await _context.SkillTag.FindAsync(skill.SkillTagId));
                }
            }
            //construir objeto task
            Models.Project.DAL.Task task = new Models.Project.DAL.Task()
            {
                Title = resource.Title,
                Description = resource.Description,
                StartDate = resource.StartDate,
                EndDate = resource.EndDate,
                SkillTags = skillTags
            };

            //converter para dto
            //retornar taskDto
            task = await _projectService.CreateTask(boardId, columnId, task);
            if (task == null)
            {
                return BadRequest();
            }

            return await TaskToDTO(task);
        }

        [HttpPost("{boardId}/task/{taskId}/fromColumn/{fromColumnId}/toColumn/{toColumnId}/position/{newPosition}/")]
        public async Task<IActionResult> MoveTask(int boardId, int taskId, int fromColumnId, int toColumnId, int newPosition)
        {
            string token = GetJWTFromHeader();

            if (token != null)
            {
                if (!_projectService.validateJWT(token, boardId))
                    return BadRequest();
            }
            else
            {
                return BadRequest();
            }

            await _projectService.MoveTask(boardId, taskId, fromColumnId, toColumnId, newPosition);

            return Ok();
        }

        [HttpDelete("{boardId}/column/{columnId}/task/{taskId}")]
        public async Task<IActionResult> RemoveTask(int boardId, int columnId, int taskId)
        {
            string token = GetJWTFromHeader();

            if (token != null)
            {
                if (!_projectService.validateJWT(token, boardId))
                    return BadRequest();
            }
            else
            {
                return BadRequest();
            }

            await _projectService.RemoveTask(boardId, columnId, taskId);


            return Ok();
        }

        [HttpPut("{boardId}/task")]
        public async Task<IActionResult> EditTask(int boardId, [FromBody] TaskDTO resource)
        {
            string token = GetJWTFromHeader();

            if (token != null)
            {
                if (!_projectService.validateJWT(token, boardId))
                    return BadRequest();
            }
            else
            {
                return BadRequest();
            }

            List<SkillTag> skillTags = new List<SkillTag>();

            if (resource.SkillTags != null)
            {
                foreach (SkillTagDTO skill in resource.SkillTags)
                {
                    skillTags.Add(await _context.SkillTag.FindAsync(skill.SkillTagId));
                }
            }

            Models.Project.DAL.Task task = new Models.Project.DAL.Task()
            {
                TaskId = (int)resource.TaskId,
                Title = resource.Title,
                Description = resource.Description,
                StartDate = resource.StartDate,
                EndDate = resource.EndDate,
                SkillTags = skillTags
            };

            await _projectService.EditTask(boardId, task);

            return Ok();
        }

        [HttpGet("skilltags")]
        public async Task<ActionResult<List<SkillTagDTO>>> GetSkillTags()
        {
            List<SkillTag> skillTags = await _skillTagService.GetAllAsync();

            List<SkillTagDTO> dtos = skillTags.Select(s => SkillTagToDTO(s)).ToList();

            return dtos;

        }

        private async Task<BoardDTO> BoardToDTO(Board board) => new BoardDTO
        {
            BoardId = board.BoardId,
            Columns = board.Columns != null ? await SortColumns(board.Columns.ToList(), board.ColumnsOrder) : new List<ColumnDTO>(),
        };

        private async Task<ColumnDTO> ColumnToDTO(Column column) => new ColumnDTO
        {
            ColumnId = column.ColumnId,
            Name = column.Name,
            Color = column.Color,
            Tasks = column.Tasks != null ? await SortTasks(column.Tasks, column.TasksOrder) : new List<TaskDTO>(),
        };

        private async  Task<TaskDTO> TaskToDTO(Models.Project.DAL.Task task) 
        {
            ContractingProcess? cp = null;
            try
            {
                cp = await _contractingService.GetContractingProcessByTaskId(task.TaskId);

            }catch (Exception ex)
            {

            }

            return new TaskDTO
            {
                TaskId = task.TaskId,
                Title = task.Title,
                Description = task.Description,
                StartDate = task.StartDate,
                EndDate = task.EndDate,
                SkillTags = task.SkillTags != null ? task.SkillTags.Select(s => SkillTagToDTO(s)).ToList() : new List<SkillTagDTO>(),
                HasContractingProcess = cp != null
                };
        } 

        private static SkillTagDTO SkillTagToDTO(SkillTag skill) => new SkillTagDTO
        {
            SkillTagId = skill.SkillTagId,
            Name = skill.Name,
            Description = skill.Description,
        };

        private async Task<List<ColumnDTO>> SortColumns(List<Column> columns, string columnsOrder)
        {
            List<int> ids = ConvertIdsOrderInList(columnsOrder);
            List<ColumnDTO> columnsSorted = new List<ColumnDTO>();
            foreach (int id in ids)
            {
                columnsSorted.Add(await ColumnToDTO(columns.FirstOrDefault(c => c.ColumnId == id)));
            }

            return columnsSorted;
        }

        private async Task<List<TaskDTO>> SortTasks(List<Models.Project.DAL.Task> tasks, string tasksorder)
        {
            List<int> ids = ConvertIdsOrderInList(tasksorder);
            List<TaskDTO> tasksSorted = new List<TaskDTO>();
            foreach (int id in ids)
            {
                tasksSorted.Add(await TaskToDTO(tasks.FirstOrDefault(t => t.TaskId == id)));
            }

            return tasksSorted;

        }

        static private List<int> ConvertIdsOrderInList(string idsOrder)
        {
            List<int>? idList = JsonSerializer.Deserialize<List<int>>(idsOrder);

            if (idList == null)
            {
                return new List<int>();
            }

            return idList;
        }


        private string GetJWTFromHeader()
        {
            var header = Request.Headers;
            string token = null;

            StringValues tokenHeader;
            if (header.TryGetValue("JWT", out tokenHeader))
            {
                token = tokenHeader.First();
            }

            return token;
        }

        [HttpGet("testGenerateJWT")]
        public async Task<ActionResult<string>> testGenerateJWT()
        {
            string userId = "a951bf49-59dd-428e-96e9-51f25a687f40";
            int boardId = 1;

            string token = _projectService.generateJwt(userId, boardId);

            return token;
        }

        [HttpPost("testValidateJWT")]
        public async Task<ActionResult<bool>> testValidateJWT([FromBody] TestJWT payload)
        {
            string x = payload.Token;
            return _projectService.validateJWT(x, 1);
        }

        public class TestJWT
        {
            public string Token { get; set; }
        }
    }
}
