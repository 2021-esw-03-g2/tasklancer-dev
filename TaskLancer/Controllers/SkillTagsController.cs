﻿#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TaskLancer.Data;
using TaskLancer.Models;
using TaskLancer.Services;

namespace TaskLancer.Controllers
{
    public class SkillTagsController : Controller
    {
        private readonly TaskLancerContext _context;
        private readonly ISkillTagService _skilltagservice;
        public SkillTagsController(TaskLancerContext context, ISkillTagService skilltagservice)
        {
            _context = context;
            _skilltagservice = skilltagservice;
        }

        // GET: SkillTags
        [Authorize(Roles = "administrator")]
        public async Task<IActionResult> Index()
        {
            return View(await _context.SkillTag.ToListAsync());
        }

        // GET: SkillTags/Details/5
        [Authorize(Roles = "administrator")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var skillTag = await _context.SkillTag
                .FirstOrDefaultAsync(m => m.SkillTagId == id);
            if (skillTag == null)
            {
                return NotFound();
            }

            return View(skillTag);
        }

        // GET: SkillTags/Create
        [Authorize(Roles = "administrator")]
        public IActionResult Create()
        {
            ViewBag.Message = "";
            return View();
        }

        // POST: SkillTags/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "administrator")]
        public async Task<IActionResult> Create([Bind("SkillTagId,Name,Description")] SkillTag skillTag)
        {
            if (ModelState.IsValid)
            {
                    //chamar serviço
                if (!_skilltagservice.CheckIfExists(skillTag))
                {
                    await _skilltagservice.AddSkillTag(skillTag.Name, skillTag.Description);
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    ViewBag.Message = "SkillTag Already Exists";
                    return View(skillTag);
                }
            }
            return View(skillTag);
        }

        // GET: SkillTags/Edit/5
        [Authorize(Roles = "administrator")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var skillTag = await _context.SkillTag.FindAsync(id);
            if (skillTag == null)
            {
                return NotFound();
            }
            ViewBag.Message = "";
            return View(skillTag);
        }

        // POST: SkillTags/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "administrator")]
        public async Task<IActionResult> Edit(int id, [Bind("SkillTagId,Name,Description")] SkillTag skillTag)
        {
            if (id != skillTag.SkillTagId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    //chamar serviço
                    SkillTag oldtag = _context.SkillTag.Where<SkillTag>(e => e.SkillTagId == id).FirstOrDefault();
                    if (oldtag.Name.Equals(skillTag.Name))
                    {
                        await _skilltagservice.ChangeSkillTag(skillTag.SkillTagId, skillTag.Name, skillTag.Description);
                    }
                    else
                    {
                        if (!_skilltagservice.CheckIfExists(skillTag))
                        {
                            await _skilltagservice.ChangeSkillTag(skillTag.SkillTagId, skillTag.Name, skillTag.Description);
                        }
                        else
                        {
                            ViewBag.Message = "SkillTag Already Exists";
                            return View(skillTag);
                        }
                    }       
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SkillTagExists(skillTag.SkillTagId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(skillTag);
        }

        // GET: SkillTags/Delete/5
        [Authorize(Roles = "administrator")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var skillTag = await _context.SkillTag
                .FirstOrDefaultAsync(m => m.SkillTagId == id);
            if (skillTag == null)
            {
                return NotFound();
            }

            return View(skillTag);
        }

        // POST: SkillTags/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "administrator")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            //chamar serviço
            await _skilltagservice.DeleteSkillTag(id);
            return RedirectToAction(nameof(Index));
        }

        private bool SkillTagExists(int id)
        {
            return _context.SkillTag.Any(e => e.SkillTagId == id);
        }
    }
}
