﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Text.Json;
using TaskLancer.Areas.Identity.Data;
using TaskLancer.Data;
using TaskLancer.Models;
using TaskLancer.Models.Contracting.BLL;
using TaskLancer.Models.Contracting.DAL;
using TaskLancer.Models.Payments.BLL;
using TaskLancer.Models.Project.BLL;
using TaskLancer.Models.Rating.BLL;
using TaskLancer.Models.Rating.DAL;

namespace TaskLancer.Controllers
{

    public class ContractingController : Controller
    {
        private readonly TaskLancerContext _context;
        private readonly UserManager<TaskLancerUser> _userManager;
        private readonly IContractingService _contractingService;
        private readonly IPaymentService _paymentService;
        private readonly ILogger<ContractingController> _logger;
        private readonly IProjectService _projectService;
        private readonly IRatingService _ratingService;

        public ContractingController(TaskLancerContext context, UserManager<TaskLancerUser> userManager, IContractingService contractingService, IPaymentService paymentService, ILogger<ContractingController> logger, IProjectService projectService, IRatingService ratingService)
        {
            _context = context;
            _userManager = userManager;
            _contractingService = contractingService;
            _paymentService = paymentService;
            _logger = logger;
            _projectService = projectService;
            _ratingService = ratingService;
        }

        [HttpGet("/Contracting/{id}")]
        [Authorize]
        public async Task<IActionResult> Details(int id, string? message)
        {
            try
            {
                ContractingProcess cp = await _contractingService.GetContractingProcess(id);

                System.Security.Claims.ClaimsPrincipal currentUser = this.User;

                TaskLancerUser user = await _userManager.GetUserAsync(currentUser);

                ViewData["currentUser"] = user;
                ViewData["message"] = message;

                ContractingProcessViewModel cpvm = await GenerateContractingProcessViewModelAsync(cp);

                return View(cpvm);
            }
            catch(Exception ex)
            {
                //RouteValueDictionary routeValues = new RouteValueDictionary();
                //routeValues.Add("title", "Countracting process not found");
                //routeValues.Add("description", ex.Message);

                return RedirectToAction("CustomError", "Home", new { title = "Countracting process not found", description = ex.Message}) ;
            }          
        }

        [Authorize]
        public async Task<IActionResult> ByTask(int id)
        {
            try
            {
                ContractingProcess cp = await _contractingService.GetContractingProcessByTaskId(id);

                return RedirectToAction("Details", new { id = cp.Id });
            }
            catch(Exception ex)
            {
                return RedirectToAction("CustomError", "Home", new { title = "Countracting process not found", description = ex.Message });
            }
            
        }

        [HttpGet("/Contracting/PublishTask/{id}")]
        [Authorize]
        public async Task<IActionResult> PublishTask(int id)
        {
            try
            {
                await _contractingService.GetContractingProcessByTaskId(id);
                return RedirectToAction("CustomError", "Home", new { title = "This task has already been published!", description = "" });
            } catch (Exception ex)
            {
                Models.Project.DAL.Task task = await _projectService.GetTask(id);

                DateTime deliveryDate = (DateTime) ((task.EndDate != null) ? task.EndDate : DateTime.Now);
                DateTime couterProposalEndDate = (DateTime) ((task.StartDate != null) ? task.StartDate : DateTime.Now);


                ViewData["TaskId"] = id;
                return View("Publishtask", new ProposalViewModel()
                {
                    DeliveryDate = deliveryDate,
                    CounterProposalsEndDate = couterProposalEndDate,
                    ReferenceFilesIdsJson = "[]",
                    UploadPartialViewModel = new Views.FileTransfer.UploadPartialViewModel()
                    {
                        AccessPolicy = "public",
                        PreviouslyUploadedFiles = new List<Models.FileTransfer.DAL.File>(),
                        UploadedFilesJson = "[]"
                    }
                });
            }
        }


        [HttpPost]
        [Authorize]
        public async Task<IActionResult> PublishTask([Bind("TaskId,Description,Amount, ReferenceFilesIdsJson, DeliveryDate, CounterProposalsEndDate")] ProposalViewModel proposal)
        {
            if (proposal == null)
            {
                return RedirectToAction("CustomError", "Home", new { title = "Operation failed", description = "There was a problem publishing this task. Please try again." });
            }

            TaskLancerUser user = await GetCurrentUser();
            //string formatedAmount = proposal.Amount.Replace(".", ","); //comparar com o formulário do submit e editar uma contra-proposta
            //float amount = (float)(Math.Truncate(100 * Decimal.Parse(formatedAmount, System.Globalization.NumberStyles.Currency)) / 100);

            float amount = (float)(Math.Truncate(100 * Decimal.Parse(proposal.Amount, System.Globalization.NumberStyles.Currency)) / 100);

            if(amount < 0.01)
            {
                return RedirectToAction("CustomError", "Home", new { title = "Invalid operation", description = "The amount must be positive" });
            }

            if (proposal.DeliveryDate < DateTime.Now || proposal.CounterProposalsEndDate < DateTime.Now)
            {
                return RedirectToAction("CustomError", "Home", new { title = "Invalid operation", description = "Dates can't be in the past" });
            }

            if(proposal.DeliveryDate < proposal.CounterProposalsEndDate)
            {
                return RedirectToAction("CustomError", "Home", new { title = "Invalid operation", description = "Delivery date must be after counter-proposals end date." });
            }

            //float amount = AmountStringToFloat(proposal.Amount);

            List<string>? referenceFilesIds = JsonSerializer.Deserialize<List<string>>(proposal.ReferenceFilesIdsJson!);

            try
            {
                await _contractingService.PublishTask(user.Id, proposal.TaskId, proposal.Description, amount, referenceFilesIds!, proposal.DeliveryDate, proposal.CounterProposalsEndDate);
            } catch (Exception ex)
            {
                return RedirectToAction("CustomError", "Home", new { title = "Invalid operation", description = ex.Message });
            }

            return RedirectToAction("ByTask", new { id = proposal.TaskId });
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> SubmitCounterProposal([Bind("ContractingProcessId,DeliveryDate,Amount,SelfIntro")]CounterProposalViewModel counterProposal)
        {
            if (counterProposal == null)
            {
                return RedirectToAction("CustomError", "Home", new { title = "Operation failed", description = "There was a problem submiting your counter-proposal. Please try again." });
            }

            int taskId;

            TaskLancerUser user = await GetCurrentUser();
            string formatedAmount = counterProposal.Amount.Replace(".", ",");
            //float amount = (float)(Math.Truncate(100 * Decimal.Parse(formatedAmount, System.Globalization.NumberStyles.Currency)) / 100);

            float amount = (float)(Math.Truncate(100 * Decimal.Parse(counterProposal.Amount, System.Globalization.NumberStyles.Currency)) / 100);

            if (amount < 0.01)
            {
                return RedirectToAction("CustomError", "Home", new { title = "Invalid operation", description = "The amount must be positive" });
            }

            if(counterProposal.DeliveryDate < DateTime.Now)
            {
                return RedirectToAction("CustomError", "Home", new { title = "Invalid operation", description = "Delivery Date can't be in the past" });
            }

            //float amount = AmountStringToFloat(counterProposal.Amount);
            try
            {
                await _contractingService.AddCounterProposal(counterProposal.ContractingProcessId, user.Id, counterProposal.SelfIntro, amount, counterProposal.DeliveryDate);
                taskId = await _contractingService.GetTaskId(counterProposal.ContractingProcessId);
            } catch (Exception ex)
            {
                return RedirectToAction("CustomError", "Home", new { title = "Invalid operation", description = ex.Message });
            }


            return RedirectToAction("ByTask", new { id = taskId });
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> EditCounterProposal([Bind("ContractingProcessId,CounterProposalId,DeliveryDate,Amount,SelfIntro")] CounterProposalViewModel counterProposal)
        {
            if (counterProposal == null)
            {
                return BadRequest();
            }

            if (counterProposal.DeliveryDate < DateTime.Now)
            {
                return RedirectToAction("CustomError", "Home", new { title = "Invalid operation", description = "Delivery Date can't be in the past" });
            }

            int taskId;
            TaskLancerUser user = await GetCurrentUser();
            float amount = (float)(Math.Truncate(100 * Decimal.Parse(counterProposal.Amount, System.Globalization.NumberStyles.Currency)) / 100);

            if (amount < 0.01)
            {
                return RedirectToAction("CustomError", "Home", new { title = "Invalid operation", description = "The amount must be positive" });
            }

            try
            {
                await _contractingService.EditCounterProposal(counterProposal.ContractingProcessId, counterProposal.CounterProposalId, user.Id, counterProposal.SelfIntro, amount, counterProposal.DeliveryDate);
                taskId = await _contractingService.GetTaskId(counterProposal.ContractingProcessId);
            }
            catch (Exception ex)
            {
                return RedirectToAction("CustomError", "Home", new { title = "Invalid operation", description = ex.Message });
            }

            return RedirectToAction("ByTask", new { id = taskId });
        }

        [HttpGet("/Contracting/{contractingProcessId}/AcceptCounterProposal/{counterProposalId}")]
        [Authorize]
        public async Task<IActionResult> AcceptCounterProposal(int counterProposalId, int contractingProcessId)
        {
            try
            {
                string userId = GetCurrentUser().Result.Id;

                int taskId = await _contractingService.GetTaskId(contractingProcessId);

                await _contractingService.AcceptCounterProposal(userId, contractingProcessId, counterProposalId!);

                return RedirectToAction("ByTask", new { id = taskId });

            }
            catch (Exception ex)
            {
                return RedirectToAction("CustomError", "Home", new { title = "Invalid operation", description = ex.Message });
            }


        }

        [HttpGet("/Contracting/{contractingProcessId}/DeclineCounterProposal/{counterProposalId}")]
        [Authorize]
        public async Task<IActionResult> DeclineCounterProposal(int counterProposalId, int contractingProcessId)
        {
            int taskId;
            try
            {
                string userId = GetCurrentUser().Result.Id;

                taskId = await _contractingService.GetTaskId(contractingProcessId);

                await _contractingService.RemoveCounterProposal(userId, contractingProcessId, counterProposalId);
            }
            catch (Exception ex)
            {
                return RedirectToAction("CustomError", "Home", new { title = "Invalid operation", description = ex.Message });
            }

            return RedirectToAction("ByTask", new { id = taskId });
        }

        [Authorize]
        public async Task<IActionResult> SubmitDeliver(DeliveryViewModel delivery)
        {
            
            try
            {
                if(delivery != null)
                {
                    TaskLancerUser user = await GetCurrentUser();

                    List<string>? sampleFilesIds = JsonSerializer.Deserialize<List<string>>(delivery.SampleFileIdJson!);
                    List<string>? finalFilesIds = JsonSerializer.Deserialize<List<string>>(delivery.FinalFileIdJson!);


                    await _contractingService.AddDelivery(delivery.ContractingProcessId, user.Id, delivery.FinalComment, sampleFilesIds!, finalFilesIds!); 
                }

                return RedirectToAction("Details", new { id = delivery.ContractingProcessId });
            }
            catch (Exception ex)
            {
                return RedirectToAction("CustomError", "Home", new { title = "Invalid operation", description = ex.Message });
            }
        }

        [HttpGet("/Contracting/{contractingProcessId}/DeclineDelivery/{deliveryId}")]
        [Authorize]
        public async Task<IActionResult> DeclineDelivery(int contractingProcessId, int deliveryId)
        {
            try
            {
                string userId = GetCurrentUser().Result.Id;
                await _contractingService.SetDeliveryApprovalState(userId, contractingProcessId, deliveryId, DeliveryApprovalState.Declined);
            }
            catch (Exception ex)
            {
                return RedirectToAction("CustomError", "Home", new { title = "Invalid operation", description = ex.Message });
            }

            return RedirectToAction("Details", new { id = contractingProcessId });
        }

        [HttpGet("/Contracting/{contractingProcessId}/AcceptDelivery/{deliveryId}")]
        [Authorize]
        public async Task<IActionResult> AcceptDelivery(int contractingProcessId, int deliveryId)
        {
            try
            {
                string userId = GetCurrentUser().Result.Id;
                await _contractingService.SetDeliveryApprovalState(userId, contractingProcessId, deliveryId, DeliveryApprovalState.Approved);
            }
            catch (Exception ex)
            {
                return RedirectToAction("CustomError", "Home", new { title = "Invalid operation", description = ex.Message });
            }

            return RedirectToAction("Details", new { id = contractingProcessId });
        }

        private float AmountStringToFloat(string amount)
        {
            double temp = Math.Floor(float.Parse(amount) * 100);

            return (float)temp / 100.0f;
        }

        private async Task<TaskLancerUser> GetCurrentUser()
        {
            System.Security.Claims.ClaimsPrincipal currentUser = this.User;
            return await _userManager.GetUserAsync(currentUser);
        }

        private async Task<ContractingProcessViewModel> GenerateContractingProcessViewModelAsync(ContractingProcess cp)
        {
            CounterProposalViewModel acceptedCounterProposalViewModel = null;
            if (cp.AcceptedCounterProposal != null)
            {
                acceptedCounterProposalViewModel = new CounterProposalViewModel()
                {
                    CounterProposalId = cp.AcceptedCounterProposal.Id,
                    FreelancerId = cp.AcceptedCounterProposal.Freelancer.Id,
                    FreelancerFirstName = cp.AcceptedCounterProposal.Freelancer.FirstName,
                    FreelancerSecondName = cp.AcceptedCounterProposal.Freelancer.LastName,
                    FreelancerUsername = cp.AcceptedCounterProposal.Freelancer.UserName,
                    SelfIntro = cp.AcceptedCounterProposal.SelfIntro,
                    DeliveryDate = cp.AcceptedCounterProposal.DeliveryDate,
                    Amount = cp.AcceptedCounterProposal.Amount.ToString()
                };
            }

            List<CounterProposalViewModel> cpsvm = new List<CounterProposalViewModel>();
            if (cp.CounterProposals != null)
            {

                foreach (CounterProposal couterProposal in cp.CounterProposals)
                {
                    CounterProposalViewModel counterProposalViewModel = new CounterProposalViewModel()
                    {
                        ContractingProcessId = cp.Id,
                        CounterProposalId = couterProposal.Id,
                        FreelancerId = couterProposal.Freelancer.Id,
                        FreelancerFirstName = couterProposal.Freelancer.FirstName,
                        FreelancerSecondName = couterProposal.Freelancer.LastName,
                        FreelancerUsername = couterProposal.Freelancer.UserName,
                        SelfIntro = couterProposal.SelfIntro,
                        DeliveryDate = couterProposal.DeliveryDate,
                        Amount = couterProposal.Amount.ToString(),
                        LimitDate = cp.CounterProposalsEndDate,
                    };

                    cpsvm.Add(counterProposalViewModel);
                }
            }

            DeliveryViewModel deliveryViewModel = null;
            Delivery delivery = cp.Deliveries.FirstOrDefault(d => d.ApprovalState == DeliveryApprovalState.Approved || d.ApprovalState == DeliveryApprovalState.Pending);
            if (delivery != null)
            {
                deliveryViewModel = new DeliveryViewModel()
                {
                    DeliveryId = delivery.Id,
                    ContractingProcessId = cp.Id,
                    FinalComment = delivery.FinalComment,
                    FinalFileIdJson = JsonSerializer.Serialize(delivery.FinalFiles.Select(f => f.FileId)),
                    SampleFileIdJson = JsonSerializer.Serialize(delivery.SampleFiles.Select(f => f.FileId)),
                    FinalFiles = delivery.FinalFiles,
                    SampleFiles = delivery.SampleFiles,
                    ApprovalState = delivery.ApprovalState,
                    LimitDate = cp.AcceptedCounterProposal.DeliveryDate

                };
            }
            

            var payment = _context.Payment.FirstOrDefault(p => p.PaymentId == cp.PaymentId);

            ContractingPaymentViewModel pvm = null;
            if (cp.Payment != null)
            {
                pvm = new ContractingPaymentViewModel()
                {
                    PaymentId = payment.PaymentId,
                    ContractingProcessId = cp.Id,
                    ServiceFee =payment.ServiceFee,
                    SubTotal = payment.SubTotal,
                    Total = payment.Total,
                    Currency = payment.Currency,
                    State = payment.State,
                    LimitDate = payment.LimitDate,
                    Title = payment.Title,
                };
            }
            TaskLancerUser? user = await _userManager.GetUserAsync(this.User);
            Review reviewSubmited = null;
            ReviewViewModel reviewSubmitedModel = null;

            try
            {
                reviewSubmited = await _ratingService.GetReview(cp.Id, user.Id);
            }
            catch (Exception ex){}         
            
            if(reviewSubmited != null)
            {
                reviewSubmitedModel = new ReviewViewModel()
                {
                    Id = reviewSubmited.Id,
                    AuthorId = reviewSubmited.AuthorId,
                    AuthorUsername = reviewSubmited.Author.UserName,
                    ManagerId = reviewSubmited.ManagerId,
                    FreelancerId = reviewSubmited.FreelancerId,
                    SubjectId = reviewSubmited.SubjectId,
                    SubjectUsername = reviewSubmited.Subject.UserName,
                    Score = reviewSubmited.Score.ToString(),
                    Comment = reviewSubmited.Comment,
                    ContractingProcessId = reviewSubmited.ContractingProcessId,
                    State = reviewSubmited.State
                };
            }

            ReviewViewModel reviewReceivedModel = null;
            Review reviewReceived = null;
            try
            {
                reviewReceived = await _ratingService.GetReviewReceived(cp.Id, user.Id);
            }
            catch (Exception ex) { }
            
            if (reviewReceived != null)
            {
                reviewReceivedModel = new ReviewViewModel()
                {
                    Id = reviewReceived.Id,
                    AuthorId = reviewReceived.AuthorId,
                    AuthorUsername = reviewReceived.Author.UserName,
                    ManagerId = reviewReceived.ManagerId,
                    FreelancerId = reviewReceived.FreelancerId,
                    SubjectId = reviewReceived.SubjectId,
                    SubjectUsername = reviewReceived.Subject.UserName,
                    Score = reviewReceived.Score.ToString(),
                    Comment = reviewReceived.Comment,
                    ContractingProcessId = reviewReceived.ContractingProcessId,
                    State = reviewReceived.State
                };
            }
            

            ContractingProcessViewModel cpvm = new ContractingProcessViewModel()
            {
                ContractingProcessId = cp.Id,

                TaskId = cp.Task.TaskId,
                TaskTitle = cp.Task.Title,
                TaskDescription = cp.Task.Description,
                TaskEndDate = (DateTime)cp.Task.EndDate,
                TaskStartDate = cp.Task.StartDate != null ? (DateTime)cp.Task.StartDate : cp.CounterProposalsEndDate,
                TaskSkillTags = cp.Task.SkillTags.Select(s => s.Name).ToList(),

                ProposalId = cp.Proposal.Id,
                ProposalDescription = cp.Proposal.Description,
                ProposalAmount = cp.Proposal.Amount,
                ProposalDeliveryDate = cp.Proposal.DeliveryDate,
                ReferenceFiles = cp.Proposal.ReferenceFiles,

                ManagerId = cp.Proposal.Manager.Id,
                ManagerFirstName = cp.Proposal.Manager.FirstName,
                ManagerLastName = cp.Proposal.Manager.LastName,
                ManagerUsername = cp.Proposal.Manager.UserName,

                PaymentViewModel = pvm,
                CouterProposalsViewModel = cpsvm,
                AcceptedCounterProposal = acceptedCounterProposalViewModel,
                State = cp.State,
                PublishingDate = cp.PublishingDate,
                CounterProposalsEndDate = cp.CounterProposalsEndDate,
                DeliveryViewModel = deliveryViewModel,
                ReviewSubmitedViewModel = reviewSubmitedModel,
                ReviewReceivedViewModel = reviewReceivedModel
            };

            return cpvm;
        }
    }
}
