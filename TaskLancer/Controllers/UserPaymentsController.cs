﻿using System.Diagnostics;
using System.Text.Json;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PayPalCheckoutSdk.Orders;
using TaskLancer.Areas.Identity.Data;
using TaskLancer.Data;
using TaskLancer.Models;
using TaskLancer.Models.Contracting.BLL;
using TaskLancer.Models.Contracting.DAL;
using TaskLancer.Models.Payments.BLL;
using TaskLancer.Models.Payments.DAL;
using TaskLancer.Utils;

namespace TaskLancer.Controllers
{
    public class UserPaymentsController : Controller
    {
        private readonly TaskLancerContext _context;
        private readonly UserManager<TaskLancerUser> _userManager;
        private readonly IPaymentService _paymentService;
        private readonly ILogger<HomeController> _logger;
        private readonly IContractingService _contractingService;

        public UserPaymentsController(TaskLancerContext context, UserManager<TaskLancerUser> userManager, IPaymentService paymentService, IContractingService contractingService, ILogger<HomeController> logger)
        {
            _context = context;
            _userManager = userManager;
            _paymentService = paymentService;
            _logger = logger;
            _contractingService = contractingService;
        }

        // GET: UserPayments
        public async Task<IActionResult> Index(PaymentViewModel? paymentViewModel)
        {
            System.Security.Claims.ClaimsPrincipal currentUser = this.User;

            TaskLancerUser user = await _userManager.GetUserAsync(currentUser);

            List<Payment> allUserPayments = await _paymentService.GetPayments(user);

            allUserPayments.Reverse();

            if(paymentViewModel == null)
            {
                paymentViewModel = new PaymentViewModel()
                {
                    ShowStatusMessage = false,
                };
            }

            paymentViewModel.Payments = allUserPayments;        

            return View(paymentViewModel);
        }

        [Authorize]
        [HttpGet("/Payment/{paymentId}/RelatedContractingProcess")]
        public async Task<IActionResult> RelatedContractingProcess(int paymentId)
        {
            ContractingProcess contractingProcess = await _contractingService.GetContractingProcessByPaymentId(paymentId);

            if(contractingProcess != null)
            {
                return RedirectToAction("Details", "Contracting", new {id = contractingProcess.Id});
            }

            return BadRequest("No contracting process related to payment.");
        }

        [Authorize]
        public async Task<IActionResult> RefundedPayments()
        {
            System.Security.Claims.ClaimsPrincipal currentUser = this.User;

            TaskLancerUser user = await _userManager.GetUserAsync(currentUser);

            ViewBag.hasEdit = User.Identity.Name == user.UserName;

            List<Payment> paidPayments = await _paymentService.GetRefundedPayments(user);

            paidPayments.Reverse();

            return View(paidPayments);
        }

        [Authorize]
        public async Task<IActionResult> CanceledPayments()
        {
            System.Security.Claims.ClaimsPrincipal currentUser = this.User;

            TaskLancerUser user = await _userManager.GetUserAsync(currentUser);

            ViewBag.hasEdit = User.Identity.Name == user.UserName;

            List<Payment> paidPayments = await _paymentService.GetCanceledPayments(user);

            paidPayments.Reverse();

            return View(paidPayments);
        }

        [Authorize]
        public async Task<IActionResult> PaidPayments()
        {
            System.Security.Claims.ClaimsPrincipal currentUser = this.User;

            TaskLancerUser user = await _userManager.GetUserAsync(currentUser);

            ViewBag.hasEdit = User.Identity.Name == user.UserName;

            List<Payment> paidPayments = await _paymentService.GetPaidPayments(user);

            paidPayments.Reverse();

            return View(paidPayments);
        }

        [Authorize]
        public async Task<IActionResult> UnpaidPayments()
        {
            System.Security.Claims.ClaimsPrincipal currentUser = this.User;

            TaskLancerUser user = await _userManager.GetUserAsync(currentUser);

            ViewBag.hasEdit = User.Identity.Name == user.UserName;

            List<Payment> paidPayments = await _paymentService.GetUnpaidPayments(user);

            paidPayments.Reverse();

            return View(paidPayments);
        }

        [Authorize]
        public async Task<IActionResult> Payouts(PaymentViewModel? paymentViewModel)
        {
            System.Security.Claims.ClaimsPrincipal currentUser = this.User;

            TaskLancerUser user = await _userManager.GetUserAsync(currentUser);

            ViewBag.hasEdit = User.Identity.Name == user.UserName;

            if (paymentViewModel == null)
            {
                paymentViewModel = new PaymentViewModel()
                {
                    ShowStatusMessage = false,
                };
            }

            List<Payment> paidPayments = await _paymentService.GetPayouts(user);

            paymentViewModel.Payments = paidPayments;

            paymentViewModel.Payments.Reverse();

            return View(paymentViewModel);
        }
 

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }


        [HttpPost("/Payment/{paymentId}/CreateOrder")]
        public async Task<IActionResult> CreateOrder(int paymentId, string? redirectRoute = "/UserPayments/")
        {
            Order result = await _paymentService.CreateOrder(paymentId, redirectRoute);
            
            return Content(JsonSerializer.Serialize(result, new JsonSerializerOptions() { IncludeFields = true, PropertyNamingPolicy = new SnakeCaseNamingPolicy() }), "application/json");
        }



        [HttpPost("/Payment/{paymentId}/Capture/{orderId}")]
        public async Task<IActionResult> Capture(int paymentId, string orderId)
        {
            Order result = await _paymentService.CapturePayment(paymentId, orderId);

            if(result.Status == "COMPLETED")
            {
                await _contractingService.PaymentWasSuccessful(paymentId);
            }

            return Content(JsonSerializer.Serialize(result, new JsonSerializerOptions() { IncludeFields = true, PropertyNamingPolicy = new SnakeCaseNamingPolicy() }), "application/json");

        }

        [HttpGet("/Payment/{paymentId}/RecievePayout")]
        [Authorize]
        public async Task<IActionResult> RecievePayout(int paymentId, string? redirectUrl)
        {
            TaskLancerUser? user = await _userManager.GetUserAsync(this.User);

            await _paymentService.ExecutePayout(user.Id, paymentId);

            return Redirect(redirectUrl!);
        }

        [HttpGet("/Payment/{paymentId}/PayoutPartial")]
        [Authorize]
        public async Task<PartialViewResult> PayoutPartial(int paymentId, string? redirectUrl)
        {
            TaskLancerUser? user = await _userManager.GetUserAsync(this.User);
            Payment payment = await _paymentService.GetPayment(paymentId);

            ViewBag.PaypalEmail = _paymentService.GetPayPalEmail(user);

            ViewBag.PaymentId = paymentId;
            ViewBag.Amount = payment.SubTotal;
            ViewBag.State = payment.State.ToString();

            ViewBag.RedirectUrl = redirectUrl;

            if (payment.FreeLancer == user)
            {
                ViewData["Role"] = "freelancer";
            } else if (payment.ProjectManager == user)
            {
                ViewData["Role"] = "manager";
            }

            return PartialView("PayoutPartial");
        }



    }

    public class PaymentViewModel
    {
        public List<Payment>? Payments { get; set; }
        public bool ShowStatusMessage { get; set; }
        public string? StatusMessageTitle { get; set; }
        public string? StatusMessageDescription { get; set; }
        public bool Successful { get; set; }
    }
}
