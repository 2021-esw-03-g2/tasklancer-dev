﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TaskLancer.Areas.Identity.Data;
using TaskLancer.Data;
using TaskLancer.Models.Rating.BLL;
using TaskLancer.Models.Rating.DAL;

namespace TaskLancer.Controllers
{
    public class RatingController : Controller
    {
        private readonly TaskLancerContext _context;
        private readonly UserManager<TaskLancerUser> _userManager;
        private readonly IRatingService _ratingService;
        private readonly ILogger<ContractingController> _logger;

        public RatingController(TaskLancerContext context, UserManager<TaskLancerUser> userManager, IRatingService ratingService,ILogger<ContractingController> logger)
        {
            _context = context;
            _userManager = userManager;
            _ratingService = ratingService;
            _logger = logger;
        }

        [Authorize]
        public async Task<ActionResult> Index(string username)
        {
            try
            {
                TaskLancerUser? user = await GetUserByUsername(username);
                TaskLancerUser? authenticatedUser = await _userManager.GetUserAsync(this.User);

                ViewBag.ManagerRating = Math.Round(await _ratingService.GetManagerRating(user.Id), 1);
                ViewBag.FreelancerRating = Math.Round(await _ratingService.GetFreelancerRating(user.Id), 1);
                ViewBag.Username = user.UserName;
                ViewBag.AuthenticatedUserUsername = authenticatedUser.UserName;

                return View("RatingAndReviews");
            }
            catch(Exception ex)
            {
                return RedirectToAction("CustomError", "Home", new { title = "Invalid operation", description = "This username doens't exist" });
            }    
        }

        [HttpGet("/Rating/ReceivedReviewsPartial/{username}")]
        [Authorize]
        public async Task<PartialViewResult> ReceivedReviewsPartial(string username)
        {
            TaskLancerUser? user = await GetUserByUsername(username);

            List<Review> reviewsReceivedAsManager = await _ratingService.GetReviewsReceivedAsManager(user.Id);
            List<ReviewViewModel> reviewsReceivedAsManagerViewModel = new List<ReviewViewModel>();
            foreach (Review review in reviewsReceivedAsManager)
            {
                reviewsReceivedAsManagerViewModel.Add(GenerateReviewViewModel(review));
            }

            List<Review> reviewsReceivedAsFreelancer = await _ratingService.GetReviewsReceivedAsFreelancer(user.Id);
            List<ReviewViewModel> reviewsReceivedAsFreelancerViewModel = new List<ReviewViewModel>();
            foreach(Review review in reviewsReceivedAsFreelancer)
            {
                reviewsReceivedAsFreelancerViewModel.Add(GenerateReviewViewModel(review));
            }

            ListOfReviewsViewModel listOfReviewsViewModel = new ListOfReviewsViewModel()
            {
                ReviewsAsManager = reviewsReceivedAsManagerViewModel,
                ReviewsAsFreelancer = reviewsReceivedAsFreelancerViewModel,
                RadioButtonGroupName = "received_role"
            };

            return PartialView("ListOfReviewsPartial", listOfReviewsViewModel);
        }

        [HttpGet("/Rating/SubmittedReviewsPartial/{username}")]
        [Authorize]
        public async Task<PartialViewResult> SubmittedReviewsPartial(string username)
        {
            TaskLancerUser? user = await GetUserByUsername(username);

            List<Review> reviewsSubmittedAsManager = await _ratingService.GetReviewsSubmittedAsManager(user.Id);
            List<ReviewViewModel> reviewsSubmittedAsManagerViewModel = new List<ReviewViewModel>();
            foreach (Review review in reviewsSubmittedAsManager)
            {
                reviewsSubmittedAsManagerViewModel.Add(GenerateReviewViewModel(review));
            }

            List<Review> reviewsSubmittedAsFreelancer = await _ratingService.GetReviewsSubmittedAsFreelancer(user.Id);
            List<ReviewViewModel> reviewsSubmittedAsFreelancerViewModel = new List<ReviewViewModel>();
            foreach (Review review in reviewsSubmittedAsFreelancer)
            {
                reviewsSubmittedAsFreelancerViewModel.Add(GenerateReviewViewModel(review));
            }

            ListOfReviewsViewModel listOfReviewsViewModel = new ListOfReviewsViewModel()
            {
                ReviewsAsManager = reviewsSubmittedAsManagerViewModel,
                ReviewsAsFreelancer = reviewsSubmittedAsFreelancerViewModel,
                RadioButtonGroupName = "submitted_role"
            };

            return PartialView("ListOfReviewsPartial", listOfReviewsViewModel);
        }

        [HttpGet("/Rating/PendingReviewsPartial")]
        [Authorize]
        public async Task<PartialViewResult> PendingReviewsPartial()
        {
            TaskLancerUser? user = await _userManager.GetUserAsync(this.User);

            List<Review> pendingReviews = await _ratingService.GetUserPendingReviews(user.Id);
            List<ContractingProcessWithPendingReviewViewModel> listOfContractingProcess= new List<ContractingProcessWithPendingReviewViewModel>();
            foreach (Review review in pendingReviews)
            {
                listOfContractingProcess.Add(GenerateContractingProcessWithPendingReviewViewModel(review));
            }

            return PartialView("PendingReviewsPartial", listOfContractingProcess);
        }

        [HttpGet("/Contracting/{contractingProcessId}/SubmitReviewPartial")]
        [Authorize]
        public async Task<PartialViewResult> SubmitReviewPartial(int contractingProcessId, string? redirectUrl)
        {
            TaskLancerUser? user = await _userManager.GetUserAsync(this.User);
            Review review = await _ratingService.GetReview(contractingProcessId, user.Id);

            ViewBag.ReviewId = review.Id;
            ViewBag.ContractingProcessId = review.ContractingProcessId;
            ViewBag.AuthorId = review.AuthorId;

            ViewBag.RedirectUrl = redirectUrl;

            return PartialView("SubmitReviewPartial");
        }

        [Authorize]
        public async Task<IActionResult> SubmitReview(ReviewViewModel reviewViewModel)
        {
            try
            {
                TaskLancerUser? user = await _userManager.GetUserAsync(this.User);

                //string formatedScore = reviewViewModel.Score.Replace(".", ","); //comparar com o formulário do submit e editar uma contra-proposta
                double score = Convert.ToDouble(reviewViewModel.Score);
                //double score = (double)(Math.Truncate(100 * Decimal.Parse(formatedScore, System.Globalization.NumberStyles.Currency)) / 100);

                await _ratingService.SubmitReview(reviewViewModel.ContractingProcessId, user.Id, reviewViewModel.Comment, score);

                return RedirectToAction("Details","Contracting", new { id = reviewViewModel.ContractingProcessId});
            }catch(Exception ex)
            {
                return RedirectToAction("CustomError", "Home", new { title = "Invalid operation", description = ex.Message });
            }
            
        }

        [HttpGet("/ReviewDetailsPartial/{reviewId}")]
        [Authorize]
        public async Task<PartialViewResult> ReviewDetailsPartial(int reviewId)
        {
            TaskLancerUser? user = await _userManager.GetUserAsync(this.User);
            Review review = await _ratingService.GetReview(reviewId);
            bool isAuthor = false;
            if (review.AuthorId == user.Id)
            {
                isAuthor = true;
            }
           
            ReviewViewModel reviewViewModel = new ReviewViewModel()
            {
                Id = review.Id,
                Score = review.Score.ToString(),
                Comment = review.Comment,
                IsAuthor = isAuthor,
            };

  
            return PartialView("ReviewDetailsPartial", reviewViewModel);
        }

        private ReviewViewModel GenerateReviewViewModel(Review review)
        {
            ReviewViewModel reviewViewModel = null;
            if (review != null)
            {
                reviewViewModel = new ReviewViewModel()
                {
                    Id = review.Id,
                    AuthorUsername = review.Author.UserName,
                    SubjectUsername = review.Subject.UserName,
                    Comment = review.Comment,
                    Score = review.Score.ToString(),
                    SubmittedOn = review.SubmittedOn,
                    ContractingProcessId = review.ContractingProcessId
                };
            }

            return reviewViewModel;
        }

        private ContractingProcessWithPendingReviewViewModel GenerateContractingProcessWithPendingReviewViewModel(Review review) {
            ContractingProcessWithPendingReviewViewModel viewModel = null;

            if(review != null)
            {
                viewModel = new ContractingProcessWithPendingReviewViewModel()
                {
                    ContractingProcessId = review.ContractingProcessId,
                    TaskTitle = review.ContractingProcess.Task.Title,
                    TaskDescription = review.ContractingProcess.Task.Description,
                    ManagerUsername = review.Manager.UserName,
                };
            }

            return viewModel;
        }

        private async Task<TaskLancerUser> GetUserByUsername(string username)
        {
            return await _context.Users.FirstOrDefaultAsync(u => u.UserName == username);
        }
    }


}
