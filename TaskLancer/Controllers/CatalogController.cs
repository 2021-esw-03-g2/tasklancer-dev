﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.Web.Http;
using TaskLancer.Data;
using TaskLancer.Models;
using TaskLancer.Models.Catalog.BLL;
using TaskLancer.Models.Contracting.BLL;
using TaskLancer.Models.Contracting.DAL;

namespace TaskLancer.Controllers
{
    public class CatalogController : Controller
    {
        private readonly TaskLancerContext _context;
        private readonly ICatalogService _catalogService;

        public CatalogController(TaskLancerContext context, ICatalogService catalogService)
        {
            _context = context;
            _catalogService = catalogService;
        }

        [Authorize]
        public async Task<IActionResult> Index(InputModel? inputModel)
        {

            //List<ContractingProcess> contractsList = _contractingService.GetAllPublishedContractingProcesses();

            List<SkillTag> allSkillTags = await _context.SkillTag.ToListAsync();

            IEnumerable<SelectListItem> SkillTagsSelectList = new SelectList(allSkillTags, "SkillTagId", "Name");

            ViewBag.SkillTagsSelectList = SkillTagsSelectList;

            var selectedValues = new List<int>();

            if (inputModel.FilterDate == null)
            {
                inputModel.FilterDate = "";
            }

            /*foreach (var contract in contractList)
            {
                contract.SkillTags = allSkillTags;
            }*/

            if (inputModel.SelectedSkillTags != null)
            {
                foreach (var tag in inputModel.SelectedSkillTags)
                {
                    selectedValues.Add(tag);
                }
            }

            List<ContractingProcess> contractsList = _catalogService.GetFilteredContracts(selectedValues, inputModel.FilterDate);

            /*foreach(var skillTag in selectedSkillTags)
            {
                foreach(var contract in contractList)
                {
                    selectedSkillTags.Exists(x => x.SkillTagId == contract.SkillTags);
                }
            }*/

            InputModel input = new InputModel
            {
                PublishedContracts = contractsList,
                FilterDate = inputModel.FilterDate,
                SelectedSkillTags = selectedValues,
            };

            return View(input);
        }



        public class InputModel
        {
            public List<ContractingProcess>? PublishedContracts { get; set; }

            [Display(Name = "Filter Date")]
            public string FilterDate { get; set; }

            [Display(Name = "Skill Tags")]
            public IEnumerable<int>? SelectedSkillTags { get; set; }

        }
    }
}
