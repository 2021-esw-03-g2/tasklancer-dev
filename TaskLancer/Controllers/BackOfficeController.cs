﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.Web.Http;
using TaskLancer.Areas.Identity.Data;
using TaskLancer.Data;
using TaskLancer.Models.BackOffice.BLL;
using TaskLancer.Models.Contracting.DAL;
using TaskLancer.Services;
using HttpPostAttribute = Microsoft.AspNetCore.Mvc.HttpPostAttribute;

namespace TaskLancer.Controllers
{
    public class BackOfficeController : Controller
    {
        private readonly TaskLancerContext _context;
        private readonly IBackOfficeService _backofficeService;
        private readonly UserManager<TaskLancerUser> _userManager;
        private readonly IStatisticsService _statisticsService;

        public BackOfficeController(TaskLancerContext context, IBackOfficeService backofficeService, UserManager<TaskLancerUser> userManager, IStatisticsService statisticsService)
        {
            _context = context;
            _backofficeService = backofficeService;
            _userManager = userManager;
            _statisticsService = statisticsService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ManageUsers()
        {
            List<InputModel> model = new List<InputModel>();
            List<TaskLancerUser> users = _context.Users.ToList();
            foreach (var user in users)
            {
                InputModel input = new InputModel
                {
                    Id = user.Id,
                    UserName = user.UserName,
                    UserRole = _backofficeService.getUserRole(user.Id, _userManager),
                };
                model.Add(input);
            }
            return View(model);
        }

        [Authorize(Roles = "administrator")]
        public async Task<IActionResult> Edit(string? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            TaskLancerUser user = await _userManager.FindByIdAsync(id);
            if (user == null)
            {
                return NotFound();
            }
            InputModel input = new InputModel
            {
                Id = user.Id,
                UserName = user.UserName,
                UserRole = _backofficeService.getUserRole(user.Id, _userManager),
            };
            ViewBag.Message = "";
            return View(input);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "administrator")]
        public async Task<IActionResult> Edit(string id, InputModel input)
        {
            if (id != input.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await _backofficeService.ChangeUserRole(input.Id, input.UserRole, _userManager);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RoleExists(input.UserRole))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(ManageUsers));
            }
            return View(input);
        }

        public async Task<IActionResult> Statistics()
        {
            System.Security.Claims.ClaimsPrincipal currentUser = this.User;

            TaskLancerUser user = await _userManager.GetUserAsync(currentUser);
            List<int> avg = await _statisticsService.AverageTimeContractsCompleted();
            StatisticsModel model = new StatisticsModel
            {
                totalUsers = await _statisticsService.UserTotal(),
                totalManagers = await _statisticsService.ParticipatingManagerTotal(),
                totalFreelancers = await _statisticsService.ParticipatingFreelancerTotal(),
                totalProjects = await _statisticsService.ProjectsTotal(),
                totalContracts = await _statisticsService.ContractingProcessesTotal(null),
                totalContractsPublished = await _statisticsService.ContractingProcessesTotal(ContractingProcessState.Published),
                totalContractsPendingPayment = await _statisticsService.ContractingProcessesTotal(ContractingProcessState.Pending_Payment),
                totalContractsOngoing = await _statisticsService.ContractingProcessesTotal(ContractingProcessState.Ordered),
                totalContractsUnderEvaluation = await _statisticsService.ContractingProcessesTotal(ContractingProcessState.Under_Evaluation),
                totalContractsCompleted = await _statisticsService.ContractingProcessesTotal(ContractingProcessState.Completed),
                totalContractsCanceled = await _statisticsService.ContractingProcessesTotal(ContractingProcessState.Canceled),
                totalTransactions = await _statisticsService.PaymentTotal(),
                platformProfit = await _statisticsService.PlatformProfitFinancialTotal(),
                averageTimeContractsCompleted = "" + avg.ElementAt(0) + "d" + avg.ElementAt(1) + "h" + avg.ElementAt(2) + "m",
            };

            return View(model);
        }
        private bool RoleExists(string name)
        {
            return _context.Roles.Any(e => e.Name == name);
        }

        public class InputModel
        {
            [Display(Name = "Id")]
            public string Id { get; set; }

            [Display(Name = "UserName")]
            public string UserName { get; set; }

            [Display(Name = "UserRole")]
            public string UserRole { get; set; }

        }

        public class StatisticsModel
        {
            public int totalUsers { get; set; }

            public int totalManagers { get; set; }

            public int totalFreelancers { get; set; }

            public int totalManagersAndFreelancers { get; set; }

            public int totalProjects { get; set; }

            public int totalContracts { get; set; }

            public int totalContractsPublished { get; set; }

            public int totalContractsPendingPayment { get; set; }

            public int totalContractsOngoing { get; set; }

            public int totalContractsUnderEvaluation { get; set; }

            public int totalContractsCompleted { get; set; }

            public int totalContractsCanceled { get; set; }

            public int totalTransactions { get; set; }

            public float platformProfit { get; set; }
            public String averageTimeContractsCompleted { get; set; }
        }
    }
}
