﻿#nullable disable
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting.Server;
using Microsoft.AspNetCore.Hosting.Server.Features;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TaskLancer.Areas.Identity.Data;
using TaskLancer.Data;
using TaskLancer.Models.Project.BLL;
using TaskLancer.Models.Project.DAL;

namespace TaskLancer.Controllers
{
    public class ProjectsController : Controller
    {
        private readonly TaskLancerContext _context;
        private readonly UserManager<TaskLancerUser> _userManager;
        private readonly IProjectService _projectService;
        private readonly IServer _server;

        public ProjectsController(TaskLancerContext context, UserManager<TaskLancerUser> userManager, IProjectService projectService, IServer server )
        {
            _context = context;
            _userManager = userManager;
            _projectService = projectService;
            _server = server;
        }

        [Authorize]
        public  IActionResult AccessDenied()
        {
            return View();
        }

        //[Authorize(Roles = "administrator")]
        //// GET: Projects
        //public async Task<IActionResult> Index()
        //{
        //    return View(await _context.Project.ToListAsync());
        //}

        [Authorize]
        public async Task<IActionResult> DetailsUserProject()
        {
            System.Security.Claims.ClaimsPrincipal currentUser = this.User;
            TaskLancerUser user = await _userManager.GetUserAsync(currentUser);

            if(user != null)
            {
                var project = await _context.Project.Where(x => x.Manager == user).FirstOrDefaultAsync();
                if(project != null)
                {
                    return RedirectToAction("Details", new { id = project.ProjectId });
                }
            }

            return NotFound();
        }

        [Authorize]
        public async Task<IActionResult> Board(int id)
        {
            System.Security.Claims.ClaimsPrincipal currentUser = this.User;
            TaskLancerUser user = await _userManager.GetUserAsync(currentUser);

            Project project = await _context.Project.Include(p => p.Manager).Include(p => p.Board).FirstOrDefaultAsync(p => p.Board.BoardId == id);

            if(project == null)
            {
                return RedirectToAction("AccessDenied");
            }

            if(!project.IsPublic && project.Manager != user)
            {
                return RedirectToAction("AccessDenied");
            }

            string token = "";

            if (project.Manager == user)
            {
                token = _projectService.generateJwt(user.Id, id);
            }

            ViewBag.Host = _server.Features.Get<IServerAddressesFeature>().Addresses.First();
            ViewBag.ProjectName = project.Name;
            ViewBag.ProjectId = project.ProjectId;
            ViewBag.BoardId = id;
            ViewBag.Token = token;
            return View();
        }

        [Authorize]
        // GET: Projects/Details/5
        public async Task<IActionResult> Details(int? id, string? statusMessage="")
        {

            if (id == null)
            {
                return NotFound();
            }

            //var project = await _context.Project
            //    .FirstOrDefaultAsync(m => m.ProjectId == id);

            System.Security.Claims.ClaimsPrincipal currentUser = this.User;
            TaskLancerUser user = await _userManager.GetUserAsync(currentUser);

            Project project;

            try{
                project = await _projectService.GetProjectDetailsAsync((int)id, user.Id);
            } catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
                return RedirectToAction("AccessDenied");
            }

            ViewBag.BoardId = await _projectService.GetProjectBoardId(project.ProjectId);

            ViewBag.hasEdit = project.Manager.Id == user.Id;
            ViewBag.managerUserName = project.Manager.UserName;

            // Obter todas as tags
            List<CategoryTag> allCategoryTags = await _context.CategoryTag.ToListAsync();
            // criar uma select list que consiste em todas as tags, o atributo que serve de id e o que serve de display
            IEnumerable<SelectListItem> CategoryTagsSelectList = new SelectList(allCategoryTags, "CategoryTagId", "Name");
            // adicionar à view bag
            ViewBag.CategoryTagsSelectList = CategoryTagsSelectList;

            // determinar as tags que ja estão selecionadas
            var selectedValues = new List<int>();
            if (project.CategoryTags != null)
            {
                foreach (var tag in project.CategoryTags)
                {
                    selectedValues.Add(tag.CategoryTagId);
                }
            }

            // construir um input model com a lista de ids de tags em vez de tags
            InputModel input = new InputModel
            {
                ProjectId = project.ProjectId,
                Name = project.Name,
                Description = project.Description,
                IsPublic = project.IsPublic,
                Manager = project.Manager,
                SelectedCategoryTags = selectedValues,
                StatusMessage = statusMessage
            };


            return View(input);
        }

        public class InputModel
        {
            public int ProjectId { get; set; }
            [Required]
            [MinLength(3)]
            [Display(Name = "Project Name")]
            public string Name { get; set; }
            public string? Description { get; set; }

            [Display(Name = "Is Public")]
            public bool IsPublic { get; set; }
            public TaskLancerUser? Manager { get; set; }

            [Display(Name = "Category Tags")]
            public IEnumerable<int> SelectedCategoryTags { get; set; }

            public string? StatusMessage { get; set; }
        }


        // GET: Projects/Create
        //[Authorize(Roles = "administrator")]
        public async Task<IActionResult> Create(string? statusMessage = "")
        {
            List<CategoryTag> allCategoryTags = await _context.CategoryTag.ToListAsync();

            IEnumerable<SelectListItem> CategoryTagsSelectList = new SelectList(allCategoryTags, "CategoryTagId", "Name");

            ViewBag.CategoryTagsSelectList = CategoryTagsSelectList;

            var selectedValues = new List<int>();

            InputModel input = new InputModel
            {
                Name = "",
                Description = "",
                IsPublic = true,
                SelectedCategoryTags = selectedValues,
                StatusMessage = statusMessage
            };


            return View(input);
        }

        // POST: Projects/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(InputModel input)
        {
            if (ModelState.IsValid)
            {
                List<CategoryTag> allCategoryTags = await _context.CategoryTag.ToListAsync();
                ICollection<CategoryTag> categoryTags = new List<CategoryTag>();

                if (input.SelectedCategoryTags != null)
                {
                    foreach (int selectedId in input.SelectedCategoryTags)
                    {
                        CategoryTag categoryTag = allCategoryTags.Where(x => x.CategoryTagId == selectedId).FirstOrDefault();
                        if (categoryTag != null)
                        {
                            categoryTags.Add(categoryTag);
                        }
                    }
                }

                try
                {
                    System.Security.Claims.ClaimsPrincipal currentUser = this.User;
                    TaskLancerUser user = await _userManager.GetUserAsync(currentUser);

                    int projectId = await _projectService.CreateProjectAsync(input.Name,
                        input.Description, input.IsPublic, categoryTags, user.Id);

                    return RedirectToAction(nameof(Details), new { id = projectId, statusMessage = "New project created successfully!" });
                }
                catch (ArgumentException e)
                {
                    Console.WriteLine(e.Message);
                    return RedirectToAction("AccessDenied");
                }                
            }
            return View(input);
        }

        // GET: Projects/Edit/5
        [Authorize]

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            System.Security.Claims.ClaimsPrincipal currentUser = this.User;
            TaskLancerUser user = await _userManager.GetUserAsync(currentUser);

            Project project;

            try
            {
                project = await _projectService.GetProjectDetailsAsync((int)id, user.Id);
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
                return RedirectToAction("AccessDenied");
            }

            if(project.Manager.Id != user.Id)
            {
                return RedirectToAction("AccessDenied");
            }

            List<CategoryTag> allCategoryTags = await _context.CategoryTag.ToListAsync();

            IEnumerable<SelectListItem> CategoryTagsSelectList = new SelectList(allCategoryTags, "CategoryTagId", "Name");

            ViewBag.CategoryTagsSelectList = CategoryTagsSelectList;

            var selectedValues = new List<int>();

            if (project.CategoryTags != null)
            {
                foreach (var tag in project.CategoryTags)
                {
                    selectedValues.Add(tag.CategoryTagId);
                }
            }

            InputModel input = new InputModel
            {
                ProjectId = project.ProjectId,
                Name = project.Name,
                Description = project.Description,
                IsPublic = project.IsPublic,
                Manager = project.Manager,
                SelectedCategoryTags = selectedValues,
            };


            //var project = await _context.Project.FindAsync(id);
            //if (project == null)
            //{
            //    return NotFound();
            //}


            return View(input);
        }

        // POST: Projects/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, InputModel input)
        {
            if (id != input.ProjectId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                List<CategoryTag> allCategoryTags = await _context.CategoryTag.ToListAsync();
                ICollection<CategoryTag> categoryTags = new List<CategoryTag>();

                if (input.SelectedCategoryTags != null)
                {
                    foreach (int selectedId in input.SelectedCategoryTags)
                    {
                        CategoryTag categoryTag = allCategoryTags.Where(x => x.CategoryTagId == selectedId).FirstOrDefault();
                        if (categoryTag != null)
                        {
                            categoryTags.Add(categoryTag);
                        }
                    }
                }

                try
                {
                    System.Security.Claims.ClaimsPrincipal currentUser = this.User;
                    TaskLancerUser user = await _userManager.GetUserAsync(currentUser);

                    await _projectService.SetProjectDetailsAsync(
                        input.ProjectId,
                        input.Name,
                        input.Description,
                        input.IsPublic,
                        categoryTags,
                        user.Id
                        );
                }
                catch (ArgumentException e)
                {
                    Console.WriteLine(e.Message);
                    return RedirectToAction("AccessDenied");
                }


                return RedirectToAction(nameof(Details), new { id = input.ProjectId, statusMessage = "Your project has been updated."});
            }
            return View(input);
        }

        //// GET: Projects/Delete/5
        //[Authorize]
        //[Authorize(Roles = "administrator")]
        //public async Task<IActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var project = await _context.Project
        //        .FirstOrDefaultAsync(m => m.ProjectId == id);
        //    if (project == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(project);
        //}

        //// POST: Projects/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //[Authorize]
        //[Authorize(Roles = "administrator")]
        //public async Task<IActionResult> DeleteConfirmed(int id)
        //{
        //    var project = await _context.Project.FindAsync(id);
        //    _context.Project.Remove(project);
        //    await _context.SaveChangesAsync();
        //    return RedirectToAction(nameof(Index));
        //}

        private bool ProjectExists(int id)
        {
            return _context.Project.Any(e => e.ProjectId == id);
        }
    }
}
