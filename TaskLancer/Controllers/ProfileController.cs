﻿#nullable disable
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TaskLancer.Areas.Identity.Data;
using TaskLancer.Data;
using TaskLancer.Models;
using TaskLancer.Models.Project;

using TaskLancer.Services;

namespace TaskLancer.Controllers
{
    public class ProfileController : Controller
    {
        private readonly TaskLancerContext _context;
        private readonly UserManager<TaskLancerUser> _userManager;
        private readonly IProfileService _profileService;

        public ProfileController(TaskLancerContext context, UserManager<TaskLancerUser> userManager, IProfileService profileService)
        {
            _context = context;
            _userManager = userManager;
            _profileService = profileService;
        }


        [Authorize]
        public async Task<IActionResult> DetailsUserProfile()
        {
            System.Security.Claims.ClaimsPrincipal currentUser = this.User;
            TaskLancerUser user = await _userManager.GetUserAsync(currentUser);

            if(user != null)
            {
                return RedirectToAction("Details", new { username = user.UserName });
            }

            return NotFound();
        }

        [Authorize]
        // GET: Projects/Details/5
        public async Task<IActionResult> Details(string? username)
        {

            if (username == null)
            {
                return NotFound();
            }

            //var project = await _context.Project
            //    .FirstOrDefaultAsync(m => m.ProjectId == id);

            System.Security.Claims.ClaimsPrincipal currentUser = this.User;

            TaskLancerUser user;

            try{
                user = await _profileService.GetUserWithSkillTagsAsync(username);
            } catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
                return NotFound();
            }

            ViewBag.hasEdit = User.Identity.Name == user.UserName;

            List<SkillTag> allSkillTags = await _context.SkillTag.ToListAsync();

            IEnumerable<SelectListItem> SkillTagsSelectList = new SelectList(allSkillTags, "SkillTagId", "Name");

            ViewBag.SkillTagsSelectList = SkillTagsSelectList;

            var selectedValues = new List<int>();

            if (user.SkillTags != null)
            {
                foreach (var tag in user.SkillTags)
                {
                    selectedValues.Add(tag.SkillTagId);
                }
            }

            InputModel input = new InputModel
            {
                UserName = user.UserName,
                FullName = user.FirstName + " " + user.LastName,
                Biography = user.Biography,
                Curriculum = user.Curriculum,
                SelectedSkillTags = selectedValues,
                
            };


            return View(input);
        }

        public class InputModel
        {
            public string UserName { get; set; }

            [Display(Name = "Full Name")]
            public string FullName { get; set; }

            [Display(Name = "Biography")]
            public string Biography { get; set; }

            [Display(Name = "Curriculum")]
            public string Curriculum { get; set; }

            [Display(Name = "Skill Tags")]
            public IEnumerable<int> SelectedSkillTags { get; set; }

        }
    }
}
