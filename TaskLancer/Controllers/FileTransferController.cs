﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using TaskLancer.Areas.Identity.Data;
using TaskLancer.Models.FileTransfer.BLL;
using TaskLancer.Views.FileTransfer;

namespace TaskLancer.Controllers
{
    public class FileTransferController : Controller
    {

		private readonly IFileTransferService _fileTransferService;

		private readonly UserManager<TaskLancerUser> _userManager;
		public FileTransferController(IFileTransferService fileTransferService, UserManager<TaskLancerUser> userManager)
        {
			_fileTransferService = fileTransferService;
			_userManager = userManager;
        }

		[Authorize]
        public IActionResult Index()
        {
			System.Security.Claims.ClaimsPrincipal currentUser = this.User;
			string userId = _userManager.GetUserId(currentUser);
			//var model = ConstructViewModel();

			List<Models.FileTransfer.DAL.File> files = _fileTransferService.GetUserUnusedFiles(userId);

			List<string> filesIds = files.Select(f => f.FileId).ToList();
			string json = JsonSerializer.Serialize(filesIds);

			FilesViewModel model = new FilesViewModel()
			{
				DownloadFiles = files,
				UploadPartialViewModel = new UploadPartialViewModel()
				{
					PreviouslyUploadedFiles = files,
					UploadedFilesJson = json,
					AccessPolicy = "public",
                }
			};

			return View(model);
		}

		[Authorize]
		[HttpPost]
		public async Task<JsonResult> UploadPublicFile(IFormFile file)
        {
			System.Security.Claims.ClaimsPrincipal currentUser = this.User;

			var fileName = System.IO.Path.GetFileName(file.FileName);

            Models.FileTransfer.DAL.File uploadedFile = await _fileTransferService.SavePublicFile(fileName, file.OpenReadStream(), _userManager.GetUserId(currentUser));

			var result = new
			{
				fileId = uploadedFile.FileId,
				fileName = uploadedFile.Name,
			};

			return Json(result);
		}

		[Authorize]
		[HttpPost]
		public async Task<JsonResult> UploadPrivateFile(IFormFile file)
		{
			System.Security.Claims.ClaimsPrincipal currentUser = this.User;

			var fileName = System.IO.Path.GetFileName(file.FileName);

            Models.FileTransfer.DAL.File uploadedFile = await _fileTransferService.SavePublicFile(fileName, file.OpenReadStream(), _userManager.GetUserId(currentUser));

			var result = new
			{
				fileId = uploadedFile.FileId,
				fileName = uploadedFile.Name,
			};

			return Json(result);
		}

		// Download file from the server
		[HttpDelete("FileTransfer/DeleteFile/{fileId}")]
		[Authorize]
		public async Task<ActionResult> DeleteFile(string fileId)
		{
			System.Security.Claims.ClaimsPrincipal currentUser = this.User;
			string userId = _userManager.GetUserId(currentUser);

            try
            {
				if (await _fileTransferService.RemoveFile(fileId, userId))
				{
					return Ok();
				}
				else
				{
					throw new Exception();
				}
			}
			catch(ArgumentException e)
            {
				throw e;
            }

			

			
		}

		// Download file from the server
		[HttpGet("FileTransfer/DownloadFile/{fileId}")]
		[Authorize]
		public async Task<FileResult> DownloadFile(string fileId)
		{
			System.Security.Claims.ClaimsPrincipal currentUser = this.User;
			string userId = _userManager.GetUserId(currentUser);

            Models.FileTransfer.DAL.File file = await _fileTransferService.GetFile(fileId, userId);

			Stream data = await _fileTransferService.GetFileStream(fileId, userId);

			// Reset stream reading position otherwise result fails with 500 why tho
			data.Position = 0;

			return File(data, GetContentType(file.Name), Path.GetFileName(file.Name));
		}

		// Get content type
		private string GetContentType(string path)
		{
			var types = GetMimeTypes();
			var ext = Path.GetExtension(path).ToLowerInvariant();
			return types[ext];
		}

		// Get mime types
		private Dictionary<string, string> GetMimeTypes()
		{
			return new Dictionary<string, string>
	{
		{".txt", "text/plain"},
		{".pdf", "application/pdf"},
		{".doc", "application/vnd.ms-word"},
		{".docx", "application/vnd.ms-word"},
		{".xls", "application/vnd.ms-excel"},
		{".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"},
		{".png", "image/png"},
		{".jpg", "image/jpeg"},
		{".jpeg", "image/jpeg"},
		{".gif", "image/gif"},
		{".csv", "text/csv"}
	};
		}

		//private FilesViewModel ConstructViewModel()
  //      {
		//	// Get files from the server
		//	var model = new FilesViewModel();

		//	foreach (var item in Directory.GetFiles(Path.Combine(Directory.GetCurrentDirectory(), "upload")))
		//	{
		//		model.Files.Add(
		//			new FileDetails { Name = System.IO.Path.GetFileName(item), Path = item });
		//	}

		//	return model;
		//}
		public class FilesViewModel
		{
			public List<Models.FileTransfer.DAL.File>? DownloadFiles;
			public UploadPartialViewModel? UploadPartialViewModel;
		}


	}

}
