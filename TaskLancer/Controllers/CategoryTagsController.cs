﻿#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TaskLancer.Data;
using TaskLancer.Models.Project.DAL;
using TaskLancer.Services;

namespace TaskLancer.Controllers
{
    public class CategoryTagsController : Controller
    {
        private readonly TaskLancerContext _context;
        private readonly ICategoryTagService _categorytagservice;
        public CategoryTagsController(TaskLancerContext context, ICategoryTagService categorytagservice)
        {
            _context = context;
            _categorytagservice = categorytagservice;
        }

        // GET: CategoryTags
        [Authorize(Roles = "administrator")]
        public async Task<IActionResult> Index()
        {
            return View(await _context.CategoryTag.ToListAsync());
        }

        // GET: CategoryTags/Details/5
        [Authorize(Roles = "administrator")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var categoryTag = await _context.CategoryTag
                .FirstOrDefaultAsync(m => m.CategoryTagId == id);
            if (categoryTag == null)
            {
                return NotFound();
            }

            return View(categoryTag);
        }

        // GET: CategoryTags/Create
        [Authorize(Roles = "administrator")]
        public IActionResult Create()
        {
            ViewBag.Message = "";
            return View();
        }

        // POST: CategoryTags/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "administrator")]
        public async Task<IActionResult> Create([Bind("CategoryTagId,Name,Description")] CategoryTag categoryTag)
        {
            if (ModelState.IsValid)
            {
                //chamar serviço
                if (!_categorytagservice.CheckIfExists(categoryTag))
                {
                    await _categorytagservice.AddCategoryTag(categoryTag.Name, categoryTag.Description);
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    ViewBag.Message = "CategoryTag already exist";
                    return View(categoryTag);
                }
            }
            return View(categoryTag);
        }

        // GET: CategoryTags/Edit/5
        [Authorize(Roles = "administrator")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var categoryTag = await _context.CategoryTag.FindAsync(id);
            if (categoryTag == null)
            {
                return NotFound();
            }
            ViewBag.Message = "";
            return View(categoryTag);
        }

        // POST: CategoryTags/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "administrator")]
        public async Task<IActionResult> Edit(int id, [Bind("CategoryTagId,Name,Description")] CategoryTag categoryTag)
        {
            if (id != categoryTag.CategoryTagId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    //chamar serviço
                    CategoryTag oldtag = _context.CategoryTag.Where<CategoryTag>(e => e.CategoryTagId == id).FirstOrDefault();
                    if (oldtag.Name.Equals(categoryTag.Name))
                    {
                        await _categorytagservice.ChangeCategoryTag(categoryTag.CategoryTagId, categoryTag.Name, categoryTag.Description);
                    }
                    else
                    {
                        if (!_categorytagservice.CheckIfExists(categoryTag))
                        {
                            await _categorytagservice.ChangeCategoryTag(categoryTag.CategoryTagId, categoryTag.Name, categoryTag.Description);
                        }
                        else
                        {
                            ViewBag.Message = "CategoryTag already exist";
                            return View(categoryTag);
                        }
                    }
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CategoryTagExists(categoryTag.CategoryTagId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(categoryTag);
        }

        // GET: CategoryTags/Delete/5
        [Authorize(Roles = "administrator")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var categoryTag = await _context.CategoryTag
                .FirstOrDefaultAsync(m => m.CategoryTagId == id);
            if (categoryTag == null)
            {
                return NotFound();
            }

            return View(categoryTag);
        }

        // POST: CategoryTags/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "administrator")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            //chamar serviço
            await _categorytagservice.DeleteCategoryTag(id);
            return RedirectToAction(nameof(Index));
        }

        [Authorize(Roles = "administrator")]
        private bool CategoryTagExists(int id)
        {
            return _context.CategoryTag.Any(e => e.CategoryTagId == id);
        }
    }
}
