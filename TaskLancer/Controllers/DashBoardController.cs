﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using TaskLancer.Areas.Identity.Data;
using TaskLancer.Data;
using TaskLancer.Models.Contracting.BLL;
using TaskLancer.Models.Contracting.DAL;
using TaskLancer.Models.Project.BLL;
using TaskLancer.Models.Project.DAL;
using TaskLancer.Services;

namespace TaskLancer.Controllers
{
    [Authorize]
    public class DashBoardController : Controller
    {
        private readonly TaskLancerContext _context;
        private readonly UserManager<TaskLancerUser> _userManager;
        private readonly IContractingService _contractingService;
        private readonly IProjectService _projectService;
        private readonly IStatisticsService _statisticsService;

        public DashBoardController(TaskLancerContext context, IStatisticsService statisticsService, UserManager<TaskLancerUser> userManager, IContractingService contractingService, IProjectService projectService)
        {
            _context = context;
            _userManager = userManager;
            _contractingService = contractingService;
            _projectService = projectService;
            _statisticsService = statisticsService;
        }
        
        [Authorize]
        public async Task<IActionResult> UnderConsiderationTasks()
        {
            System.Security.Claims.ClaimsPrincipal currentUser = this.User;

            TaskLancerUser user = await _userManager.GetUserAsync(currentUser);

            List<ContractingProcess> underConsiderationContracts = new List<ContractingProcess>();

            List<ContractingProcess> freelancerContracts = await _contractingService.AllContractingProcessesAsFreelancer(user.Id);

            foreach (var item in freelancerContracts)
            {
                if (item.State == ContractingProcessState.Published)
                {
                    underConsiderationContracts.Add(item);
                }
            }

            return View(underConsiderationContracts.ToList());
        }

        [Authorize]
        public async Task<IActionResult> OnGoingTasks()
        {
            System.Security.Claims.ClaimsPrincipal currentUser = this.User;

            TaskLancerUser user = await _userManager.GetUserAsync(currentUser);

            List<ContractingProcess> ongoingContracts = new List<ContractingProcess>();

            List<ContractingProcess> freelancerContracts = await _contractingService.AllContractingProcessesAsFreelancer(user.Id);

            foreach (var item in freelancerContracts)
            {
                if (item.State == ContractingProcessState.Ordered || item.State == ContractingProcessState.Pending_Payment)
                {
                    ongoingContracts.Add(item);
                }
            }

            return View(ongoingContracts.ToList());
        }

        [Authorize]
        public async Task<IActionResult> CompletedTasks()
        {
            System.Security.Claims.ClaimsPrincipal currentUser = this.User;

            TaskLancerUser user = await _userManager.GetUserAsync(currentUser);

            List<ContractingProcess> completedContracts = new List<ContractingProcess>();

            List<ContractingProcess> freelancerContracts = await _contractingService.AllContractingProcessesAsFreelancer(user.Id);

            foreach (var item in freelancerContracts)
            {
                if (item.State == ContractingProcessState.Completed)
                {
                    completedContracts.Add(item);
                }
            }

            return View(completedContracts.ToList());
        }

        [Authorize]
        public async Task<IActionResult> CanceledTasks()
        {
            System.Security.Claims.ClaimsPrincipal currentUser = this.User;

            TaskLancerUser user = await _userManager.GetUserAsync(currentUser);

            List<ContractingProcess> canceledContracts = new List<ContractingProcess>();

            List<ContractingProcess> freelancerContracts = await _contractingService.AllContractingProcessesAsFreelancer(user.Id);

            foreach (var item in freelancerContracts)
            {
                if (item.State == ContractingProcessState.Canceled)
                {
                    canceledContracts.Add(item);
                }
            }

            return View(canceledContracts.ToList());
        }

        [Authorize]
        public async Task<IActionResult> MyProjects()
        {
            System.Security.Claims.ClaimsPrincipal currentUser = this.User;

            TaskLancerUser user = await _userManager.GetUserAsync(currentUser);
            if(user != null)
            {
                List<Project> userProjects = await _projectService.GetAllUserProjects(user.Id);

                ProjectsList projectList = new ProjectsList
                {
                    projectModels = new List<ProjectModel>()
                };

                foreach (var p in userProjects)
                {
                    projectList.projectModels.Add(new ProjectModel
                    {
                        project = p,
                        totalTasks = await _statisticsService.ProjectTasksTotal(p.ProjectId),
                        totalPaid = await _statisticsService.TotalPaidByProject(p.ProjectId),
                        totalToPay = await _statisticsService.TotalToPayByProject(p.ProjectId),
                    });
                }

                return View(projectList);
            }
            return NotFound();
        }
        public class ProjectsList
        {
            public List<ProjectModel> projectModels { get; set; }
        }

        public class ProjectModel
        {
            public Project project { get; set; }

            public int totalTasks { get; set; }

            public float totalPaid { get; set; }

            public float totalToPay { get; set; }
        }

        [Authorize]
        public async Task<IActionResult> OutsourcedTasks()
        {
            System.Security.Claims.ClaimsPrincipal currentUser = this.User;

            TaskLancerUser user = await _userManager.GetUserAsync(currentUser);

            List<ContractingProcess> managerContracts = await _contractingService.AllContractingProcessesAsManager(user.Id);

            int totalPublished = 0;
            int totalPendingPayment = 0;
            int totalWaitingForDelivery = 0;
            int totalCompleted = 0;
            int totalCanceled = 0;

            foreach(var process in managerContracts)
            {
                if(process.State == ContractingProcessState.Published)
                {
                    totalPublished++;
                }else if(process.State == ContractingProcessState.Pending_Payment)
                {
                    totalPendingPayment++;
                }
                else if (process.State == ContractingProcessState.Ordered)
                {
                    totalWaitingForDelivery++;
                }
                else if (process.State == ContractingProcessState.Completed)
                {
                    totalCompleted++;
                }
                else if (process.State == ContractingProcessState.Canceled)
                {
                    totalCanceled++;
                }
            }

            OutSourcedTasksViewModel model = new OutSourcedTasksViewModel
            {
                contractingProcesses = managerContracts,
                totalPendingPayment = totalPendingPayment,
                totalCompleted = totalCompleted,
                totalCanceled = totalCanceled,
                totalPublished = totalPublished,
                totalWaitingForDelivery = totalWaitingForDelivery              
            };

            return View(model);
        }

        public class OutSourcedTasksViewModel
        {
            public List<ContractingProcess> contractingProcesses { get; set; }

            public int totalPublished { get; set; }

            public int totalPendingPayment { get; set; }

            public int totalWaitingForDelivery { get; set; }

            public int totalCompleted { get; set; }

            public int totalCanceled { get; set; }
        }

        [Authorize]
        public async Task<IActionResult> ManagerStats()
        {
            System.Security.Claims.ClaimsPrincipal currentUser = this.User;

            TaskLancerUser user = await _userManager.GetUserAsync(currentUser);

            ManagerStatisticsModel model = new ManagerStatisticsModel
            {
                totalProjects = await _statisticsService.TotalUserProjects(user.Id),
                totalPrivateProjects = await _statisticsService.UserProjectTotalByPrivacy(user.Id, false),
                totalPublicProjects = await _statisticsService.UserProjectTotalByPrivacy(user.Id, true),
                totalTasks = await _statisticsService.UserCreatedTasksTotal(user.Id),
                totalContracts = await _statisticsService.ContractingProcessesCreatedByManagerTotal(user.Id, null),
                totalContractsPublished = await _statisticsService.ContractingProcessesCreatedByManagerTotal(user.Id, ContractingProcessState.Published),
                totalContractsPendingPayment = await _statisticsService.ContractingProcessesCreatedByManagerTotal(user.Id, ContractingProcessState.Pending_Payment),
                totalContractsWaintingForDelivery = await _statisticsService.ContractingProcessesCreatedByManagerTotal(user.Id, ContractingProcessState.Ordered),
                totalContractsUnderEvaluation = await _statisticsService.ContractingProcessesCreatedByManagerTotal(user.Id, ContractingProcessState.Under_Evaluation),
                totalContractsCompleted = await _statisticsService.ContractingProcessesCreatedByManagerTotal(user.Id, ContractingProcessState.Completed),
                totalContractsCanceled = await _statisticsService.ContractingProcessesCreatedByManagerTotal(user.Id, ContractingProcessState.Canceled),
                totalPaid = await _statisticsService.ManagerPaidFinancialTotal(user.Id),
                totalWaitingForPayment = await _statisticsService.ManagerPendingFinancialTotal(user.Id),
                totalInCurrentProposals = await _statisticsService.ManagerProposedFinancialTotal(user.Id),
            };

            return View(model);
        }

        public class ManagerStatisticsModel
        {
            public int totalProjects { get; set; }

            public int totalPrivateProjects { get; set; }

            public int totalPublicProjects { get; set; }

            public int totalTasks { get; set; }

            public int totalContracts { get; set; }

            public int totalContractsPublished { get; set; }

            public int totalContractsPendingPayment { get; set; }

            public int totalContractsWaintingForDelivery { get; set; }

            public int totalContractsUnderEvaluation { get; set; }

            public int totalContractsCompleted { get; set; }

            public int totalContractsCanceled { get; set; }

            public float totalPaid { get; set; }

            public float totalWaitingForPayment { get; set; }

            public float totalInCurrentProposals { get; set; }
        }

        public class FreelancerStatisticsModel
        {
            public int totalContracts { get; set; }

            public int totalContractsPendingCounterProposal{ get; set; }

            public int totalContractsPendingManagerPayment { get; set; }

            public int totalContractsOngoing { get; set; }

            public int totalContractsWaitingManagerApproval { get; set; }

            public int totalContractsCompleted { get; set; }

            public int totalContractsCanceled { get; set; }

            public float totalReceived { get; set; }

            public float totalWaitingForPayout { get; set; }

            public float totalFromOngoingTasks{ get; set; }
        }

        [Authorize]
        public async Task<IActionResult> FreelancerStats()
        {
            System.Security.Claims.ClaimsPrincipal currentUser = this.User;

            TaskLancerUser user = await _userManager.GetUserAsync(currentUser);

            FreelancerStatisticsModel model = new FreelancerStatisticsModel
            {
                totalContracts = await _statisticsService.ContractingProcessesCreatedByFreelancerTotal(user.Id, null),
                totalContractsPendingCounterProposal = await _statisticsService.ContractingProcessesCreatedByFreelancerTotal(user.Id, ContractingProcessState.Published),
                totalContractsPendingManagerPayment = await _statisticsService.ContractingProcessesCreatedByFreelancerTotal(user.Id, ContractingProcessState.Pending_Payment),
                totalContractsOngoing = await _statisticsService.ContractingProcessesCreatedByFreelancerTotal(user.Id, ContractingProcessState.Ordered),
                totalContractsWaitingManagerApproval = await _statisticsService.ContractingProcessesCreatedByFreelancerTotal(user.Id, ContractingProcessState.Under_Evaluation),
                totalContractsCompleted = await _statisticsService.ContractingProcessesCreatedByFreelancerTotal(user.Id, ContractingProcessState.Completed),
                totalContractsCanceled = await _statisticsService.ContractingProcessesCreatedByFreelancerTotal(user.Id, ContractingProcessState.Canceled),
                totalReceived = await _statisticsService.FreelancerPaidFinancialTotal(user.Id),
                totalWaitingForPayout = await _statisticsService.FreelancerPendingFinancialTotal(user.Id),
                totalFromOngoingTasks = await _statisticsService.FreelancerExpectedFinancialTotal(user.Id)
            };

            return View(model);
        }
    }
}
