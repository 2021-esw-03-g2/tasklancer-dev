﻿#nullable disable
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TaskLancer.Areas.Identity.Data;
using TaskLancer.Data;
using TaskLancer.Models.Notifications.BLL;
using TaskLancer.Models.Notifications.DAL;
using WebPush;

namespace TaskLancer.Controllers
{
    public class NotificationSubscriptionsController : Controller
    {
        private readonly TaskLancerContext _context;
        private readonly UserManager<TaskLancerUser> _userManager;
        private readonly INotificationService _notificationService;

        public NotificationSubscriptionsController(TaskLancerContext context, UserManager<TaskLancerUser> userManager, INotificationService notificationService)
        {
            _context = context;
            _userManager = userManager;
            _notificationService = notificationService;
        }

        // GET: NotificationSubscriptions
        public async Task<IActionResult> Index()
        {
            System.Security.Claims.ClaimsPrincipal currentUser = this.User;

            TaskLancerUser user = await _userManager.GetUserAsync(currentUser);


            List<Notification> allUserNotifications = _notificationService.GetNotificationsAsync(user);

            //ViewBag.allUserNotifications = allUserNotifications;

            //allUserNotifications.Reverse();

            return View(allUserNotifications);
        }

        [Authorize]
        public async Task<IActionResult> UnSeenNotifications()
        {
            System.Security.Claims.ClaimsPrincipal currentUser = this.User;

            TaskLancerUser user = await _userManager.GetUserAsync(currentUser);

            //ViewBag.hasEdit = User.Identity.Name == user.UserName;

            List<Notification> UnSeenUserNotifications = _notificationService.GetUnseenNotifications(user);

            //ViewBag.allUserNotifications = allUserNotifications;

            //UnSeenUserNotifications.Reverse();

            return View(UnSeenUserNotifications);
        }

        [Authorize]
        public async Task<IActionResult> SeenNotifications()
        {
            System.Security.Claims.ClaimsPrincipal currentUser = this.User;

            TaskLancerUser user = await _userManager.GetUserAsync(currentUser);

            //ViewBag.hasEdit = User.Identity.Name == user.UserName;

            List<Notification> SeenUserNotifications = _notificationService.GetSeenNotifications(user);

            //ViewBag.allUserNotifications = allUserNotifications;

            //SeenUserNotifications.Reverse();

            return View(SeenUserNotifications);
        }

        //[HttpPost("/NotificationSubscriptions/Delete/{id}"), ActionName("Delete")]
        [HttpGet("/NotificationSubscriptions/Delete/{id}"), ActionName("Delete")]
        [Authorize]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id)
        {
            //devolver á página que o chamou e não apenas ao index
            System.Security.Claims.ClaimsPrincipal currentUser = this.User;
            TaskLancerUser user = await _userManager.GetUserAsync(currentUser);
            await _notificationService.DeleteNotification(user, id);
            return RedirectToAction("Index");
            //return View(_notificationService.GetNotifications(user));
        }

        //[HttpPost("/NotificationSubscriptions/Delete/{id}"), ActionName("Delete")]
        [HttpGet("/NotificationSubscriptions/ChangeStatus/{id}"), ActionName("ChangeStatus")]
        [Authorize]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> ChangeStatus(int id)
        {
            //devolver á página que o chamou e não apenas ao index
            System.Security.Claims.ClaimsPrincipal currentUser = this.User;
            TaskLancerUser user = await _userManager.GetUserAsync(currentUser);
            await _notificationService.SetNotificationStatus(user, id);
            List<Notification> notifications = _notificationService.GetNotificationsAsync(user);

            string url = "";

            foreach(var notification in notifications)
            {
                if(notification.NotificationId == id)
                {
                    url = notification.Link;
                }
            }
            //return RedirectToAction("Index");
            return Redirect(url);
            //return View(_notificationService.GetNotifications(user));
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> RegisterWebPushSubscription([FromBody] SubscriptionDTO subscription)
        {
            System.Security.Claims.ClaimsPrincipal currentUser = this.User;
            TaskLancerUser user = await _userManager.GetUserAsync(currentUser);

            if (user == null)
            {
                return BadRequest("No Client Name parsed.");
            }

            await _notificationService.SaveWebPushSubscription(user.Id, subscription.endpoint, subscription.p256dh, subscription.auth);
            return View("Notify");
        }

        public class SubscriptionDTO
        {
            public string endpoint { get; set; }

            public string p256dh { get; set; }

            public string auth { get; set; }
        }
    }
}
