﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TaskLancer.Migrations
{
    public partial class webpushv2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_NotificationSubscription_PushSubscription_WebPushSubscriptionPushSubscriptionId",
                table: "NotificationSubscription");

            migrationBuilder.DropIndex(
                name: "IX_NotificationSubscription_WebPushSubscriptionPushSubscriptionId",
                table: "NotificationSubscription");

            migrationBuilder.DropColumn(
                name: "WebPushSubscriptionPushSubscriptionId",
                table: "NotificationSubscription");

            migrationBuilder.AddColumn<int>(
                name: "NotificationSubscriptionId",
                table: "PushSubscription",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_PushSubscription_NotificationSubscriptionId",
                table: "PushSubscription",
                column: "NotificationSubscriptionId");

            migrationBuilder.AddForeignKey(
                name: "FK_PushSubscription_NotificationSubscription_NotificationSubscriptionId",
                table: "PushSubscription",
                column: "NotificationSubscriptionId",
                principalTable: "NotificationSubscription",
                principalColumn: "NotificationSubscriptionId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PushSubscription_NotificationSubscription_NotificationSubscriptionId",
                table: "PushSubscription");

            migrationBuilder.DropIndex(
                name: "IX_PushSubscription_NotificationSubscriptionId",
                table: "PushSubscription");

            migrationBuilder.DropColumn(
                name: "NotificationSubscriptionId",
                table: "PushSubscription");

            migrationBuilder.AddColumn<int>(
                name: "WebPushSubscriptionPushSubscriptionId",
                table: "NotificationSubscription",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_NotificationSubscription_WebPushSubscriptionPushSubscriptionId",
                table: "NotificationSubscription",
                column: "WebPushSubscriptionPushSubscriptionId");

            migrationBuilder.AddForeignKey(
                name: "FK_NotificationSubscription_PushSubscription_WebPushSubscriptionPushSubscriptionId",
                table: "NotificationSubscription",
                column: "WebPushSubscriptionPushSubscriptionId",
                principalTable: "PushSubscription",
                principalColumn: "PushSubscriptionId");
        }
    }
}
