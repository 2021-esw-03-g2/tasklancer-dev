﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TaskLancer.Migrations
{
    public partial class contracting_model_changes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ContractingEvents",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ContractingProcessId = table.Column<int>(type: "int", nullable: false),
                    TriggerDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    EventType = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContractingEvents", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ContractingEvents_ContractingProcess_ContractingProcessId",
                        column: x => x.ContractingProcessId,
                        principalTable: "ContractingProcess",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_ContractingEvents_ContractingProcessId",
                table: "ContractingEvents",
                column: "ContractingProcessId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ContractingEvents");
        }
    }
}
