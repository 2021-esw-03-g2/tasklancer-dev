﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TaskLancer.Migrations
{
    public partial class sprint2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "File",
                columns: table => new
                {
                    FileId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    OwnerId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    AccessPolicy = table.Column<int>(type: "int", nullable: false),
                    InUse = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_File", x => x.FileId);
                    table.ForeignKey(
                        name: "FK_File_AspNetUsers_OwnerId",
                        column: x => x.OwnerId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "UserPayment",
                columns: table => new
                {
                    UserPaymentId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    paypalEmail = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserPayment", x => x.UserPaymentId);
                    table.ForeignKey(
                        name: "FK_UserPayment_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserFileAllowedAccess",
                columns: table => new
                {
                    FilesAllowedAccessFileId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    UsersAllowedAccessId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserFileAllowedAccess", x => new { x.FilesAllowedAccessFileId, x.UsersAllowedAccessId });
                    table.ForeignKey(
                        name: "FK_UserFileAllowedAccess_AspNetUsers_UsersAllowedAccessId",
                        column: x => x.UsersAllowedAccessId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserFileAllowedAccess_File_FilesAllowedAccessFileId",
                        column: x => x.FilesAllowedAccessFileId,
                        principalTable: "File",
                        principalColumn: "FileId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Payment",
                columns: table => new
                {
                    PaymentId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ServiceFee = table.Column<float>(type: "real", nullable: false),
                    SubTotal = table.Column<float>(type: "real", nullable: false),
                    Total = table.Column<float>(type: "real", nullable: false),
                    Currency = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    State = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    InitialDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LimitDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    PaymentDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Title = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ProjectManagerId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    FreeLancerId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    UserPaymentId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Payment", x => x.PaymentId);
                    table.ForeignKey(
                        name: "FK_Payment_AspNetUsers_FreeLancerId",
                        column: x => x.FreeLancerId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Payment_AspNetUsers_ProjectManagerId",
                        column: x => x.ProjectManagerId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Payment_UserPayment_UserPaymentId",
                        column: x => x.UserPaymentId,
                        principalTable: "UserPayment",
                        principalColumn: "UserPaymentId");
                });

            migrationBuilder.CreateIndex(
                name: "IX_File_FileId",
                table: "File",
                column: "FileId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_File_OwnerId",
                table: "File",
                column: "OwnerId");

            migrationBuilder.CreateIndex(
                name: "IX_Payment_FreeLancerId",
                table: "Payment",
                column: "FreeLancerId");

            migrationBuilder.CreateIndex(
                name: "IX_Payment_ProjectManagerId",
                table: "Payment",
                column: "ProjectManagerId");

            migrationBuilder.CreateIndex(
                name: "IX_Payment_UserPaymentId",
                table: "Payment",
                column: "UserPaymentId");

            migrationBuilder.CreateIndex(
                name: "IX_UserFileAllowedAccess_UsersAllowedAccessId",
                table: "UserFileAllowedAccess",
                column: "UsersAllowedAccessId");

            migrationBuilder.CreateIndex(
                name: "IX_UserPayment_UserId",
                table: "UserPayment",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Payment");

            migrationBuilder.DropTable(
                name: "UserFileAllowedAccess");

            migrationBuilder.DropTable(
                name: "UserPayment");

            migrationBuilder.DropTable(
                name: "File");
        }
    }
}
