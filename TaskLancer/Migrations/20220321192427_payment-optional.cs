﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TaskLancer.Migrations
{
    public partial class paymentoptional : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ContractingProcess_Payment_PaymentId",
                table: "ContractingProcess");

            migrationBuilder.AlterColumn<int>(
                name: "PaymentId",
                table: "ContractingProcess",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_ContractingProcess_Payment_PaymentId",
                table: "ContractingProcess",
                column: "PaymentId",
                principalTable: "Payment",
                principalColumn: "PaymentId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ContractingProcess_Payment_PaymentId",
                table: "ContractingProcess");

            migrationBuilder.AlterColumn<int>(
                name: "PaymentId",
                table: "ContractingProcess",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_ContractingProcess_Payment_PaymentId",
                table: "ContractingProcess",
                column: "PaymentId",
                principalTable: "Payment",
                principalColumn: "PaymentId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
