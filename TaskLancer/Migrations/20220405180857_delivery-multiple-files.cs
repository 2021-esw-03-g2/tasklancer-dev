﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TaskLancer.Migrations
{
    public partial class deliverymultiplefiles : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Delivery_File_FinalFileId",
                table: "Delivery");

            migrationBuilder.DropForeignKey(
                name: "FK_Delivery_File_SampleFileId",
                table: "Delivery");

            migrationBuilder.DropIndex(
                name: "IX_Delivery_FinalFileId",
                table: "Delivery");

            migrationBuilder.DropIndex(
                name: "IX_Delivery_SampleFileId",
                table: "Delivery");

            migrationBuilder.DropColumn(
                name: "FinalFileId",
                table: "Delivery");

            migrationBuilder.DropColumn(
                name: "SampleFileId",
                table: "Delivery");

            migrationBuilder.CreateTable(
                name: "DeliveryFinalFiles",
                columns: table => new
                {
                    DeliveriesAsFinalId = table.Column<int>(type: "int", nullable: false),
                    FinalFilesFileId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DeliveryFinalFiles", x => new { x.DeliveriesAsFinalId, x.FinalFilesFileId });
                    table.ForeignKey(
                        name: "FK_DeliveryFinalFiles_Delivery_DeliveriesAsFinalId",
                        column: x => x.DeliveriesAsFinalId,
                        principalTable: "Delivery",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DeliveryFinalFiles_File_FinalFilesFileId",
                        column: x => x.FinalFilesFileId,
                        principalTable: "File",
                        principalColumn: "FileId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DeliverySampleFiles",
                columns: table => new
                {
                    DeliveriesAsSampleId = table.Column<int>(type: "int", nullable: false),
                    SampleFilesFileId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DeliverySampleFiles", x => new { x.DeliveriesAsSampleId, x.SampleFilesFileId });
                    table.ForeignKey(
                        name: "FK_DeliverySampleFiles_Delivery_DeliveriesAsSampleId",
                        column: x => x.DeliveriesAsSampleId,
                        principalTable: "Delivery",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DeliverySampleFiles_File_SampleFilesFileId",
                        column: x => x.SampleFilesFileId,
                        principalTable: "File",
                        principalColumn: "FileId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DeliveryFinalFiles_FinalFilesFileId",
                table: "DeliveryFinalFiles",
                column: "FinalFilesFileId");

            migrationBuilder.CreateIndex(
                name: "IX_DeliverySampleFiles_SampleFilesFileId",
                table: "DeliverySampleFiles",
                column: "SampleFilesFileId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DeliveryFinalFiles");

            migrationBuilder.DropTable(
                name: "DeliverySampleFiles");

            migrationBuilder.AddColumn<string>(
                name: "FinalFileId",
                table: "Delivery",
                type: "nvarchar(450)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "SampleFileId",
                table: "Delivery",
                type: "nvarchar(450)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_Delivery_FinalFileId",
                table: "Delivery",
                column: "FinalFileId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Delivery_SampleFileId",
                table: "Delivery",
                column: "SampleFileId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Delivery_File_FinalFileId",
                table: "Delivery",
                column: "FinalFileId",
                principalTable: "File",
                principalColumn: "FileId");

            migrationBuilder.AddForeignKey(
                name: "FK_Delivery_File_SampleFileId",
                table: "Delivery",
                column: "SampleFileId",
                principalTable: "File",
                principalColumn: "FileId");
        }
    }
}
