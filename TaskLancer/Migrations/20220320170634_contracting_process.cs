﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TaskLancer.Migrations
{
    public partial class contracting_process : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Delivery",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FinalComment = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    SampleFileId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    FinalFileId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ApprovalState = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Delivery", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Delivery_File_FinalFileId",
                        column: x => x.FinalFileId,
                        principalTable: "File",
                        principalColumn: "FileId");
                    table.ForeignKey(
                        name: "FK_Delivery_File_SampleFileId",
                        column: x => x.SampleFileId,
                        principalTable: "File",
                        principalColumn: "FileId");
                });

            migrationBuilder.CreateTable(
                name: "ContractingProcess",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TaskId = table.Column<int>(type: "int", nullable: false),
                    AcceptedCounterProposalId = table.Column<int>(type: "int", nullable: true),
                    PaymentId = table.Column<int>(type: "int", nullable: false),
                    State = table.Column<int>(type: "int", nullable: false),
                    PublishingDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CounterProposalsEndDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContractingProcess", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ContractingProcess_Payment_PaymentId",
                        column: x => x.PaymentId,
                        principalTable: "Payment",
                        principalColumn: "PaymentId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ContractingProcess_Task_TaskId",
                        column: x => x.TaskId,
                        principalTable: "Task",
                        principalColumn: "TaskId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CounterProposal",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FreelancerId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ContractingProcessId = table.Column<int>(type: "int", nullable: false),
                    SelfIntro = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Amount = table.Column<float>(type: "real", nullable: false),
                    DeliveryDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CounterProposal", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CounterProposal_AspNetUsers_FreelancerId",
                        column: x => x.FreelancerId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CounterProposal_ContractingProcess_ContractingProcessId",
                        column: x => x.ContractingProcessId,
                        principalTable: "ContractingProcess",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Proposal",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ManagerId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ContractingProcessId = table.Column<int>(type: "int", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Amount = table.Column<float>(type: "real", nullable: false),
                    DeliveryDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Proposal", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Proposal_AspNetUsers_ManagerId",
                        column: x => x.ManagerId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Proposal_ContractingProcess_ContractingProcessId",
                        column: x => x.ContractingProcessId,
                        principalTable: "ContractingProcess",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProposalFiles",
                columns: table => new
                {
                    ProposalsId = table.Column<int>(type: "int", nullable: false),
                    ReferenceFilesFileId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProposalFiles", x => new { x.ProposalsId, x.ReferenceFilesFileId });
                    table.ForeignKey(
                        name: "FK_ProposalFiles_File_ReferenceFilesFileId",
                        column: x => x.ReferenceFilesFileId,
                        principalTable: "File",
                        principalColumn: "FileId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProposalFiles_Proposal_ProposalsId",
                        column: x => x.ProposalsId,
                        principalTable: "Proposal",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ContractingProcess_AcceptedCounterProposalId",
                table: "ContractingProcess",
                column: "AcceptedCounterProposalId",
                unique: true,
                filter: "[AcceptedCounterProposalId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_ContractingProcess_PaymentId",
                table: "ContractingProcess",
                column: "PaymentId");

            migrationBuilder.CreateIndex(
                name: "IX_ContractingProcess_TaskId",
                table: "ContractingProcess",
                column: "TaskId");

            migrationBuilder.CreateIndex(
                name: "IX_CounterProposal_ContractingProcessId",
                table: "CounterProposal",
                column: "ContractingProcessId");

            migrationBuilder.CreateIndex(
                name: "IX_CounterProposal_FreelancerId",
                table: "CounterProposal",
                column: "FreelancerId");

            migrationBuilder.CreateIndex(
                name: "IX_Delivery_FinalFileId",
                table: "Delivery",
                column: "FinalFileId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Delivery_SampleFileId",
                table: "Delivery",
                column: "SampleFileId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Proposal_ContractingProcessId",
                table: "Proposal",
                column: "ContractingProcessId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Proposal_ManagerId",
                table: "Proposal",
                column: "ManagerId");

            migrationBuilder.CreateIndex(
                name: "IX_ProposalFiles_ReferenceFilesFileId",
                table: "ProposalFiles",
                column: "ReferenceFilesFileId");

            migrationBuilder.AddForeignKey(
                name: "FK_ContractingProcess_CounterProposal_AcceptedCounterProposalId",
                table: "ContractingProcess",
                column: "AcceptedCounterProposalId",
                principalTable: "CounterProposal",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ContractingProcess_CounterProposal_AcceptedCounterProposalId",
                table: "ContractingProcess");

            migrationBuilder.DropTable(
                name: "Delivery");

            migrationBuilder.DropTable(
                name: "ProposalFiles");

            migrationBuilder.DropTable(
                name: "Proposal");

            migrationBuilder.DropTable(
                name: "CounterProposal");

            migrationBuilder.DropTable(
                name: "ContractingProcess");
        }
    }
}
