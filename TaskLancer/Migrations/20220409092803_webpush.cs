﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TaskLancer.Migrations
{
    public partial class webpush : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "WebPushKey",
                table: "NotificationSubscription");

            migrationBuilder.AddColumn<int>(
                name: "WebPushSubscriptionPushSubscriptionId",
                table: "NotificationSubscription",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "PushSubscription",
                columns: table => new
                {
                    PushSubscriptionId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Endpoint = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    P256dh = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Auth = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PushSubscription", x => x.PushSubscriptionId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_NotificationSubscription_WebPushSubscriptionPushSubscriptionId",
                table: "NotificationSubscription",
                column: "WebPushSubscriptionPushSubscriptionId");

            migrationBuilder.AddForeignKey(
                name: "FK_NotificationSubscription_PushSubscription_WebPushSubscriptionPushSubscriptionId",
                table: "NotificationSubscription",
                column: "WebPushSubscriptionPushSubscriptionId",
                principalTable: "PushSubscription",
                principalColumn: "PushSubscriptionId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_NotificationSubscription_PushSubscription_WebPushSubscriptionPushSubscriptionId",
                table: "NotificationSubscription");

            migrationBuilder.DropTable(
                name: "PushSubscription");

            migrationBuilder.DropIndex(
                name: "IX_NotificationSubscription_WebPushSubscriptionPushSubscriptionId",
                table: "NotificationSubscription");

            migrationBuilder.DropColumn(
                name: "WebPushSubscriptionPushSubscriptionId",
                table: "NotificationSubscription");

            migrationBuilder.AddColumn<int>(
                name: "WebPushKey",
                table: "NotificationSubscription",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
