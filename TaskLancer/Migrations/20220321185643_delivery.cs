﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TaskLancer.Migrations
{
    public partial class delivery : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ContractingProcessId",
                table: "Delivery",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Delivery_ContractingProcessId",
                table: "Delivery",
                column: "ContractingProcessId");

            migrationBuilder.AddForeignKey(
                name: "FK_Delivery_ContractingProcess_ContractingProcessId",
                table: "Delivery",
                column: "ContractingProcessId",
                principalTable: "ContractingProcess",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Delivery_ContractingProcess_ContractingProcessId",
                table: "Delivery");

            migrationBuilder.DropIndex(
                name: "IX_Delivery_ContractingProcessId",
                table: "Delivery");

            migrationBuilder.DropColumn(
                name: "ContractingProcessId",
                table: "Delivery");
        }
    }
}
