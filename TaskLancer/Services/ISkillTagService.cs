﻿using TaskLancer.Areas.Identity.Data;
using TaskLancer.Models;

namespace TaskLancer.Services
{
    public interface ISkillTagService
    {
        Task<List<SkillTag>> GetAllAsync();
        bool CheckIfExists(SkillTag skilltag);
        Task<int> AddSkillTag(string name, string description);
        Task<int> DeleteSkillTag(int skillTagId);
        Task<int> ChangeSkillTag(int skillTagId, string name, string description);
    }
}
