﻿using TaskLancer.Models.Project.DAL;

namespace TaskLancer.Services
{
    public interface ICategoryTagService
    {
        bool CheckIfExists(CategoryTag CategoryTag);
        Task<int> AddCategoryTag(string name, string description);
        Task<int> DeleteCategoryTag(int categoryTagId);
        Task<int> ChangeCategoryTag(int categoryTagId, string name, string description);
    }
}
