﻿using Microsoft.EntityFrameworkCore;
using TaskLancer.Areas.Identity.Data;
using TaskLancer.Data;
using TaskLancer.Models.Contracting.BLL;
using TaskLancer.Models.Contracting.DAL;
using TaskLancer.Models.Payments.DAL;
using TaskLancer.Models.Project.BLL;
using TaskLancer.Models.Project.DAL;

namespace TaskLancer.Services
{
    public class StatisticsService : IStatisticsService
    {
        private readonly IContractingService _contractingService;
        private readonly TaskLancerContext _context;
        private readonly IProjectService _projectService;

        public StatisticsService(TaskLancerContext ctx, IContractingService contractingService, IProjectService projectService)
        {
            _context = ctx;
            _contractingService = contractingService;
            _projectService = projectService;
        }

        /// <summary>
        /// Returns the number of contracting processes by state of a certain project
        /// If state is null returns the total number of processes
        /// </summary>
        /// <param name="state"></param>
        /// <param name="projectId"></param>
        /// <returns></returns>
        public async Task<int> ContractingProcessesByProjectTotal(ContractingProcessState? state, int projectId)
        {
            if (state == null)
            {
                List<ContractingProcess> contractingProcesses = await _context.ContractingProcess.Where(c => c.Task.Board.ProjectId == projectId).ToListAsync();
                return contractingProcesses.Count;
            }
            else
            {
                List<ContractingProcess> contractingProcesses2 = await _context.ContractingProcess.Where(c => c.State == state && c.Task.Board.ProjectId == projectId).ToListAsync();
                return contractingProcesses2.Count;
            }
        }

        /// <summary>
        /// Returns the number of contracting processes by state of a certain manager
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        public async Task<int> ContractingProcessesCreatedByManagerTotal(string userId, ContractingProcessState? state)
        {
            if (state == null)
            {
                List<ContractingProcess> contractingProcesses = await _context.ContractingProcess.Where(c=> c.Proposal.Manager.Id == userId).ToListAsync();
                return contractingProcesses.Count;
            }
            else
            {
                List<ContractingProcess> contractingProcesses2 = await _context.ContractingProcess.Where(c => c.State == state && c.Proposal.Manager.Id == userId).ToListAsync();
                return contractingProcesses2.Count;
            }
        }

        /// <summary>
        /// Returns the number of contracting processes by state of a certain freelancer
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        public async Task<int> ContractingProcessesCreatedByFreelancerTotal(string userId, ContractingProcessState? state)
        {
            if (state == null)
            {
                List<ContractingProcess> contractingProcesses = await _contractingService.AllContractingProcessesAsFreelancer(userId);
                return contractingProcesses.Count;
            }
            else
            {
                int count = 0;

                List<ContractingProcess> contractingProcesses2 = await _contractingService.AllContractingProcessesAsFreelancer(userId);

                foreach(var contract in contractingProcesses2)
                {
                    if(contract.State == state)
                    {
                        count++;
                    }
                }
                    
                return count;
            }
        }

        /// <summary>
        /// Returns the total of contracting processes
        /// </summary>
        /// <param name="state"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<int> ContractingProcessesTotal(ContractingProcessState? state)
        {
            if (state == null)
            {
                List<ContractingProcess> contractingProcesses = await _context.ContractingProcess.ToListAsync();
                return contractingProcesses.Count;
            }
            else
            {
                List<ContractingProcess> contractingProcesses2 = await _context.ContractingProcess.Where(c => c.State == state).ToListAsync();
                return contractingProcesses2.Count;
            }
            throw new NotImplementedException();
        }

        /// <summary>
        /// Returns the total amount of money a freelancer expects to recieve
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<float> FreelancerExpectedFinancialTotal(string userId)
        {
            float total = 0;
            List<Payment> UserPayments = await _context.Payment.Where(p => p.State == PaymentState.Paid && p.FreeLancer.Id == userId).ToListAsync();
            foreach (Payment payment in UserPayments)
            {
                total += payment.SubTotal;
            }
            return total;
        }

        /// <summary>
        /// Returns the total amount of money a freelancer already recieved
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<float> FreelancerPaidFinancialTotal(string userId)
        {
            float total = 0;
            List<Payment> UserPayments = await _context.Payment.Where(p => p.State == PaymentState.Paid_Out && p.FreeLancer.Id == userId).ToListAsync();
            foreach (Payment payment in UserPayments)
            {
                total += payment.SubTotal;
            }
            return total;
        }

        /// <summary>
        /// Returns the total amount of money a freelancer has available to collect
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<float> FreelancerPendingFinancialTotal(string userId)
        {
            float total = 0;
            List<Payment> UserPayments = await _context.Payment.Where(p => p.State == PaymentState.Pending_Payout && p.FreeLancer.Id == userId).ToListAsync();
            foreach (Payment payment in UserPayments)
            {
                total += payment.SubTotal;
            }
            return total;
        }

        /// <summary>
        /// Returns the total amount of money a Manager has paid
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<float> ManagerPaidFinancialTotal(string userId)
        {
            float total = 0;
            List<Payment> UserPayments = await _context.Payment.Where(p => p.State == PaymentState.Paid && p.ProjectManager.Id == userId).ToListAsync();
            foreach (Payment payment in UserPayments)
            {
                total += payment.Total;
            }
            return total;
        }

        /// <summary>
        /// Returns the total amount of money a Manager has to pay in ongoing contracting processes
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<float> ManagerPendingFinancialTotal(string userId)
        {
            float total = 0;
            List<Payment> UserPayments = await _context.Payment.Where(p => p.State == PaymentState.Pending_Payment && p.ProjectManager.Id == userId).ToListAsync();
            foreach (Payment payment in UserPayments)
            {
                total += payment.Total;
            }
            return total;
        }


        /// <summary>
        /// Returns the total amount of money a Manager has to pay in proposed contracting processes
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<float> ManagerProposedFinancialTotal(string userId)
        {
            float total = 0;
            List<ContractingProcess> contractingProcesses = await _context.ContractingProcess.Where(c => c.State == ContractingProcessState.Published && c.Proposal.Manager.Id == userId).Include(c => c.Proposal).ToListAsync();
            foreach (ContractingProcess cont in contractingProcesses)
            {
                if(cont.Proposal != null) {
                    total += cont.Proposal.Amount;
                }
            }
            return total;
        }

        /// <summary>
        /// Returns the total amount of money tranfered between freelancers and managers
        /// </summary>
        /// <returns></returns>
        public async Task<float> OutsourcingFinancialTotal()
        {
            float total = 0;
            List<Payment> payments = await _context.Payment.ToListAsync();
            foreach (Payment payment in payments)
            {
                total += payment.SubTotal;
            }
            return total;
        }

        /// <summary>
        /// Returns the number of Freelancers on the platform
        /// </summary>
        /// <returns></returns>
        public async Task<int> ParticipatingFreelancerTotal()
        {
            List<TaskLancerUser> freelancers = new List<TaskLancerUser>();
            List<TaskLancerUser> users = await _context.Users.ToListAsync();
            List<Payment> payments = await _context.Payment.ToListAsync();
            foreach (Payment payment in payments)
            {
                foreach (var user in users)
                {
                    if(user.Id == payment.FreeLancer.Id)
                    {
                        freelancers.Add(user);
                    }
                }
            }
            return freelancers.Distinct().Count();
        }

        /// <summary>
        /// Returns the number of Managers on the platform
        /// </summary>
        /// <returns></returns>
        public async Task<int> ParticipatingManagerTotal()
        {
            List<TaskLancerUser> managers = new List<TaskLancerUser>();
            List<TaskLancerUser> users = await _context.Users.ToListAsync();
            List<Payment> payments = await _context.Payment.ToListAsync();
            foreach (Payment payment in payments)
            {
                foreach (var user in users)
                {
                    if (user.Id == payment.ProjectManager.Id)
                    {
                        managers.Add(user);
                    }
                }
            }
            return managers.Distinct().Count();
        }

        /// <summary>
        /// Returns the total of payments done on the platform
        /// </summary>
        /// <returns></returns>
        public async Task<int> PaymentTotal()
        {
            List<Payment> payments = await _context.Payment.ToListAsync();
            return payments.Count;
        }

        /// <summary>
        /// Returns the total of payments by state
        /// </summary>
        /// <param name="state"></param>
        /// <returns></returns>
        public async Task<int> PaymentTotalByState(PaymentState state)
        {
            List<Payment> payments = await _context.Payment.Where(p => p.State == state).ToListAsync();
            return payments.Count;
        }

        /// <summary>
        /// Returns the total profit of the platform
        /// </summary>
        /// <returns></returns>
        public async Task<float> PlatformProfitFinancialTotal()
            {
            float total = 0;
            List<Payment> payments = await _context.Payment.Where(p => p.State >= PaymentState.Paid).ToListAsync();
            foreach (Payment payment in payments)
            {
                total += payment.ServiceFee;
            }
            return total;
        }

        /// <summary>
        /// Returns the total tasks of a certain project
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        public async Task<int> ProjectTasksTotal(int projectId)
        {
            List<ContractingProcess> contractingProcesses = await _context.ContractingProcess.Where(c => c.Task.Board.ProjectId == projectId).ToListAsync();
            return contractingProcesses.Count;
        }

        /// <summary>
        /// Returns the total number of projects
        /// </summary>
        /// <returns></returns>
        public async Task<int> ProjectTotal()
        {
            List<Project> Projects = await _context.Project.ToListAsync();
            return Projects.Count;
        }

        /// <summary>
        /// Returns the total number of tasks created by a certain user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<int> UserCreatedTasksTotal(string userId)
        {
            List<ContractingProcess> contractingProcesses = await _context.ContractingProcess.Where(c => c.Proposal.Manager.Id == userId).ToListAsync();
            return contractingProcesses.Count;
        }

        /// <summary>
        /// Returns the total number of registered users
        /// </summary>
        /// <returns></returns>
        public async Task<int> UserTotal()
        {
            List<TaskLancerUser> users = _context.Users.ToList();
            return users.Count;
        }

        /// <summary>
        /// Returns the total number of projects of a certain user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<int> TotalUserProjects(string userId)
        {
            List<Project> projects = await _projectService.GetAllUserProjects(userId);
            return projects.Count;
        }

        /// <summary>
        /// Returns the total number of projects of a certain user with the inserted privacy state
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="isPublic"></param>
        /// <returns></returns>
        public async Task<int> UserProjectTotalByPrivacy(string userId, bool isPublic)
        {
            List<Project> projects = await _projectService.GetAllUserProjects(userId);

            int count = 0;

            foreach(var project in projects)
            {
                if(project.IsPublic == isPublic)
                {
                    count++;
                }
            }

            return count;
        }

        /// <summary>
        /// Returns the number of projects on the platform
        /// </summary>
        /// <returns></returns>
        public async Task<int> ProjectsTotal()
        {
            List<Project> projects = await _context.Project.ToListAsync();
            return projects.Count;
        }

        /// <summary>
        /// Returns the total amount of money paid in a certain project
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        public async Task<float> TotalPaidByProject(int projectId)
        {
            float count = 0;

            List<ContractingProcess> contractingProcesses = await _context.ContractingProcess.Where(c => c.Task.Board.ProjectId == projectId).Include(c => c.AcceptedCounterProposal).ToListAsync();

            foreach (var process in contractingProcesses)
            {
                if(process.State == ContractingProcessState.Ordered)
                {
                    count += process.AcceptedCounterProposal.Amount;
                }
            }

            return count;
        }

        /// <summary>
        /// Returns the total amount of money to pay in a certain project
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        public async Task<float> TotalToPayByProject(int projectId)
        {
            float count = 0;

            List<ContractingProcess> contractingProcesses = await _context.ContractingProcess.Where(c => c.Task.Board.ProjectId == projectId).Include(c => c.Proposal).Include(c => c.AcceptedCounterProposal).ToListAsync();

            foreach (var process in contractingProcesses)
            {
                if (process.State == ContractingProcessState.Published)
                {
                    count += process.Proposal.Amount;
                }
                if (process.State == ContractingProcessState.Pending_Payment)
                {
                    count += process.AcceptedCounterProposal.Amount;
                }
            }

            return count;
        }
        public async Task<List<int>> AverageTimeContractsCompleted()
        {
            TimeSpan diff = TimeSpan.Zero;
            double averageSec = 0;
            List<int> time = new List<int>();
            List<ContractingProcess> contractingProcesses = await _context.ContractingProcess.Where(c => c.State == ContractingProcessState.Completed).Include(c => c.AcceptedCounterProposal).ToListAsync();
            if (contractingProcesses.Count == 0)
            {
                time.Add(0);
                time.Add(0);
                time.Add(0);
                return time;
            }
             foreach (ContractingProcess process in contractingProcesses)
             {
                 diff += process.AcceptedCounterProposal.DeliveryDate - process.PublishingDate;
             }
             averageSec = diff.TotalSeconds / contractingProcesses.Count;
             TimeSpan aveTime = TimeSpan.FromSeconds(averageSec);
             time.Add(aveTime.Days);
             time.Add(aveTime.Hours);
             time.Add(aveTime.Minutes);
            return time;
        }
    }
}
