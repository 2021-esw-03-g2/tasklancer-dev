﻿using TaskLancer.Models.Contracting.DAL;
using TaskLancer.Models.Payments.DAL;

namespace TaskLancer.Services
{
    public interface IStatisticsService
    {
        Task<int> UserTotal();
        Task<int> ProjectsTotal();
        Task<int> ParticipatingManagerTotal();
        Task<int> ParticipatingFreelancerTotal();
        Task<int> ProjectTotal();
        Task<int> PaymentTotalByState(PaymentState state);
        Task<int> PaymentTotal();
        Task<int> ContractingProcessesByProjectTotal(ContractingProcessState? state, int projectId);
        Task<float> TotalPaidByProject(int projectId);
        Task<float> TotalToPayByProject(int projectId);
        Task<int> ContractingProcessesCreatedByManagerTotal(string userId, ContractingProcessState? state);
        Task<int> ContractingProcessesCreatedByFreelancerTotal(string userId, ContractingProcessState? state);
        Task<int> TotalUserProjects(string userId);
        Task<int> UserProjectTotalByPrivacy(string userId, bool isPublic);
        Task<float> ManagerProposedFinancialTotal(string userId);
        Task<float> ManagerPendingFinancialTotal(string userId);
        Task<float> ManagerPaidFinancialTotal(string userId);
        Task<float> FreelancerExpectedFinancialTotal(string userId);
        Task<float> FreelancerPendingFinancialTotal(string userId);
        Task<float> FreelancerPaidFinancialTotal(string userId);
        Task<int> UserCreatedTasksTotal(string userId);
        Task<int> ProjectTasksTotal(int projectId);
        Task<int> ContractingProcessesTotal(ContractingProcessState? state);
        Task<float> OutsourcingFinancialTotal();
        Task<float> PlatformProfitFinancialTotal();
        Task<List<int>> AverageTimeContractsCompleted();
    }
}
