﻿namespace TaskLancer.Services
{
    public interface IScopedSchedulerService
    {
        Task ExecuteAsync(CancellationToken cancellationToken);
    }
}