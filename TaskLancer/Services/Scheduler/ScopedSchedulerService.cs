﻿namespace TaskLancer.Services.Scheduler
{
    public class ScopedSchedulerService : IScopedSchedulerService
    {
        private readonly ILogger<ScopedSchedulerService> _logger;

        public ScopedSchedulerService(ILogger<ScopedSchedulerService> logger)
        {
            _logger = logger;
        }

        public Task ExecuteAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now);

            return Task.CompletedTask;
        }
    }
}
