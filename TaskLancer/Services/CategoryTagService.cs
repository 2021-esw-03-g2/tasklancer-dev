﻿using TaskLancer.Data;
using TaskLancer.Models.Project.DAL;

namespace TaskLancer.Services
{
    public class CategoryTagService : ICategoryTagService
    {
        private readonly TaskLancerContext _context;

        public CategoryTagService(TaskLancerContext ctx)
        {
            _context = ctx;
        }
        private CategoryTag GetCategoryTagId(int categorytagId)
        {
            return _context.CategoryTag.Where<CategoryTag>(e => e.CategoryTagId == categorytagId).FirstOrDefault();
        }
        public bool CheckIfExists(CategoryTag categoryTag)
        {
            List<CategoryTag> allcategoryTags = _context.CategoryTag.ToList();
            foreach (CategoryTag tag in allcategoryTags)
            {
                if (tag.Name == categoryTag.Name)
                {
                    return true;
                }
            }

            return false;
        }

        public async Task<int> AddCategoryTag(string name, string description)
        {
            CategoryTag categorytag = new CategoryTag
            {
                Name = name,
                Description = description,
            };
            _context.CategoryTag.Add(categorytag);

            return await _context.SaveChangesAsync();
        }

        public async Task<int> ChangeCategoryTag(int categoryTagId, string name, string description)
        {
            CategoryTag categoryTag = GetCategoryTagId(categoryTagId);

            if (categoryTag == null)
            {
                throw new ArgumentException("CategoryTag not found.");
            }

            categoryTag.Name = name;
            categoryTag.Description = description;
            _context.CategoryTag.Update(categoryTag);

            return await _context.SaveChangesAsync();
        }

        public async Task<int> DeleteCategoryTag(int categoryTagId)
        {
            CategoryTag categoryTag = GetCategoryTagId(categoryTagId);

            if (categoryTag == null)
            {
                throw new ArgumentException("CategoryTag not found.");
            }

            _context.CategoryTag.Remove(categoryTag);

            return await _context.SaveChangesAsync();
        }

    }
}
