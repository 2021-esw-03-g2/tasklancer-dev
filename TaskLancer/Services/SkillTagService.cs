﻿using Microsoft.EntityFrameworkCore;
using TaskLancer.Areas.Identity.Data;
using TaskLancer.Data;
using TaskLancer.Models;

namespace TaskLancer.Services
{
    /// <summary>
    ///  Service for SkillTag Model
    /// </summary>
    public class SkillTagService : ISkillTagService
    {
        private readonly TaskLancerContext _context;

        public SkillTagService(TaskLancerContext ctx)
        {
            _context = ctx;
        }

        /// <summary>
        /// Returns a skilltag with the given ID
        /// </summary>
        /// <param name="skilltagId"></param>
        /// <returns></returns>
        private SkillTag GetSkillTagId(int skilltagId)
        {
            return _context.SkillTag.Where<SkillTag>(e => e.SkillTagId == skilltagId).FirstOrDefault();
        }

        /// <summary>
        /// Check if a skilltag exists
        /// </summary>
        /// <param name="skilltag"></param>
        /// <returns></returns>
        public bool CheckIfExists(SkillTag skilltag)
        {
            List<SkillTag> allskilltags = _context.SkillTag.ToList();
            foreach (SkillTag tag in allskilltags)
            {
                if (tag.Name == skilltag.Name)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Adds a skillTag
        /// </summary>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        public async Task<int> AddSkillTag(string name, string description)
        {
            SkillTag skilltag = new SkillTag
            {
                Name = name,
                Description = description,
            };
            _context.SkillTag.Add(skilltag);

            return await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Changes a skilltag
        /// </summary>
        /// <param name="skillTagId"></param>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        public async Task<int> ChangeSkillTag(int skillTagId, string name, string description)
        {
            SkillTag skillTag = GetSkillTagId(skillTagId);

            if (skillTag == null)
            {
                throw new ArgumentException("SkillTag not found.");
            }
                skillTag.Name = name;
                skillTag.Description = description;
                _context.SkillTag.Update(skillTag);

            return await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Deletes a skill tag
        /// </summary>
        /// <param name="skillTagId"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        public async Task<int> DeleteSkillTag(int skillTagId)
        {
            SkillTag skillTag = GetSkillTagId(skillTagId);

            if (skillTag == null)
            {
                throw new ArgumentException("SkillTag not found.");
            }

            _context.SkillTag.Remove(skillTag);

            return await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Returns a List of all SkillTags available
        /// </summary>
        /// <returns></returns>
        public async Task<List<SkillTag>> GetAllAsync()
        {
            return await _context.SkillTag.ToListAsync();
        }
    }
}
