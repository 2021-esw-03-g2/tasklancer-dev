﻿// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
#nullable disable

using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using TaskLancer.Areas.Identity.Data;
using TaskLancer.Models;
using TaskLancer.Models.Notifications.BLL;
using TaskLancer.Models.Payments.BLL;
using TaskLancer.Services;

namespace TaskLancer.Areas.Identity.Pages.Account.Manage
{
    public class IndexModel : PageModel
    {
        private readonly UserManager<TaskLancerUser> _userManager;
        private readonly SignInManager<TaskLancerUser> _signInManager;
        private readonly IProfileService _profileService;
        private readonly ISkillTagService _skillTagService;
        private readonly IEmailNotificationService _emailNotificationService;
        private readonly INotificationService _notificationService;
        private readonly IPaymentService _paymentService;

        public IndexModel(
            UserManager<TaskLancerUser> userManager,
            SignInManager<TaskLancerUser> signInManager,
            IProfileService profileService,
            ISkillTagService skillTagService,
            IEmailNotificationService emailNotificationService, 
            INotificationService notificationService,
            IPaymentService paymentService)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _profileService = profileService;
            _skillTagService = skillTagService;
            _emailNotificationService = emailNotificationService;
            _notificationService = notificationService;
            _paymentService = paymentService;
        }

        /// <summary>
        ///     This API supports the ASP.NET Core Identity default UI infrastructure and is not intended to be used
        ///     directly from your code. This API may change or be removed in future releases.
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        ///     This API supports the ASP.NET Core Identity default UI infrastructure and is not intended to be used
        ///     directly from your code. This API may change or be removed in future releases.
        /// </summary>
        [TempData]
        public string StatusMessage { get; set; }

        /// <summary>
        ///     This API supports the ASP.NET Core Identity default UI infrastructure and is not intended to be used
        ///     directly from your code. This API may change or be removed in future releases.
        /// </summary>
        [BindProperty]
        public InputModel Input { get; set; }
       
        [BindProperty]
        public IEnumerable<SelectListItem> SkillTagsSelectList { get; set; }

        /// <summary>
        ///     This API supports the ASP.NET Core Identity default UI infrastructure and is not intended to be used
        ///     directly from your code. This API may change or be removed in future releases.
        /// </summary>
        public class InputModel
        {
            /// <summary>
            ///     This API supports the ASP.NET Core Identity default UI infrastructure and is not intended to be used
            ///     directly from your code. This API may change or be removed in future releases.
            /// </summary>
            [Phone]
            [Display(Name = "Phone number")]
            public string PhoneNumber { get; set; }

            [EmailAddress]
            [Display(Name = "Paypal Email")]
            public string PaypalEmail { get; set; }

            [Display(Name = "Biography")]
            public string Biography { get; set; }

            [Display(Name = "Curriculum")]
            public string Curriculum { get; set; }

            [Display(Name = "Skill Tags")]
            public IEnumerable<int> SelectedSkillTags { get; set; }

            [Display(Name = "Email Notifications")]
            public bool IsEmailSubscribed { get; set; }

        }

        public class SkillTagDTO
        {
            public int SkillTagId { get; set; }

            public string Name { get; set; }

            public string Description { get; set; }
        }

        private async Task LoadAsync(TaskLancerUser user)
        {
            var userName = await _userManager.GetUserNameAsync(user);
            var phoneNumber = await _userManager.GetPhoneNumberAsync(user);
            var biography = user.Biography;
            var curriculum = user.Curriculum;
            var skillTags = _profileService.GetSkillTags(user);
            List<SkillTag> allSkillTags = await _skillTagService.GetAllAsync();
            var isEmailSubscribed = await _emailNotificationService.IsSubscribedEmail(user);
            var paypalEmail = _paymentService.GetPayPalEmail(user);

            
            SkillTagsSelectList = new SelectList(allSkillTags, "SkillTagId", "Name");

            var selectedValues = new List<int>();

            if (skillTags != null)
            {
                foreach (var skillTag in skillTags)
                {
                    selectedValues.Add(skillTag.SkillTagId);
                }
            }     

            Username = userName;

            Input = new InputModel
            {
                PhoneNumber = phoneNumber,
                PaypalEmail = paypalEmail,
                Biography = biography,
                Curriculum = curriculum,
                SelectedSkillTags = selectedValues,
                IsEmailSubscribed = isEmailSubscribed
            };

            
        }

        private SkillTagDTO ToSkillTagDTOMap(SkillTag skillTag)
        {
            return new SkillTagDTO
            {
                SkillTagId = skillTag.SkillTagId,
                Name = skillTag.Name,
                Description = skillTag.Description,
            };
        }

        public async Task<IActionResult> OnGetAsync()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            await LoadAsync(user);
            ViewData["skillTags"] = SkillTagsSelectList;
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            if (!ModelState.IsValid)
            {
                await LoadAsync(user);
                return Page();
            }

            var phoneNumber = await _userManager.GetPhoneNumberAsync(user);
            if (Input.PhoneNumber != phoneNumber)
            {
                var setPhoneResult = await _userManager.SetPhoneNumberAsync(user, Input.PhoneNumber);
                if (!setPhoneResult.Succeeded)
                {
                    StatusMessage = "Unexpected error when trying to set phone number.";
                    return RedirectToPage();
                }
            }

            var paypalEmail = _paymentService.GetPayPalEmail(user);
            if(Input.PaypalEmail != paypalEmail)
            {
                var setPaypalEmail = await _paymentService.SetupPaypalEmail(user, Input.PaypalEmail);
                if (setPaypalEmail == 0)
                {
                    StatusMessage = "Unexpected error when trying to set phone number.";
                    return RedirectToPage();
                }
            }


            var biography = user.Biography;
            if (Input.Biography != biography)
            {
                var setBiographyResult = await _profileService.SetBiographyAsync(user, Input.Biography);
                if (setBiographyResult == 0)
                {
                    StatusMessage = "Unexpected error when trying to set the biography.";
                    return RedirectToPage();
                }
            }
            
            var curriculum = user.Curriculum;
            if (Input.Curriculum != curriculum)
            {
                var setCurriculumResult = await _profileService.SetCurriculumAsync(user, Input.Curriculum);
                if (setCurriculumResult == 0)
                {
                    StatusMessage = "Unexpected error when trying to set the Curriculum.";
                    return RedirectToPage();
                }
            }

            var isEmailSubscribed = await _emailNotificationService.IsSubscribedEmail(user);
            if(Input.IsEmailSubscribed != isEmailSubscribed)
            {
                var setIsSubscribedEmail = await _emailNotificationService.SetEmailSubscription(user, Input.IsEmailSubscribed);
                if (setIsSubscribedEmail == 0)
                {
                    StatusMessage = "Unexpected error when trying to set Email Notification Subscription.";
                    return RedirectToPage();
                }
            }

            /*var skillTagList = new List<SkillTag>();
            var skillTagList = user.SkillTags;
            if(user.SkillTags == )
            if (!skillTagList.Contains(Input.SkillTag)){
                var addSkillTagResult = await _skillTagService.AddSkillTagAsync(user, Input.SkillTag);
                if (addSkillTagResult == 0)
                {
                    StatusMessage = "Unexpected error when trying to add a Skill Tag.";
                    return RedirectToPage();
                }
            }*/

            List<SkillTag> allSkillTags = await _skillTagService.GetAllAsync();
            ICollection<SkillTag> skillTags = new List<SkillTag>();

            if (Input.SelectedSkillTags != null)
            {
                foreach (int id in Input.SelectedSkillTags)
                {
                    SkillTag skillTag = allSkillTags.Where<SkillTag>(x => x.SkillTagId == id).FirstOrDefault();
                    if (skillTag != null)
                    {
                        skillTags.Add(skillTag);
                    }
                }
            }

            var setSkillTagsResult = await _profileService.SetSkillTagsAsync(user, skillTags);
            if (setSkillTagsResult == 0)
            {
                StatusMessage = "Unexpected error when trying to set the SkillTags.";
                return RedirectToPage();
            }

            var profileLink = "/Profile/Details?username=" + user.UserName;
            await _notificationService.SendNotification(user, "Profile Updated", "Your profile has been updated! Please comfirm the changes made to your profile.", profileLink);

            await _signInManager.RefreshSignInAsync(user);
            StatusMessage = "Your profile has been updated";
            return RedirectToPage();
        }
    }
}
