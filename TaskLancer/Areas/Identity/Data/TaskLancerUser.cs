﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using TaskLancer.Models;

namespace TaskLancer.Areas.Identity.Data;

// Add profile data for application users by adding properties to the TaskLancerUser class
public class TaskLancerUser : IdentityUser
{
    [PersonalData]
    public String? FirstName { get; set; }
    [PersonalData]
    public String? LastName { get; set; }
    [PersonalData]
    public String? Biography { get; set; }
    [PersonalData]
    public String? Curriculum { get; set; }
    
    public virtual ICollection<SkillTag>? SkillTags{get; set;}

    public virtual ICollection<Models.FileTransfer.DAL.File>? FilesAllowedAccess { get; set; }

}

