﻿namespace TaskLancer.Views.FileTransfer
{
    public class UploadPartialViewModel
    {
        public List<Models.FileTransfer.DAL.File>? PreviouslyUploadedFiles;

        public string? UploadedFilesJson;

        public string AccessPolicy = "public";

        public string FieldName = "";
    }
}
